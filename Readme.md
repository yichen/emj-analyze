# EMJ Analysis

This is a collection of EDM file analyzers, mainly designed to help with
debugging the production process, as well as generate basic quality plots for the
signal process. For installing, in the CMSSW release that contains the production
code, run the following commands:

```bash
cd CMSSW_X_Y_Z/src

# For CMSSW augment library written by Yi-Mu
git clone --recurse-submodules https://github.com/yimuchen/UserUtils

# For the main EMJ analysis code
git clone https://gitlab.cern.ch/yichen/emj-analyze EMJ/Analyze
```

This sub package includes a set of EDAnalyzers to help produce quality checks on
the generated EDM files to ensure that the Production code is generating useful
samples. The configuration files to be used can be found in `test/run*.py`

## `runEMJVar.py`

Running a histogram creation routine for Emerging jet variables (weighted prompt
particle fractions... etc).