#include "EMJ/Analyze/interface/EMJObjectSelect.hpp"

#include "UserUtils/Common/interface/STLUtils/VectorUtils.hpp"
#include "UserUtils/PhysUtils/interface/MCHelper.hpp"

#include "DataFormats/Math/interface/deltaR.h"
#include "DataFormats/VertexReco/interface/Vertex.h"
#include "TrackingTools/IPTools/interface/IPTools.h"

#include <algorithm>
#include <stack>

#include "RecoVertex/KalmanVertexFit/interface/KalmanVertexFitter.h"
#include \
  "RecoVertex/TrimmedKalmanVertexFinder/interface/KalmanTrimmedVertexFinder.h"

#include "TLorentzVector.h"

std::vector<const reco::Candidate*>
FindEMJPartons( const std::vector<reco::GenParticle>& vec )
{
  std::vector<const reco::Candidate*> ans;

  // Getting iterator to the mediator parton
  auto med1 = std::find_if( vec.begin(),
                            vec.end(), []( const reco::GenParticle& x ){
    return x.pdgId() == 4900001;
  } );
  auto med2 = std::find_if( med1+1,
                            vec.end(), []( const reco::GenParticle& x ){
    return x.pdgId() == -4900001;
  } );

  // Early exit if mediator particles are not found
  if( med1 == vec.end() && med2 == vec.end() ){
    return ans;
  }

  // Moving the iterators to the last entry in the radiative correction
  // Chain
  const auto med1_ptr = usr::mc::GetLastInChain( &*med1 );
  const auto med2_ptr = usr::mc::GetLastInChain( &*med2 );

  for( unsigned i = 0; i < med1_ptr->numberOfDaughters(); ++i ){
    if( ( abs( med1_ptr->daughter( i )->pdgId() ) <= 6 ) ||// SM Decay candidate
        ( abs( med1_ptr->daughter( i )->pdgId() ) >= 4900101 &&
          abs( med1_ptr->daughter( i )->pdgId() ) <= 4900103 ) ){
      ans.push_back( usr::mc::GetLastInChain( med1_ptr->daughter( i ) ) );
    }
  }

  for( unsigned i = 0; i < med2_ptr->numberOfDaughters(); ++i ){
    if( ( abs( med2_ptr->daughter( i )->pdgId() ) <= 6 ) ||// SM Decay candidate
        ( abs( med2_ptr->daughter( i )->pdgId() ) >= 4900101 &&
          abs( med2_ptr->daughter( i )->pdgId() ) <= 4900103 ) ){
      ans.push_back( usr::mc::GetLastInChain( med2_ptr->daughter( i ) ) );
    }
  }

  return ans;
}


// Returning the dark quark with the smaller Eta
const reco::Candidate*
FindDarkQuark( const std::vector<reco::GenParticle>& vec )
{
  const auto partons = FindEMJPartons( vec );

  int    idx     = -1;
  double abs_eta = 100;

  for( unsigned i = 0; i < partons.size(); ++i ){
    if( abs( partons[i]->pdgId() ) > 4900000 ){
      if( fabs( partons[i]->eta() ) < abs_eta ){
        idx     = i;
        abs_eta = fabs( partons[i]->eta() );
      }
    }
  }

  if( idx > 0 ){
    return partons[idx];
  } else {
    return nullptr;
  }
}


const reco::Candidate*
FindSMQuark( const std::vector<reco::GenParticle>& vec )
{
  const auto partons = FindEMJPartons( vec );

  int    idx     = -1;
  double abs_eta = 100;

  for( unsigned i = 0; i < partons.size(); ++i ){
    if( abs( partons[i]->pdgId() ) <= 6 ){
      if( fabs( partons[i]->eta() ) < abs_eta ){
        idx     = i;
        abs_eta = fabs( partons[i]->eta() );
      }
    }
  }

  if( idx > 0 ){
    return partons[idx];
  } else {
    return nullptr;
  }
}


std::vector<std::vector<const reco::Candidate*> >
FindVertexChildren( const reco::GenParticle* parton )
{
  std::vector<std::vector<const reco::Candidate*> > ans;
  if( parton == nullptr ){
    return ans;
  }

  // Stack for DFS crawling.
  std::stack<const reco::GenParticle*> dfs_stack;
  dfs_stack.push( parton );

  while( !dfs_stack.empty() ){
    const reco::GenParticle* part = dfs_stack.top();
    dfs_stack.pop();

    int match_idx = -1;

    // Preparing the DFS stack.
    for( unsigned i = 0; i < part->numberOfDaughters(); ++i ){
      dfs_stack.push(
        dynamic_cast<const reco::GenParticle*>( part->daughter( i ) ) );
    }

    // No need to consider vertexing if the particle is not visible to the
    // tracker
    //
    if( part->charge() == 0 || abs( part->pdgId() ) < 10 ||
        ( abs( part->pdgId() ) > 20 && abs( part->pdgId() ) < 100 ) ||
        ( abs( part->pdgId() ) > 490000 ) ){
      continue;
    }

    // Only considering final state particles or end-of-chain particles
    if( part->status() != 1 && part->numberOfDaughters() != 0 ){
      continue;
    }

    for( unsigned i = 0; i < ans.size(); ++i ){
      const auto vertex_part = ans.at( i ).at( 0 );
      if( fabs( part->vertex().x()-vertex_part->vertex().x() ) < 0.02 &&
          fabs( part->vertex().y()-vertex_part->vertex().y() ) < 0.02 &&
          fabs( part->vertex().z()-vertex_part->vertex().z() ) < 0.05 ){
        match_idx = i;
        break;
      }
    }

    if( match_idx != -1 ){
      ans[match_idx].push_back( part );
    } else {
      ans.push_back( std::vector<const reco::Candidate*>() );
      ans.back().push_back( part );
    }
  }

  for( unsigned i = ans.size()-1; i < ans.size(); --i ){
    ans[i] = usr::RemoveDuplicate( ans[i] );
    if( ans[i].size() < 2 ){
      ans.erase( ans.begin()+i );
    }
  }

  return ans;
}


int
GetPrimaryVertex( const std::vector<reco::Vertex>& pvlist )
{

  const auto&  pv0        = pvlist.front();
  const double pv0_sumpt2 = PVSumPt2( pv0 );

  for( const auto& vertex : pvlist  ){
    if( PVSumPt2( vertex ) > pv0_sumpt2 ){ return -1; }
  }

  if( fabs( pv0.z() ) > 15 ){return -1; }
  if( pv0.isFake() ){ return -1; }

  return 0;
}


double
PVSumPt2( const reco::Vertex& vtx )
{
  double sum = 0.;
  double pt  = 0.;
  if( vtx.hasRefittedTracks() ){
    for( const auto track :  vtx.refittedTracks() ){
      pt   = track.pt();
      sum += pt * pt;
    }
  } else {
    for( auto iter = vtx.tracks_begin(); iter != vtx.tracks_end(); iter++ ){
      pt   = ( *iter )->pt();
      sum += pt * pt;
    }
  }
  return sum;
}


double
PVJetFuzzyness( const reco::Vertex&                           vtx,
                const std::vector<std::vector<reco::Track> >& jet_tracks_list,
                const TransientTrackBuilder&                  ttBuilder  )
{
  double sum    = 0;
  double ntrack = 0;

  for( const auto& jet_tracks : jet_tracks_list ){
    for( const auto& track :  jet_tracks ){
      auto ttrack = ttBuilder.build( track );
      auto ip3d_p = IPTools::absoluteImpactParameter3D( ttrack, vtx );

      sum   += ip3d_p.second.value();
      ntrack = ntrack+1.0;
    }
  }

  return sum /= ntrack;
}


double
PVFuzzyness( const reco::Vertex& vtx, const TransientTrackBuilder& ttBuilder  )
{
  if( vtx.hasRefittedTracks() ){
    double sum = 0;

    for( const auto& track :  vtx.refittedTracks() ){
      auto ttrack = ttBuilder.build( track );
      auto ip3d_p = IPTools::absoluteImpactParameter3D( ttrack, vtx );

      sum += ip3d_p.second.value();
    }

    return sum /= vtx.refittedTracks().size();
  } else {
    double sum = 0;

    for( auto iter = vtx.tracks_begin(); iter != vtx.tracks_end(); iter++ ){
      auto ttrack = ttBuilder.build( *( *iter ) );
      auto ip3d_p = IPTools::absoluteImpactParameter3D( ttrack, vtx );

      sum += ip3d_p.second.value();
    }

    return sum /= std::distance( vtx.tracks_begin(), vtx.tracks_end() );
  }
}


double
PVClosestZ( const reco::Vertex& pv, const std::vector<reco::Vertex>& vtxlist )
{
  double min_z = 10000;

  for( const auto vtx : vtxlist ){
    const double z = fabs( pv.z()-vtx.z() );
    if( z == 0 ){ continue; }
    min_z = std::min( z, min_z );
  }

  return min_z;
}


void
GetSelectedJetTracks( const std::vector<reco::PFJet>&         jet_source,
                      const std::vector<reco::Track>&         track_source,
                      std::vector<reco::PFJet>&               jet_out,
                      std::vector<std::vector<reco::Track> >& track_out )
{
  jet_out.clear();
  track_out.clear();

  for( const auto jet : jet_source ){
    // Basic jet kinematic and quality cuts;
    if( jet.pt() < 50 ){ continue; }
    if( fabs( jet.eta() ) > 2.0 ){ continue;  }
    if( jet.chargedEmEnergyFraction() > 0.9 ){ continue; }
    if( jet.neutralEmEnergyFraction() > 0.9 ){ continue; }

    std::vector<reco::Track> jet_tracks;
    bool                     is_bad_jet = 0;

    for( const auto track : track_source ){
      // Basic track quality cuts
      if( track.pt() < 1.0 ){  continue; }
      if( !track.quality( reco::TrackBase::highPurity ) ){ continue; }

      // Delta R matching with jet
      if( deltaR( track, jet ) > 0.4 ){ continue; }

      // Bad if a track is within jet and has too high a pt
      if( track.pt() > jet.pt() * 0.6  ){ is_bad_jet = true;  break; }

      jet_tracks.push_back( track );
    }

    if( is_bad_jet ){ continue; }
    if( jet_tracks.size() == 0  ){ continue; }

    jet_out.push_back( jet );
    track_out.push_back( jet_tracks );
  }
}


bool
IsGoodTrack( const reco::Track&           track,
             const reco::Vertex&          pv,
             const TransientTrackBuilder& ttBuilder )
{
  if( track.pt() < 1.0 ){ return false; }
  if( !track.quality( reco::TrackBase::highPurity ) ){ return false; }

  // Calculating impact parameter

  //auto ttrack = ttBuilder.build( track );
  //auto ip3d_p = IPTools::absoluteImpactParameter3D( ttrack, pv );
  //auto ip2d_p = IPTools::absoluteTransverseImpactParameter( ttrack, pv );

  //const double ip3d = ip3d_p.second.value();
  //const double ip2d = ip2d_p.second.value();
  //const double ipz = std::sqrt( ip3d * ip3d-ip2d * ip2d );

  // if( ipz > 2.5 ){ return false; }

  return true;
}


void
ExtendTrackList( std::vector<reco::Track>&                tracklist,
                 const reco::Vertex&                      pv,
                 const std::vector<pat::PackedCandidate>& source,
                 const TransientTrackBuilder&             ttBuilder )
{
  for( const auto& cand : source ){
    if( !cand.hasTrackDetails() ){ continue; }
    const auto track = cand.pseudoTrack();
    if( !IsGoodTrack( track, pv, ttBuilder ) ){ continue; }
    tracklist.push_back( track );
  }
}


void
ExtendTrackList( std::vector<reco::Track>&       tracklist,
                 const reco::Vertex&             pv,
                 const std::vector<reco::Track>& source,
                 const TransientTrackBuilder&    ttBuilder )
{
  for( const auto& track : source ){
    if( !IsGoodTrack( track, pv, ttBuilder ) ){ continue; }
    tracklist.push_back( track );
  }
}


std::vector<reco::TransientTrack>
MakeTTList( const std::vector<reco::Track>& source,
            const TransientTrackBuilder&    ttBuilder )
{
  std::vector<reco::TransientTrack> ans;

  for( const auto& track : source ){
    ans.push_back( ttBuilder.build( track ) );
  }

  return ans;
}


double
PVTrackFraction( const reco::Vertex&                           pv,
                 const std::vector<std::vector<reco::Track> >& jet_tracks_list )
{
  double n_tracks = 0;
  double n_passed = 0;

  for( const auto jet_tracks : jet_tracks_list ){
    for( const auto track : jet_tracks ){
      // Assuming basic qualities have already been passed
      const double trk_z = track.referencePoint().z();
      const double pv_z  = pv.z();

      n_tracks = n_tracks+1;
      if( fabs( pv_z-trk_z ) < 0.01 ){ n_passed = n_passed+1;  }
    }
  }

  return n_passed / n_tracks;
}


bool
TrackMatchJet( const reco::Jet& jet, const reco::TransientTrack& ttrack  )
{
  if( deltaR( ttrack.track(), jet ) > 0.4 ){ return false; }

  return true;
}


std::vector<reco::TransientTrack>
MatchJetTracks( const reco::Jet&                         jet,
                const std::vector<reco::TransientTrack>& tracklist )
{
  std::vector<reco::TransientTrack> ans;

  for( const auto track : tracklist ){
    if( TrackMatchJet( jet, track ) ){
      ans.push_back( track );
    }
  }

  return ans;
}


TransientVertex
MakeSeedVertex( std::vector<reco::TransientTrack>& tracklist )
{
  edm::ParameterSet pset;
  pset.addParameter<double>( "maxDistance",        0.1 );
  pset.addParameter<int>(    "maxNbrOfIterations", 50 );// 10
  KalmanVertexFitter vertexFitter( pset );
  TransientVertex    ans;

  if( tracklist.size() < 2 ){ return ans; }

  for( unsigned i = 0; i < tracklist.size(); ++i ){
    for( unsigned j = i+1; j < tracklist.size(); ++j ){
      // Running over all pairs of tracks to generate vertex seed
      const std::vector<reco::TransientTrack> trackdoublet = {
        tracklist.at( i ), tracklist.at( j )
      };

      ans = vertexFitter.vertex( trackdoublet );
      if( ans.isValid() ){
        tracklist.erase( tracklist.begin()+j );
        tracklist.erase( tracklist.begin()+i );
        return ans;
      }
    }
  }

  return ans;
}


std::vector<TransientVertex>
ManualVertexLoop( const std::vector<reco::TransientTrack>& tracklist )
{
  edm::ParameterSet pset;
  pset.addParameter<double>( "maxDistance",        0.1 );
  pset.addParameter<int>(    "maxNbrOfIterations", 50 );// 10
  KalmanVertexFitter vertexFitter( pset );

  std::vector<TransientVertex>      ans;
  std::vector<reco::TransientTrack> unused = tracklist;

  TransientVertex seed = MakeSeedVertex( unused );
  if( !seed.isValid() ){ return ans; }

  for( int i = unused.size()-1; i >= 0; --i ){
    const auto track = unused.at( i );

    // Getting the list of tracks in the vertex and try to add this track to it
    std::vector<reco::TransientTrack> track_in_vertex = seed.originalTracks();
    track_in_vertex.push_back( track );

    TransientVertex new_seed = vertexFitter.vertex( track_in_vertex );
    if( new_seed.isValid()
        && new_seed.originalTracks().size() == seed.originalTracks().size()+1 ){
      unused.erase( unused.begin()+i );
      seed = new_seed;
    }
  }

  ans.push_back( seed );

  if( unused.size() >= 2 ){
    const auto second_ans = ManualVertexLoop( unused );

    for( const auto vertex : second_ans ){
      ans.push_back( vertex );
    }
  }

  return ans;
}


std::vector<TransientVertex>
MakeJetVertices( const reco::Jet&                         jet,
                 const reco::Vertex&                      pv,
                 const std::vector<reco::TransientTrack>& tracklist )
{
  KalmanTrimmedVertexFinder vertexFinder;
  vertexFinder.setPtCut( 1.0 );
  vertexFinder.setVertexFitProbabilityCut( 0.2 );
  vertexFinder.setTrackCompatibilityToSV( 0.5 );
  vertexFinder.setTrackCompatibilityCut( 0.5 );

  std::vector<reco::TransientTrack> unused_tracks;
  std::vector<TransientVertex>      ans =
    vertexFinder.vertices( MatchJetTracks( jet, tracklist ),
                           unused_tracks );

  if( unused_tracks.size() > 2 ){
    std::vector<TransientVertex> manual = ManualVertexLoop( unused_tracks );

    for( const auto vertex : manual ){
      ans.push_back( vertex );
    }
  }

  return SelectVertex( jet, pv, ans );
}


std::vector<TransientVertex>
SelectVertex( const reco::Jet&                    jet,
              const reco::Vertex&                 pv,
              const std::vector<TransientVertex>& original_list  )
{
  std::vector<TransientVertex> ans;
  TLorentzVector               jetdirection;
  jetdirection.SetPtEtaPhiE( jet.pt(), jet.eta(), jet.phi(), jet.energy() );

  for( const auto vertex : original_list ){
    TLorentzVector vertexdirection( vertex.position().x()-pv.position().x(),
                                    vertex.position().y()-pv.position().y(),
                                    vertex.position().z()-pv.position().z(),
                                    0 );
    vertexdirection.SetPxPyPzE(
      vertexdirection.Px(),
      vertexdirection.Py(),
      vertexdirection.Pz(),
      vertexdirection.P() );

    // Vertex must be in Jet cone
    if( vertexdirection.DeltaR( jetdirection ) > 0.4 ){
      continue;
    }

    ans.push_back( vertex );
  }

  return ans;
}


std::string
TrackPixelPattern( const reco::Track& track )
{
  std::string pattern_str = "";
  const auto& pattern     =  track.hitPattern();

  for( int i = 0;
       i < pattern.numberOfAllHits( reco::HitPattern::TRACK_HITS );
       i++ ){
    const uint32_t hit =
      pattern.getHitPattern( reco::HitPattern::TRACK_HITS, i );

    if( !pattern.validHitFilter( hit ) ){ continue; }
    if( !pattern.pixelHitFilter( hit ) ){ continue; }

    uint16_t layer = pattern.getLayer( hit );
    layer = pattern.pixelBarrelHitFilter( hit ) ?
            layer :
            pattern.pixelEndcapHitFilter( hit ) ?
            layer+4 :
            0;
    pattern_str.push_back( '0'+layer );
  }

  return pattern_str == "" ?
         "0" :
         pattern_str;
}
