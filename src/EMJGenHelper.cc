#include "UserUtils/PhysUtils/interface/MCHelper.hpp"

#include "DataFormats/HepMCCandidate/interface/GenParticle.h"

extern bool
IsHVParticle( const reco::Candidate* gen )
{
  return abs( gen->pdgId() ) >= 4900000 && abs( gen->pdgId() ) <= 4900500;
}

extern bool
IsHVMediator( const reco::Candidate* gen )
{
  return abs( gen->pdgId() ) >= 4900001 && abs( gen->pdgId() ) <= 4900006;
}

extern bool
IsHVQuark( const reco::Candidate* gen )
{
  return abs( gen->pdgId() ) >= 4900101 && abs( gen->pdgId() ) <= 4900103;
}

extern bool
IsHVMeson( const reco::Candidate* gen )
{
  return abs( gen->pdgId() ) == 4900111 ||
         abs( gen->pdgId() ) == 4900113 ||
         abs( gen->pdgId() ) == 4900211 ||
         abs( gen->pdgId() ) == 4900213
  ;
}

extern bool
HasHVDaughter( const reco::Candidate* x )
{
  return usr::mc::HasDaughter( x,
    usr::mc::pp( []( const reco::Candidate* d )->bool {
    return IsHVParticle( d );
  } ) );
}

extern bool
HasHVMother( const reco::Candidate* x )
{
  return usr::mc::HasMother( x,
    usr::mc::pp( []( const reco::Candidate* m )->bool {
    return IsHVParticle( m );
  } ) );
}
