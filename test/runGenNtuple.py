import FWCore.ParameterSet.Config as cms
from FWCore.ParameterSet.VarParsing import VarParsing
from EMJ.Production.emjHelper import emjHelper

options = VarParsing('analysis')
options.register('mMed',
                 default=1000.0,
                 mult=VarParsing.multiplicity.singleton,
                 mytype=VarParsing.varType.float,
                 info=('Mass of the dark mediator [GeV] (default=%default)'))
options.register('mDark',
                 default=10.0,
                 mult=VarParsing.multiplicity.singleton,
                 mytype=VarParsing.varType.float,
                 info='Mass of dark pion [GeV] (default=%default)')
options.register('kappa',
                 default=1.0,
                 mult=VarParsing.multiplicity.singleton,
                 mytype=VarParsing.varType.float,
                 info='Kappa0 Squared for scaling lifetime(default=%default)')
options.register('mode',
                 default='unflavored',
                 mult=VarParsing.multiplicity.singleton,
                 mytype=VarParsing.varType.string,
                 info='Mixing scenario to use (default=%default)')
options.register('type',
                 default='down',
                 mult=VarParsing.multiplicity.singleton,
                 mytype=VarParsing.varType.string,
                 info='Coupling to down or up type SM quarks (default=%default)')
options.register('part',
                 default=1,
                 mult=VarParsing.multiplicity.singleton,
                 mytype=VarParsing.varType.int,
                 info='Label for individual jobs (for different RNG seed)')
options.register('outpre',
                 default='gentuple',
                 mult=VarParsing.multiplicity.singleton,
                 mytype=VarParsing.varType.string,
                 info='Label for individual jobs (for different RNG seed)')

# options.outputFile = 'GenHistogram.root'
options.parseArguments()
_helper = emjHelper()
_helper.setModel(mMed=options.mMed,
                 mDark=options.mDark,
                 kappa=options.kappa,
                 mode=options.mode,
                 type=options.type)

# output name definition
_outname = _helper.getOutName(
    events=options.maxEvents, part=options.part, signal=True,
    outpre=options.outpre) + '.root'

# import process from standard to allow for identical configurations
process = getattr(
    __import__('EMJ.Production.2018.step1_GEN', fromlist=['process']), 'process')

process.generator.crossSection = cms.untracked.double(1.0)
process.generator.PythiaParameters.processParameters = cms.vstring(
    _helper.getPythiaSettings())
new_settings = [
    x for x in process.generator.PythiaParameters.pythia8CommonSettings
    if not x.startswith('ParticleDecays:limitTau0')
    and not x.startswith('ParticleDecays:tau0Max')
]
new_settings.append('ParticleDecays:limitTau0 = off')
process.generator.PythiaParameters.pythia8CommonSettings = cms.vstring(
    new_settings)

# Setting the number of max events
process.maxEvents.input = cms.untracked.int32(options.maxEvents)
process.options.numberOfThreads = cms.untracked.uint32(4)

# Removing the GEN output file. This is to avoid stuff taking up too much space
delattr(process, 'RAWSIMoutput')

# Setting up the stuff for histogram generation
process.TFileService = cms.Service("TFileService", fileName=cms.string(_outname))
process.load("EMJ.Analyze.EMJGenNtuple_cfi")
process.p1 = cms.EndPath(process.EMJGenNtuple)
process.schedule = cms.Schedule(process.generation_step,
                                process.genfiltersummary_step, process.p1,
                                process.endjob_step)

