import FWCore.ParameterSet.Config as cms
from FWCore.ParameterSet.VarParsing import VarParsing
from EMJ.Production.emjHelper import emjHelper

options = VarParsing('analysis')
options.outputFile = 'TrackGenNtuple.root'
options.parseArguments()

process = cms.Process("TrackGenNtuplizer")

process.load("Configuration.StandardSequences.Services_cff")

# Auto generated from descriptions
process.load("EMJ.Analyze.TrackGenNtuplizer_cfi")

process.maxEvents = cms.untracked.PSet(
  input=cms.untracked.int32(options.maxEvents))

process.source = cms.Source("PoolSource",
                            fileNames=cms.untracked.vstring(options.inputFiles))

process.TFileService = cms.Service("TFileService",
                                   fileName=cms.string(options.outputFile))

# Other statements
process.load('Configuration.StandardSequences.GeometryRecoDB_cff')
process.load('Configuration.StandardSequences.MagneticField_cff')
process.load('Configuration.StandardSequences.FrontierConditions_GlobalTag_cff')
from Configuration.AlCa.GlobalTag import GlobalTag

process.GlobalTag = GlobalTag(process.GlobalTag,
                              '106X_upgrade2018_realistic_v15_L1v1', '')

process.load("TrackingTools.TransientTrack.TransientTrackBuilder_cfi")
process.load("Configuration.StandardSequences.Reconstruction_cff")

# Making the program less verbose
process.load('FWCore.MessageService.MessageLogger_cfi')
process.MessageLogger.cerr.FwkReport.reportEvery = 1000

process.options = cms.untracked.PSet(allowUnscheduled=cms.untracked.bool(True), )

process.options.numberOfThreads = cms.untracked.uint32(1)
process.options.numberOfStreams = cms.untracked.uint32(1)

process.p1 = cms.Path(process.TrackGenNtuplizer)
