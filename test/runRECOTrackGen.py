# import process
import FWCore.ParameterSet.Config as cms
from FWCore.ParameterSet.VarParsing import VarParsing
from EMJ.Production.emjHelper import emjHelper

options = VarParsing('analysis')
options.outputFile = 'RECOTrackGenNtuple.root'
options.register('outEDM',
                 default='',
                 mult=VarParsing.multiplicity.singleton,
                 mytype=VarParsing.varType.string,
                 info='Output EDM root file')
options.parseArguments()

# Loading the input process
process = getattr(
  __import__('EMJ.Production.UL18.step3_RECO', fromlist=['process']), 'process')
# Overloading the input status
process.source.fileNames = cms.untracked.vstring(options.inputFiles)

process.maxEvents = cms.untracked.PSet(
  input=cms.untracked.int32(options.maxEvents))

process.TFileService = cms.Service("TFileService",
                                   fileName=cms.string(options.outputFile))

# Other statements
process.load('Configuration.StandardSequences.GeometryRecoDB_cff')
process.load('Configuration.StandardSequences.MagneticField_cff')
process.load('Configuration.StandardSequences.FrontierConditions_GlobalTag_cff')

from Configuration.AlCa.GlobalTag import GlobalTag

process.GlobalTag = GlobalTag(process.GlobalTag,
                              '106X_upgrade2018_realistic_v15_L1v1', '')

process.load("TrackingTools.TransientTrack.TransientTrackBuilder_cfi")

# Alternate method, how to check if this is loaded
process.load("EMJ.Analyze.TrackGenNtuplizer_cfi")

# Allowing 2 track vertices to be constructed
process.particleFlowDisplacedVertex.switchOff2TrackVertex = cms.untracked.bool(
  False)

# Making the program less verbose
process.load('FWCore.MessageService.MessageLogger_cfi')
process.MessageLogger.cerr.FwkReport.reportEvery = 1
process.ntuplizer = cms.Path(process.TrackGenNtuplizer)

# Modifying the processing stage
# Standard step for GEN_SIM_RAW to AOD/RECO
steps = [
  process.raw2digi_step,
  #process.L1Reco_step,
  process.reconstruction_step, process.recosim_step,
  # process.eventinterpretaion_step,
  # process.endjob_step,  #
]
steps.append(process.ntuplizer),  # Adding the n-tuplizer step

if options.outEDM:
  step.append(process.AODSIMoutput_step)
  process.AODSIMoutput.fileName = cms.untracked.string(options.outEDM)

process.schedule = cms.Schedule(*steps)
