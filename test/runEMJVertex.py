import FWCore.ParameterSet.Config as cms
from FWCore.ParameterSet.VarParsing import VarParsing

options = VarParsing('analysis')
options.outputFile = 'EMJVertex.root'

options.register('mode',
                 default=2,
                 mult=VarParsing.multiplicity.singleton,
                 mytype=VarParsing.varType.int,
                 info='Mode to assign jet types (default=%default)')
options.register('extra',
                 default='mytest.txt',
                 mult=VarParsing.multiplicity.singleton,
                 mytype=VarParsing.varType.string,
                 info='file to print extraordinary event id (default=%default)')

options.parseArguments()

process = cms.Process("EMJVar")

process.load("Configuration.StandardSequences.Services_cff")

# Auto generated from descriptions
process.load("EMJ.Analyze.EMJVertex_cfi")
process.EMJVertex.mode = options.mode
process.EMJVertex.txtfile = options.extra

process.maxEvents = cms.untracked.PSet(
    input=cms.untracked.int32(options.maxEvents))

process.source = cms.Source("PoolSource",
                            fileNames=cms.untracked.vstring(options.inputFiles),
                            )

process.TFileService = cms.Service("TFileService",
                                   fileName=cms.string(options.outputFile))

# Other statements
process.load('Configuration.StandardSequences.GeometryRecoDB_cff')
process.load('Configuration.StandardSequences.MagneticField_cff')
process.load('Configuration.StandardSequences.FrontierConditions_GlobalTag_cff')
from Configuration.AlCa.GlobalTag import GlobalTag
process.GlobalTag = GlobalTag(process.GlobalTag,
                              '102X_upgrade2018_realistic_v15', '')

process.load("TrackingTools/TransientTrack/TransientTrackBuilder_cfi")

# Making the program less verbose
process.load('FWCore.MessageService.MessageLogger_cfi')
process.MessageLogger.cerr.FwkReport.reportEvery = 100
process.MessageLogger.suppressWarning = cms.untracked.vstring(
    'EMJVertex')

process.p1 = cms.Path(process.EMJVertex)

