#!/bin/bash

cmsRun runEMJVertex.py inputFiles_load=MINIAOD_mMed-1000_mDark-20_ctau-5_unflavored-down.txt   outputFile=VertexGraph_ctau-5.root   mode=0 extra=EXT_ctau-5.txt
cmsRun runEMJVertex.py inputFiles_load=MINIAOD_mMed-1000_mDark-20_ctau-25_unflavored-down.txt  outputFile=VertexGraph_ctau-25.root  mode=0 extra=EXT_ctau-25.txt
cmsRun runEMJVertex.py inputFiles_load=MINIAOD_mMed-1000_mDark-20_ctau-100_unflavored-down.txt outputFile=VertexGraph_ctau-100.root mode=0 extra=EXT_ctau-100.txt

cmsRun runEMJVertex.py inputFiles_load=MINIAOD_mMed-1000_mDark-20_kappa-0p18_aligned-down.txt outputFile=VertexGraph_kappa-0p18.root mode=0 extra=EXT_kappa-0p18.txt
cmsRun runEMJVertex.py inputFiles_load=MINIAOD_mMed-1000_mDark-20_kappa-0p25_aligned-down.txt outputFile=VertexGraph_kappa-0p25.root mode=0 extra=EXT_kappa-0p25.txt
cmsRun runEMJVertex.py inputFiles_load=MINIAOD_mMed-1000_mDark-20_kappa-0p37_aligned-down.txt outputFile=VertexGraph_kappa-0p37.root mode=0 extra=EXT_kappa-0p37.txt

cmsRun runEMJVertex.py inputFiles=file:///home/yichen/data/EDM/TTBar/2018/TTJets_HT-1200to2500_TuneCP5_13TeV-madgraphMLM-pythia8.root outputFile=VertexGraph_TTbar.root mode=1 extra=EXT_TTbar.txt
cmsRun runEMJVertex.py inputFiles=file:///home/yichen/data/EDM/QCD/2018/QCD_HT2000toInf_TuneCP5_13TeV-madgraphMLM-pythia8.root        outputFile=VertexGraph_QCD.root   mode=2 extra=EXT_QCD.txt
