from UserUtils.PlotUtils.SignalCompare import SignalCompare1D
from UserUtils.PlotUtils import plt

SignalCompare1D(
    hist_config=[{
        'key': 'GenHistogram/DarkMesonPt',
        'xtitle': 'Dark meson p_{{T}}',
    }],
    sample_config=[{
        'title': 'm_{{X}} = 1000GeV, m_{{dark}} = 10GeV',
        'file':
        'genhist_mMed-1000_mDark-10_ctau-1_unflavored-down_n-2000_part-1.root',
        'color': plt.col.black
    }, {
        'title': 'm_{{X}} = 1000GeV, m_{{dark}} = 1GeV',
        'file':
        'genhist_mMed-1000_mDark-1_ctau-1_unflavored-down_n-2000_part-1.root',
        'color': plt.col.blue
    }])
