#!/bin/bash

file=${1}
index=${2}

inputfile=$(cat $file | head -n ${index} | tail -n 1)
outputfile=${file/.txt/}_Ntuple_${index}.root

cmsRun EMJ/Analyze/test/runTrackGenNtuplizer.py inputFiles=${inputfile} outputFile=${outputfile}
xrdcp -f ${outputfile} root://cmseos.fnal.gov//store/user/yimuchen/EmergingJets/TrackGenNTuples/${outputfile}
rm ${outputfile}