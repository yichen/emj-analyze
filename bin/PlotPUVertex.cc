#include "UserUtils/Common/interface/ArgumentExtender.hpp"
#include "UserUtils/EDMUtils/interface/ParseEDM.hpp"
#include "UserUtils/PlotUtils/interface/Simple1DCanvas.hpp"

#include "DataFormats/FWLite/interface/ChainEvent.h"
#include "DataFormats/FWLite/interface/Handle.h"
#include "DataFormats/HepMCCandidate/interface/GenParticle.h"
#include "DataFormats/VertexReco/interface/Vertex.h"
#include "SimDataFormats/PileupSummaryInfo/interface/PileupSummaryInfo.h"

#include "TRandom3.h"
#include <iostream>
#include <vector>

const PileupSummaryInfo& GetPU( const std::vector<PileupSummaryInfo>&, const int bx = 0 );
TGraph*                  MakePileUp( const PileupSummaryInfo& );
TGraph*                  MakePVFull( const std::vector<reco::Vertex>& );
TGraph*                  MakePVMain( const std::vector<reco::Vertex>& );
TGraph*                  MakeGen( const std::vector<reco::GenParticle>& );
double                   GetSumPT( const reco::Vertex& );

int
main( int argc, char* argv[] )
{
  usr::po::options_description desc(
    "Drawing the reconstructed primary vertex, the GEN level hard-scattering vertex and Pileup verticies" );
  desc.add_options()
    ( "input,i", usr::po::multivalue<std::string>(), "Input **AOD** root file to generate the event display" )
    ( "event", usr::po::value<std::string>(),        "event number in the format of \"LumiID:EventID\" or \"EventID\" (the Run is always 1)" )
    ( "eventidx", usr::po::value<int>(),             "The event to display as indexed in the file." )
    ( "data,d", usr::po::defvalue<std::string>( "QCD" ), "Display data type" )
    ( "output,o", usr::po::defvalue<std::string>( "test.pdf" ), "Output filename" )
  ;

  usr::ArgumentExtender args;
  args.AddOptions( desc );
  args.ParseOptions( argc, argv );

  // Setting up the variables
  const std::vector<std::string> input = args.ArgList<std::string>( "input" );
  const unsigned event_idx             = args.ArgOpt<int>( "eventidx", 0 );
  const std::string data               = args.Arg<std::string>( "data" );
  const std::string output             = args.Arg<std::string>( "output" );
  const edm::EventID event_id          = ParseEvent( args.ArgOpt<std::string>( "event", "" ) );

  fwlite::ChainEvent evt( input );
  fwlite::Handle<std::vector<reco::GenParticle> > genHandle;
  fwlite::Handle<std::vector<reco::Vertex> > vtxHandle;
  fwlite::Handle<std::vector<PileupSummaryInfo> > puHandle;

  // Moving to the event
  if( event_id.run() == 1 ){
    evt.to( event_id );
  } else {
    evt.to( event_idx );
  }

  usr::fout( "EventID [%u:%u:%u]\n"
           , evt.id().run()
           , evt.id().luminosityBlock()
           , evt.id().event() );

  puHandle.getByLabel( evt, "addPileupInfo" );
  genHandle.getByLabel( evt, "genParticles" );
  vtxHandle.getByLabel( evt, "offlinePrimaryVertices" );


  TGraph* pu1graph  = MakePileUp( GetPU( puHandle.ref(), 1 ) );
  TGraph* pum1graph = MakePileUp( GetPU( puHandle.ref(), -1 ) );
  TGraph* pugraph   = MakePileUp( GetPU( puHandle.ref() ) );
  TGraph* pvgraph   = MakePVFull( vtxHandle.ref() );
  TGraph* mpvgraph  = MakePVMain( vtxHandle.ref() );
  TGraph* gengraph  = MakeGen( genHandle.ref() );

  usr::plt::Simple1DCanvas c;

  c.PlotGraph( pvgraph,
    usr::plt::EntryText( "reco. vertex (#sum p_{T})" ),
    usr::plt::PlotType( usr::plt::scatter ),
    usr::plt::TrackY( usr::plt::tracky::both ),
    usr::plt::MarkerStyle( usr::plt::sty::mkrcircle ),
    usr::plt::MarkerSize( 0.5 ),
    usr::plt::MarkerColor( usr::plt::col::black ) );
  c.PlotGraph( mpvgraph,
    usr::plt::EntryText( "primary vertex" ),
    usr::plt::PlotType( usr::plt::scatter ),
    usr::plt::TrackY( usr::plt::tracky::both ),
    usr::plt::MarkerStyle( usr::plt::sty::mkropencircle ),
    usr::plt::MarkerSize( 0.7 ),
    usr::plt::MarkerColor( usr::plt::col::navy ) );
  c.PlotGraph( pugraph,
    usr::plt::EntryText( "truth p.u. vertex (#hat{p}_{T})" ),
    usr::plt::PlotType( usr::plt::scatter ),
    usr::plt::TrackY( usr::plt::tracky::both ),
    usr::plt::MarkerStyle( usr::plt::sty::mkrcircle ),
    usr::plt::MarkerSize( 0.3 ),
    usr::plt::MarkerColor( usr::plt::col::red ) );
  c.PlotGraph( pu1graph,
    usr::plt::PlotType( usr::plt::scatter ),
    usr::plt::TrackY( usr::plt::tracky::both ),
    usr::plt::MarkerStyle( usr::plt::sty::mkrcircle ),
    usr::plt::MarkerSize( 0.3 ),
    usr::plt::MarkerColor( usr::plt::col::red, 0.3 ) );
  c.PlotGraph( pum1graph,
    usr::plt::PlotType( usr::plt::scatter ),
    usr::plt::TrackY( usr::plt::tracky::both ),
    usr::plt::MarkerStyle( usr::plt::sty::mkrcircle ),
    usr::plt::MarkerSize( 0.3 ),
    usr::plt::MarkerColor( usr::plt::col::red, 0.3 ) );
  c.PlotGraph( gengraph,
    usr::plt::EntryText( "truth hard vertex" ),
    usr::plt::PlotType( usr::plt::scatter ),
    usr::plt::TrackY( usr::plt::tracky::both ),
    usr::plt::MarkerStyle( usr::plt::sty::mkrsquare ),
    usr::plt::MarkerSize( 0.3 ),
    usr::plt::MarkerColor( usr::plt::col::green ) );

  TRandom3 rand;
  const auto& pu = GetPU( puHandle.ref() );

  for( int i = 0; i < pu.getPU_NumInteractions(); ++i  ){
    const double z = pu.getPU_zpositions().at( i );
    const double r = 0.07 + rand.Uniform( -0.003, +0.003 );
    if( fabs( z ) < 2.5 ){
      c.Pad().WriteAtData( z, r,
        usr::fstr( "%.1lf", pu.getPU_pT_hats().at( i ) ),
        usr::plt::TextColor( usr::plt::col::red ),
        usr::plt::TextAlign( usr::plt::font::left ),
        usr::plt::TextSize( 4 ) );
    }
  }

  for( const auto& vtx : vtxHandle.ref() ){
    if( fabs( vtx.z() ) < 2.5 ){
      const double r = std::sqrt( vtx.x() * vtx.x() + vtx.y()*vtx.y() );
      c.Pad().WriteAtData( vtx.z()+0.1, r,
        usr::fstr( "%.1lf", GetSumPT( vtx ) ),
        usr::plt::TextColor( usr::plt::col::black ),
        usr::plt::TextAlign( usr::plt::font::top ),
        usr::plt::TextSize( 4 ) );
    }

  }

  c.Pad().SetYaxisMin( c.Pad().GetDataMin() /3.0*2.0 );

  c.Pad().Xaxis().SetTitle( "z [cm]" );
  c.Pad().Yaxis().SetTitle( "#rho [cm]" );
  c.Pad().DrawLuminosity( data );
  c.Pad().DrawCMSLabel( "Simulation" );
  c.Pad().WriteLine( usr::fstr( "EventID [%u:%u:%u]"
                              , evt.id().run()
                              , evt.id().luminosityBlock()
                              , evt.id().event() ) );

  c.SaveAsPDF( output );


  return 0;
}

const PileupSummaryInfo&
GetPU( const std::vector<PileupSummaryInfo>& pulist,
       const int                             bx )
{
  for( const auto& pu : pulist ){
    if( pu.getBunchCrossing() == bx ){return pu;}
  }

  return pulist.front();
}


TGraph*
MakePileUp( const PileupSummaryInfo& pu  )
{
  std::vector<double> z;
  std::vector<double> rho;

  const double rval = 0.07 + 0.005 * pu.getBunchCrossing();

  for( const auto zval : pu.getPU_zpositions() ){
    if( fabs( zval ) < 2.5 ){
      z.push_back( zval );
      rho.push_back( rval );
    }
  }

  return new TGraph( z.size(), z.data(), rho.data() );
}

TGraph*
MakePVFull( const std::vector<reco::Vertex>& vtxlist )
{
  std::vector<double> z;
  std::vector<double> rho;

  for( const auto& vtx : vtxlist  ){
    if( fabs( vtx.z() ) < 2.5 ){
      const double r = std::sqrt( vtx.x() * vtx.x() + vtx.y()*vtx.y() );
      z.push_back( vtx.z() );
      rho.push_back( r );
    }
  }

  return new TGraph( z.size(), z.data(), rho.data() );

}

TGraph*
MakePVMain( const std::vector<reco::Vertex>& vtxlist )
{
  TGraph* ans    = new TGraph( 1 );
  const double x = vtxlist.front().x();
  const double y = vtxlist.front().y();
  const double z = vtxlist.front().z();

  const double r = std::sqrt( x*x+ y*y );

  ans->SetPoint( 0, z, r );

  return ans;
}

TGraph*
MakeGen( const std::vector<reco::GenParticle>& genlist )
{
  assert( genlist.front().pdgId() == 2212 );
  assert( genlist.front().status() == 4 );

  TGraph* ans     = new TGraph( 1 );
  const auto part = genlist.front().daughter( 0 );// Getting the first daughter particle
  const double x  = part->vertex().x();
  const double y  = part->vertex().y();
  const double z  = part->vertex().z();
  const double r  = std::sqrt( x*x + y*y );
  ans->SetPoint( 0, z, r );
  return ans;
}

double
GetSumPT( const reco::Vertex& vtx )
{
  double ans = 0;

  for( auto trk = vtx.tracks_begin(); trk != vtx.tracks_end(); ++trk ){
    if( vtx.trackWeight( *trk ) > 0.5 ){
      ans += ( *trk )->pt();
    }
  }

  return ans;
}
