#include "UserUtils/Common/interface/ArgumentExtender.hpp"
#include "UserUtils/Common/interface/STLUtils.hpp"

#include "DataFormats/FWLite/interface/ChainEvent.h"
#include "DataFormats/FWLite/interface/Event.h"
#include "DataFormats/FWLite/interface/Handle.h"
#include "DataFormats/Math/interface/deltaR.h"
#include "DataFormats/PatCandidates/interface/Jet.h"
#include "DataFormats/PatCandidates/interface/MET.h"
#include "DataFormats/PatCandidates/interface/PackedCandidate.h"


#include "SimDataFormats/GeneratorProducts/interface/GenEventInfoProduct.h"
#include "SimDataFormats/GeneratorProducts/interface/LHEEventProduct.h"
#include <Pythia8/Pythia.h>

#include "EMJ/Analyze/interface/EMJObjectSelect.hpp"
#include "PhysicsTools/Heppy/interface/PdfWeightProducerTool.h"

#include "TFile.h"
#include "TTree.h"
#include <algorithm>
#include <vector>

namespace LHAPDF
{

void   initPDFSet( int nset, const std::string& filename, int member = 0 );
int    numberPDF( int nset );
void   usePDFMember( int nset, int member );
double xfx( int nset, double x, double Q, int fl );
double getXmin ( int m );
double getXmin ( int nset, int m );
double getXmax ( int m );
double getXmax ( int nset, int m );
double getQ2min ( int m );
double getQ2min ( int nset, int m );
double getQ2max ( int m );
double getQ2max ( int nset, int m );
void   setVerbosity( int v );

}

int
lhapdfPDGID( const int pdgid )
{
  return std::abs( pdgid ) == 21 ?
         0 :
         pdgid;
}


int
main( int argc, char* argv[] )
{
  usr::po::options_description desc( "PDF parton dumping" );
  desc.add_options()
    ( "input,i",
    usr::po::multivalue<std::string>(),
    "Input MINIAOD root file to generate the event display" )
    ( "input_list,f",
    usr::po::value<std::string>(),
    "Input MINIAOD root file list" )
  ;

  usr::ArgumentExtender args;
  args.AddOptions( desc );
  args.ParseOptions( argc, argv );

  fwlite::ChainEvent evt( args.ArgList<std::string>(
                            "input" ) );
  fwlite::Handle<GenEventInfoProduct>             genHandle;
  fwlite::Handle<std::vector<reco::GenParticle> > genpHandle;

  LHAPDF::initPDFSet( 1, "NNPDF31_nlo_as_0118" );
  LHAPDF::setVerbosity( 0 );
  std::unique_ptr<Pythia8::Pythia> pythia_ = std::make_unique<Pythia8::Pythia>(
    "../share/Pythia8/xmldoc",
    false );
  pythia_->settings.flag( "ProcessLevel:all", false );
  pythia_->settings.flag( "Print:quiet", true );
  pythia_->init();

  std::cout << "PDF Valid range:" //
            << "Q: " << sqrt( LHAPDF::getQ2min( 1, 0 )) << " " //
            << sqrt( LHAPDF::getQ2max( 1, 0 )) << std::endl;
  std::cout << "PDF Valid range:" //
            << "X: " << sqrt( LHAPDF::getXmin( 1, 0 )) << " " //
            << sqrt( LHAPDF::getXmax( 1, 0 )) << std::endl;

  TFile* file = TFile::Open( "PDFDump.root", "RECREATE" );
  TTree* info = new TTree( "PDFInfo", "PDFInfo" );

  double Q, x1, x2, mMed, facup, facdn, pdf1, pdf2, pdf1up, pdf2up, pdf1dn,
         pdf2dn;
  int id1, id2;

  info->Branch( "mMed", &mMed, "mMed/D" );
  info->Branch( "Q", &Q, "Q/D" );
  info->Branch( "x1", &x1, "x1/D" );
  info->Branch( "x2", &x2, "x2/D" );
  info->Branch( "id1", &id1, "id1/I" );
  info->Branch( "id2", &id2, "id2/I" );
  info->Branch( "pdf1", &pdf1 );
  info->Branch( "pdf2", &pdf2 );
  info->Branch( "pdf1up", &pdf1up );
  info->Branch( "pdf2up", &pdf2up );
  info->Branch( "pdf1dn", &pdf1dn );
  info->Branch( "pdf2dn", &pdf2dn );

  unsigned idx = 1;
  for( evt.toBegin(); !evt.atEnd(); ++evt, ++idx  ){
    usr::print_progress( "PDFDump", idx, evt.size(), 100 );
    genHandle.getByLabel( evt, "generator" );
    genpHandle.getByLabel( evt, "prunedGenParticles" );

    for( const auto& gen : *genpHandle ){
      if( gen.pdgId() == 4900001 ){
        mMed = gen.mass();
        break;
      }
    }

    Q = genHandle->pdf()->scalePDF;

    //factorization scale
    id1 = lhapdfPDGID( genHandle->pdf()->id.first );
    x1  = genHandle->pdf()->x.first;
    id2 = lhapdfPDGID( genHandle->pdf()->id.second );
    x2  = genHandle->pdf()->x.second;

    LHAPDF::usePDFMember( 1, 0 );
    const double kUp = 2.0;
    const double kDn = 0.5;
    pdf1   = LHAPDF::xfx( 1, x1, Q, id1 ) / x1;
    pdf2   = LHAPDF::xfx( 1, x2, Q, id2 ) / x2;
    pdf1up = LHAPDF::xfx( 1, x1, kUp * Q, id1 ) / x1;
    pdf2up = LHAPDF::xfx( 1, x2, kUp * Q, id2 ) / x2;
    pdf1dn = LHAPDF::xfx( 1, x1, kDn * Q, id1 ) / x1;
    pdf2dn = LHAPDF::xfx( 1, x2, kDn * Q, id2 ) / x2;

    info->Fill();
  }
  file->Write();

  return 0;
}
