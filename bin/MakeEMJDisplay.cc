#include "UserUtils/Common/interface/ArgumentExtender.hpp"
#include "UserUtils/EDMUtils/interface/ParseEDM.hpp"
#include "UserUtils/PhysUtils/interface/MCHelper.hpp"
#include "UserUtils/PlotUtils/interface/Flat2DCanvas.hpp"

#include "DataFormats/FWLite/interface/ChainEvent.h"
#include "DataFormats/FWLite/interface/Handle.h"
#include "DataFormats/HepMCCandidate/interface/GenParticle.h"
#include "DataFormats/Math/interface/deltaR.h"
#include "DataFormats/PatCandidates/interface/Jet.h"
#include "DataFormats/PatCandidates/interface/PackedCandidate.h"

#include "EMJ/Analyze/interface/EMJObjectSelect.hpp"

#include "TEllipse.h"

double WrapPhi( const double phi );
TGraph MakeGraph( const std::vector<double>& x,
                  const std::vector<double>& y );

int
main( int argc, char* argv[] )
{
  usr::po::options_description desc(
    "Emerging jet analysis tailored simplified event display" );
  desc.add_options()
    ( "input,i", usr::po::multivalue<std::string>(),
    "Input MINIAOD root file to generate the event display" )
    ( "event", usr::po::value<std::string>(),
    "event number in the format of \"LumiID:EventID\" or \"EventID\""
    "(the Run is always 1)" )
    ( "eventidx", usr::po::value<int>(),
    "The event to display as indexed in the file." )
  ;

  usr::ArgumentExtender args;
  args.AddOptions( desc );
  args.ParseOptions( argc, argv );

  // Setting up the variables
  const std::vector<std::string> input = args.ArgList<std::string>( "input" );

  const unsigned event_idx    = args.ArgOpt<int>( "eventidx", 0 );
  const edm::EventID event_id = ParseEvent( args.ArgOpt<std::string>( "event", "" ) );

  fwlite::ChainEvent evt( input );
  fwlite::Handle<std::vector<reco::GenParticle> > genHandle;
  fwlite::Handle<std::vector<pat::PackedCandidate> > pfHandle;
  fwlite::Handle<std::vector<pat::PackedCandidate> > lostHandle;
  fwlite::Handle<std::vector<pat::PackedCandidate> > lostelHandle;
  fwlite::Handle<std::vector<reco::Vertex> > vertexHandle;
  fwlite::Handle<std::vector<pat::Jet> > jetHandle;
  fwlite::Handle<std::vector<pat::Jet> > ak8jetHandle;

  // Moving to the event
  if( event_id.run() == 1 ){
    evt.to( event_id );
  } else {
    evt.to( event_idx );
  }

  usr::fout( "EventID [%u:%u:%u]\n"
           , evt.id().run()
           , evt.id().luminosityBlock()
           , evt.id().event() );

  genHandle.getByLabel( evt, "genParticles" );

  /*
     genHandle.getByLabel( evt, "prunedGenParticles" );
     try {
     jetHandle.getByLabel( evt, "slimmedJets" );
     ak8jetHandle.getByLabel( evt, "slimmedJetsAK8" );
     } catch( std::exception e ){
     std::cout << e.what() << std::endl;
     throw e;
     }
     pfHandle.getByLabel( evt, "packedPFCandidates" );
     lostHandle.getByLabel( evt, "lostTracks" );
     lostelHandle.getByLabel( evt, "lostTracks", "eleTracks" );
   */

  const auto emj_partons = FindEMJPartons( genHandle.ref() );
  std::vector<double> eta, phi;

  TH2D tracks( "tracks", "tracks", 112, -2.8, 2.8, 80, -4.0, -4.0 );

  /*
     auto FillTrack = [&tracks]( const std::vector<pat::PackedCandidate>& vec ){
                     for( const auto track : vec ){
                       if( track.charge() != 0 && track.pt() > 1.0 ){
                         tracks.Fill( track.eta()
                                    , track.phi()
                                    , track.pt() );
                       }
                     }
                   };
     FillTrack( pfHandle.ref() );
     FillTrack( lostHandle.ref() );
     FillTrack( lostelHandle.ref() );


     for( const auto jet : jetHandle.ref() ){
     if( jet.pt() > 100 ){
      eta.push_back( jet.eta() );
      phi.push_back( jet.phi() );
     }
     }

     TGraph jets = MakeGraph( eta, phi );
     eta.clear();
     phi.clear();

     for( const auto jet : ak8jetHandle.ref() ){
     if( jet.pt() > 100  ){
      eta.push_back( jet.eta() );
      phi.push_back( jet.phi() );
     }
     }

     TGraph ak8jets = MakeGraph( eta, phi );
     eta.clear();
     phi.clear();
   */
  for( const auto p : emj_partons ){
    std::cout << p->pdgId() << " " << p->eta() << " " << p->phi() << std::endl;
    if( abs( p->pdgId() )  < 6 ){
      eta.push_back( p->eta() );
      phi.push_back( p->phi() );
    }
  }

  TGraph smp = MakeGraph( eta, phi );

  eta.clear();
  phi.clear();

  for( const auto p : emj_partons ){
    if( abs( p->pdgId() )  > 4900000 ){
      eta.push_back( p->eta() );
      phi.push_back( p->phi() );
    }
  }

  TGraph darkp = MakeGraph( eta, phi );


  /// For dark mesons
  const auto dark_meson = usr::mc::FindAll( genHandle.ref(),
    usr::mc::pp( []( auto x )->bool {
    return abs( x->pdgId() ) == 4900111 || abs( x->pdgId() ) == 4900211;
  } ) );
  eta.clear();
  phi.clear();

  for( const auto p : dark_meson ){
    eta.push_back( p->eta() );
    phi.push_back( p->phi() );
  }

  TGraph darkmp = MakeGraph( eta, phi );


  usr::plt::Flat2DCanvas c;

  tracks.Fill( -2.8, 3.14,  1.0 );
  tracks.Fill( 2.8,  3.14,  1.0 );
  tracks.Fill( -2.8, -3.14, 1.0 );
  tracks.Fill( 2.8,  -3.14, 1.0 );
  // tracks.Fill(0.0,0.0,1.0);
  c.PlotHist( tracks,
    usr::plt::Plot2DF( usr::plt::box ),
    usr::plt::FillColor( usr::plt::col::white ),
    usr::plt::LineColor( usr::plt::col::white )
    );
  // usr::plt::EntryText( "" ) );
  /*
     c.Plot1DGraph( jets,
     usr::plt::PlotType( usr::plt::scatter ),
     usr::plt::MarkerColor( usr::plt::col::blue ),
     usr::plt::MarkerStyle( usr::plt::sty::mkropencircle ),
     usr::plt::LineColor( usr::plt::col::blue ),
     usr::plt::MarkerSize( 0.2 ),
     usr::plt::EntryText( "AK4 Jets" ) );
     c.Plot1DGraph( ak8jets,
     usr::plt::PlotType( usr::plt::scatter ),
     usr::plt::MarkerColor( usr::plt::col::cyan ),
     usr::plt::MarkerStyle( usr::plt::sty::mkropencircle ),
     usr::plt::LineColor( usr::plt::col::cyan ),
     usr::plt::MarkerSize( 0.2 ),
     usr::plt::EntryText( "AK8 Jets" ) );
     c.PlotHist( tracks,
     usr::plt::Plot2DF( usr::plt::box ),
     usr::plt::FillColor( usr::plt::col::green ),
     usr::plt::LineColor( usr::plt::col::green ) );
   */
  c.Plot1DGraph( darkmp,
    usr::plt::PlotType( usr::plt::scatter ),
    usr::plt::MarkerColor( usr::plt::col::orange ),
    usr::plt::MarkerStyle( usr::plt::sty::mkropencircle ),
    usr::plt::MarkerSize( 0.3 ),
    usr::plt::EntryText( "Dark meson" ) );
  c.Plot1DGraph( darkp,
    usr::plt::PlotType( usr::plt::scatter ),
    usr::plt::MarkerColor( usr::plt::col::red ),
    usr::plt::MarkerStyle( usr::plt::sty::mkropencircle ),
    usr::plt::MarkerSize( 0.3 ),
    usr::plt::EntryText( "Dark quark" ) );
  c.Plot1DGraph( smp,
    usr::plt::PlotType( usr::plt::scatter ),
    usr::plt::MarkerColor( usr::plt::col::red ),
    usr::plt::MarkerStyle( usr::plt::sty::mkrcircle ),
    usr::plt::MarkerSize( 0.3 ),
    usr::plt::EntryText( "SM quark" ) );

  c.Pad().Xaxis().SetTitle( "#eta" );
  c.Pad().Yaxis().SetTitle( "#phi" );

  /*
     for( int i = 0; i < jets.GetN(); ++i ){
     TEllipse* e = new TEllipse( jets.GetX()[i], jets.GetY()[i], 0.4 );
     e->SetLineColor( usr::plt::col::blue );
     e->SetLineStyle( usr::plt::sty::lindashed );
     e->SetFillColorAlpha( 0, 0 );
     e->Draw();
     }

     for( int i = 0; i < ak8jets.GetN(); ++i ){
     TEllipse* e = new TEllipse( ak8jets.GetX()[i], ak8jets.GetY()[i], 0.8 );
     e->SetLineColor( usr::plt::col::cyan );
     e->SetLineStyle( usr::plt::sty::lindashed );
     e->SetFillColorAlpha( 0, 0 );
     e->Draw();
     }*/

  c.SaveAsPDF( "test.pdf" );

}


double
WrapPhi( const double phi )
{
  if( fabs( phi - 3.1415926 ) < 0.8 ){
    return phi - 2 * 3.1415926;
  } else if( fabs( phi+3.4151926 ) < 0.8 ){
    return phi + 2 * 3.1415926;
  } else {
    return phi;
  }
}


TGraph
MakeGraph( const std::vector<double>& x,
           const std::vector<double>& y )
{
  TGraph ans( std::min( x.size(), y.size() ) );

  for( unsigned i = 0; i < std::min( x.size(), y.size() ); ++i ){
    ans.SetPoint( i, x[i], y[i] );
  }

  return ans;
}
