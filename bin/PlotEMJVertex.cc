#include "EMJ/Analyze/interface/EMJVertexFormat.hpp"
#include "UserUtils/Common/interface/STLUtils/OStreamUtils.hpp"
#include "UserUtils/Common/interface/STLUtils/StringUtils.hpp"
#include "UserUtils/PlotUtils/interface/Flat2DCanvas.hpp"

#include "TFile.h"
#include "TH2D.h"
#include "TLorentzVector.h"
#include "TTree.h"

class Sample
{
public:
  EMJJetFormat format;
  enum TYPE
  {
    EMJ_DARK_QUARK,
    EMJ_SM_QUARK,
    TTBAR_BJET,
    TTBAR_LIGHT,
    QCD_LEADING,
    QCD_SUBLEADING
  };
  std::map<std::string, TH2D*> hist;
  Sample( const std::string& file, const TYPE );

  ~Sample();
};


// Summary of
struct TrackInfo
{
  double sum_pt;
  double sum_ip3d;
  double sum_ipz;
  double sum_ip2d;
  double sum_ip3d_sig;
  double sum_ip2d_sig;
  double sum_chi;

  unsigned ntracks;
};

struct VertexInfo
{
  unsigned nvertex;
  double   avg_rho;
  double   avg_z;
  double   avg_mass;
  double   avg_ntracks;
  double   avg_mult;

  double max_rho;
};

struct GenInfo
{
  unsigned ngen;
  double   avg_rho;
  double   avg_mass;
  double   avg_tracks;

  double max_rho;
};

static std::string MakeXaxis( const std::string& hist );
static std::string MakeYaxis( const std::string& hist );

int
main()
{
  Sample ttbar_bjets( "VertexTree_TTbar.root", Sample::TTBAR_BJET  );
  Sample ttbar_ljets( "VertexTree_TTbar.root", Sample::TTBAR_LIGHT  );
  Sample qcd( "VertexTree_QCD.root", Sample::QCD_LEADING );

  Sample emj_ctau25_dq( "VertexTree_ctau-25.root",  Sample::EMJ_DARK_QUARK );
  Sample emj_ctau25_sm( "VertexTree_ctau-25.root",  Sample::EMJ_SM_QUARK );
  Sample emj_ctau5_dq( "VertexTree_ctau-5.root",  Sample::EMJ_DARK_QUARK );
  Sample emj_ctau5_sm( "VertexTree_ctau-5.root",  Sample::EMJ_SM_QUARK );
  Sample emj_kappa25_dq( "VertexTree_kappa-0p25.root",  Sample::EMJ_DARK_QUARK );
  Sample emj_kappa25_sm( "VertexTree_kappa-0p25.root",  Sample::EMJ_SM_QUARK );
  Sample emj_kappa18_dq( "VertexTree_kappa-0p18.root",  Sample::EMJ_DARK_QUARK );
  Sample emj_kappa18_sm( "VertexTree_kappa-0p18.root",  Sample::EMJ_SM_QUARK );

  for( const auto p : ttbar_bjets.hist ){
    const std::string hist = p.first;

    usr::plt::Flat2DCanvas c;


    // SM quarks next
    c.PlotHist( emj_ctau25_sm.hist[hist],
      usr::plt::Plot2DF( usr::plt::density ),
      usr::plt::FillColor( usr::plt::col::darkgreen, 0.7 ),
      usr::plt::MarkerColor( usr::plt::col::darkgreen, 0.7 ),
      usr::plt::MarkerStyle( usr::plt::sty::mkropensquare ),
      usr::plt::MarkerSize( 0.05 ),
      usr::plt::EntryText( "[EMJ_{c#tau=25cm}] SM" ) );
    c.PlotHist( emj_ctau5_sm.hist[hist],
      usr::plt::Plot2DF( usr::plt::density ),
      usr::plt::FillColor( usr::plt::col::forestgreen, 0.7 ),
      usr::plt::MarkerColor( usr::plt::col::forestgreen, 0.7 ),
      usr::plt::MarkerStyle( usr::plt::sty::mkropensquare ),
      usr::plt::MarkerSize( 0.05 ),
      usr::plt::EntryText( "[EMJ_{c#tau=5cm}] SM" ) );
    c.PlotHist( emj_kappa25_sm.hist[hist],
      usr::plt::Plot2DF( usr::plt::density ),
      usr::plt::FillColor( usr::plt::col::limegreen, 0.7 ),
      usr::plt::MarkerColor( usr::plt::col::limegreen, 0.7 ),
      usr::plt::MarkerStyle( usr::plt::sty::mkropensquare ),
      usr::plt::MarkerSize( 0.05 ),
      usr::plt::EntryText( "[fEMJ_{max c#tau=25cm}] SM" ) );
    c.PlotHist( emj_kappa18_sm.hist[hist],
      usr::plt::Plot2DF( usr::plt::density ),
      usr::plt::FillColor( usr::plt::col::springgreen, 0.7 ),
      usr::plt::MarkerColor( usr::plt::col::springgreen, 0.7 ),
      usr::plt::MarkerStyle( usr::plt::sty::mkropensquare ),
      usr::plt::MarkerSize( 0.05 ),
      usr::plt::EntryText( "[fEMJ_{max c#tau=100cm}] SM" ) );

    // SM model processes at bottom
    c.PlotHist( ttbar_bjets.hist[hist],
      usr::plt::Plot2DF( usr::plt::density ),
      usr::plt::FillColor( usr::plt::col::blueviolet, 0.4 ),
      usr::plt::MarkerColor( usr::plt::col::blueviolet, 0.4 ),
      usr::plt::MarkerStyle( usr::plt::sty::mkrcircle ),
      usr::plt::MarkerSize( 0.1 ),
      usr::plt::EntryText( "[t#bar{t}] b-tagged jets" ) );
    c.PlotHist( ttbar_ljets.hist[hist],
      usr::plt::Plot2DF( usr::plt::density ),
      usr::plt::FillColor( usr::plt::col::navy, 0.4 ),
      usr::plt::MarkerColor( usr::plt::col::navy, 0.4 ),
      usr::plt::MarkerStyle( usr::plt::sty::mkrsquare ),
      usr::plt::MarkerSize( 0.1 ),
      usr::plt::EntryText( "[t#bar{t}] light jets" ) );
    c.PlotHist( qcd.hist[hist],
      usr::plt::Plot2DF( usr::plt::density ),
      usr::plt::FillColor( usr::plt::col::royalblue, 0.4 ),
      usr::plt::MarkerColor( usr::plt::col::royalblue, 0.4 ),
      usr::plt::MarkerStyle( usr::plt::sty::mkrsquare ),
      usr::plt::MarkerSize( 0.1 ),
      usr::plt::EntryText( "[QCD] Leading" ) );

    // Dark quark jets placed at top
    c.PlotHist( emj_ctau25_dq.hist[hist],
      usr::plt::Plot2DF( usr::plt::density ),
      usr::plt::FillColor( usr::plt::col::red ),
      usr::plt::MarkerColor( usr::plt::col::red ),
      usr::plt::MarkerStyle( usr::plt::sty::mkropencircle ),
      usr::plt::MarkerSize( 0.05 ),
      usr::plt::EntryText( "[EMJ_{c#tau=25mm}] Dark" ) );
    c.PlotHist( emj_ctau5_dq.hist[hist],
      usr::plt::Plot2DF( usr::plt::density ),
      usr::plt::FillColor( usr::plt::col::crimson ),
      usr::plt::MarkerColor( usr::plt::col::crimson ),
      usr::plt::MarkerStyle( usr::plt::sty::mkropencircle ),
      usr::plt::MarkerSize( 0.05 ),
      usr::plt::EntryText( "[EMJ_{c#tau=5mm}] Dark" ) );
    c.PlotHist( emj_kappa25_dq.hist[hist],
      usr::plt::Plot2DF( usr::plt::density ),
      usr::plt::FillColor( usr::plt::col::tomato ),
      usr::plt::MarkerColor( usr::plt::col::tomato ),
      usr::plt::MarkerStyle( usr::plt::sty::mkropencircle ),
      usr::plt::MarkerSize( 0.05 ),
      usr::plt::EntryText( "[fEMJ_{max c#tau=25cm}] Dark" ) );
    c.PlotHist( emj_kappa18_dq.hist[hist],
      usr::plt::Plot2DF( usr::plt::density ),
      usr::plt::FillColor( usr::plt::col::orange ),
      usr::plt::MarkerColor( usr::plt::col::orange ),
      usr::plt::MarkerStyle( usr::plt::sty::mkropencircle ),
      usr::plt::MarkerSize( 0.05 ),
      usr::plt::EntryText( "[fEMJ_{max c#tau=100cm}] Dark" ) );


    c.Xaxis().SetTitle( MakeXaxis( hist ).c_str() );
    c.Yaxis().SetTitle( MakeYaxis( hist ).c_str() );

    c.SaveAsPDF( hist+".pdf" );
  }

  return 0;
}



// Defining the construction
static int       GetJetIndex( const EMJJetFormat& format, const Sample::TYPE  );
static TrackInfo JetTrackInfo( const EMJJetFormat& format
                             , const unsigned      jetidx );
static VertexInfo JetVertexInfo( const EMJJetFormat& format
                               , const unsigned      jetidx );
static GenInfo JetGenInfo( const EMJJetFormat& format
                         , const unsigned      jetidx );

Sample::Sample( const std::string& f, const TYPE m )
{
  // Definition of histograms Before opening files
  hist["TrackMult_v_ChiAvg"] = new TH2D( usr::RandomString( 6 ).c_str(), ""
                                       , 50, 0, 150, 100, 0, 100 );
  hist["TrackMult_v_IP2D"] = new TH2D( usr::RandomString( 6 ).c_str(), ""
                                     , 100, 0, 2, 50, 0, 50 );
  hist["TrackMult_v_IP3D"] = new TH2D( usr::RandomString( 6 ).c_str(), ""
                                     , 100, 0, 2, 50, 0, 50 );
  hist["IP2D_v_IP2Dsig"] = new TH2D( usr::RandomString( 6 ).c_str(), ""
                                   , 100, 0, 100, 100, 0, 2 );
  hist["IP3D_v_IP3Dsig"] = new TH2D( usr::RandomString( 6 ).c_str(), ""
                                   , 100, 0, 100, 100, 0, 2 );
  hist["TrackMult_v_NVtx"] = new TH2D( usr::RandomString( 6 ).c_str(), ""
                                     , 20, 0, 20, 100, 0, 100 );
  hist["TrackMult_v_VtxMult"] = new TH2D( usr::RandomString( 6 ).c_str(), ""
                                        , 20, 0, 20, 100, 0, 100 );
  hist["IP2D_v_VtxRho"] = new TH2D( usr::RandomString( 6 ).c_str(), ""
                                  , 200, 0, 10, 100, 0, 2 );
  hist["IP2D_v_VtxMaxRho"] = new TH2D( usr::RandomString( 6 ).c_str(), ""
                                     , 200, 0, 10, 100, 0, 2 );
  hist["VtxRho_v_VtxMult"] = new TH2D( usr::RandomString( 6 ).c_str(), ""
                                     , 20, 0, 20, 100, 0, 10 );
  hist["NVtx_v_NGenVtx"] = new TH2D( usr::RandomString( 6 ).c_str(), ""
                                   , 20, 0, 20, 15, 0, 15 );
  hist["VtxRho_v_GenVtxRho"] = new TH2D( usr::RandomString( 6 ).c_str(), ""
                                       , 100, 0, 20, 100, 0, 20 );


  TFile* file = TFile::Open( f.c_str(), "READ" );
  TTree* tree = (TTree*)file->Get( "EMJVertex/EMJVertex" );
  format.LoadBranches( tree );

  for( unsigned i = 0; i < tree->GetEntries(); ++i ){
    if( i % 100 == 0  || i == tree->GetEntries() - 1 ){
      usr::fout( "\r%s [%6d/%6d]", f, i+1, tree->GetEntries() );
    }
    tree->GetEntry( i );

    int jetidx = GetJetIndex( format, m );

    if( jetidx < 0 ){ continue; }

    TrackInfo trackinfo = JetTrackInfo( format, jetidx );
    VertexInfo vtxinfo  = JetVertexInfo( format, jetidx );
    GenInfo geninfo     = JetGenInfo( format, jetidx );

    hist["TrackMult_v_ChiAvg"]->Fill(
      trackinfo.sum_chi / trackinfo.sum_pt, trackinfo.ntracks );
    hist["TrackMult_v_IP2D"]->Fill(
      trackinfo.sum_ip2d / trackinfo.sum_pt, trackinfo.ntracks );
    hist["TrackMult_v_IP3D"]->Fill(
      trackinfo.sum_ip3d / trackinfo.sum_pt, trackinfo.ntracks );
    hist["IP2D_v_IP2Dsig"]->Fill(
      trackinfo.sum_ip2d_sig /trackinfo.sum_pt,
      trackinfo.sum_ip2d / trackinfo.sum_pt );
    hist["IP3D_v_IP3Dsig"]->Fill(
      trackinfo.sum_ip3d_sig /trackinfo.sum_pt,
      trackinfo.sum_ip3d / trackinfo.sum_pt );
    hist["TrackMult_v_NVtx"]->Fill(
      vtxinfo.nvertex, trackinfo.ntracks    );
    hist["TrackMult_v_VtxMult"]->Fill(
      vtxinfo.avg_mult, trackinfo.ntracks    );
    hist["IP2D_v_VtxRho"]->Fill(
      vtxinfo.avg_rho, trackinfo.sum_ip2d / trackinfo.sum_pt );
    hist["IP2D_v_VtxMaxRho"]->Fill(
      vtxinfo.max_rho, trackinfo.sum_ip2d / trackinfo.sum_pt );
    hist["VtxRho_v_VtxMult"]->Fill(
      vtxinfo.avg_mult, vtxinfo.avg_rho );
    hist["NVtx_v_NGenVtx"]->Fill(
      geninfo.ngen, vtxinfo.nvertex   );
    hist["VtxRho_v_GenVtxRho"]->Fill(
      geninfo.avg_rho, vtxinfo.avg_rho   );
  }

  std::cout << "Done" << std::endl;

  file->Close();
}

Sample::~Sample()
{
  for( auto p : hist ){
    delete p.second;
  }
}

std::string
MakeXaxis( const std::string& hist )
{
  if( usr::ends_with( hist, "_ChiAvg" ) ){
    return "p_{T} weighted #chi average";
  } else if( usr::ends_with( hist, "_IP2D" ) ){
    return "p_{T} weighted 2D impact parameters [mm]";
  } else if( usr::ends_with( hist, "_IP3D" ) ){
    return "p_{T} weighted 3D impact parameters [mm]";
  } else if( usr::ends_with( hist, "_IP2Dsig" ) ){
    return "p_{T} weighted 2D impact significance";
  } else if( usr::ends_with( hist, "_IP3Dsig" ) ){
    return "p_{T} weighted 3D impact significance";
  } else if( usr::ends_with( hist, "_NVtx" ) ){
    return "Number of reconstructed jet vertices";
  } else if( usr::ends_with( hist, "_VtxMult" ) ){
    return "m_{vtx} weighted Vertex Track Multiplicity";
  } else if( usr::ends_with( hist, "_VtxRho" ) ){
    return "m_{vtx} weighted vertex #rho [mm]";
  } else if( usr::ends_with( hist, "_VtxMaxRho" ) ){
    return "Maximum vertex #rho [mm]";
  } else if( usr::ends_with( hist, "_NGenVtx" ) ){
    return "Number of GEN-level vertices";
  } else if( usr::ends_with( hist, "_GenVtxRho" ) ){
    return "m_{vtx} weighted GEN-level vertex #rho [mm]";
  } else {
    return "";
  }
}


std::string
MakeYaxis( const std::string& hist )
{
  if( usr::starts_with( hist, "TrackMult" ) ){
    return "Track Multiplicity";
  }  else if( usr::starts_with( hist, "IP2D" ) ){
    return "p_{T} weighted 2D impact parameters [mm]";
  }  else if( usr::starts_with( hist, "IP3D" ) ){
    return "p_{T} weighted 3D impact parameters [mm]";
  }  else if( usr::starts_with( hist, "VtxRho" ) ){
    return "Average vertex #rho [mm]";
  }  else if( usr::starts_with( hist, "NVtx" ) ){
    return "Number of reconstructed jet verticies";
  }  else {
    return "";
  }
}

int
GetJetIndex( const EMJJetFormat& format, const Sample::TYPE t )
{
  for( unsigned i = 0; i < format.njets; ++i ){
    if( t == Sample::EMJ_DARK_QUARK && abs( format.parton_pdgid[i] ) > 4900000 ){
      return i;
    } else if( t == Sample::EMJ_SM_QUARK && abs( format.parton_pdgid[i] ) < 6 ){
      return i;
    } else if( t == Sample::TTBAR_BJET && abs( format.parton_pdgid[i] ) == 5 &&
               format.btag[i] > 0.8484 ){
      return i;
    } else if( t == Sample::TTBAR_LIGHT && abs( format.parton_pdgid[i] ) < 5 &&
               format.btag[i] < 0.8484 ){
      return i;
    } else if( t == Sample::QCD_LEADING && i == 0 ){
      return i;
    } else if( t == Sample::QCD_SUBLEADING && i == 1 ){
      return i;
    }
  }

  return -1;
}


TrackInfo
JetTrackInfo( const EMJJetFormat& format, const unsigned jetidx )
{
  TrackInfo info;

  info.sum_pt       = 0;
  info.sum_ipz      = 0;
  info.sum_ip2d     = 0;
  info.sum_ip3d     = 0;
  info.sum_ip3d_sig = 0;
  info.sum_ip2d_sig = 0;
  info.sum_chi      = 0;

  for( unsigned i = format.trackidx[jetidx];
       i < format.trackidx[jetidx] + format.ntracks[jetidx]; ++i  ){

    const double pt       = format.track_pt[i];
    const double ip3d     = format.track_ip3d[i];
    const double ip2d     = format.track_ip2d[i];
    const double ip3d_sig = format.track_ip3dsig[i];
    const double ip2d_sig = format.track_ip2dsig[i];
    const double ipz      = std::sqrt( ip3d*ip3d - ip2d*ip2d );
    const double chi      = std::sqrt( ipz*ipz/0.0001 + ip2d_sig * ip2d_sig );

    info.sum_pt       += pt;
    info.sum_ipz      += ipz * pt;
    info.sum_ip2d     += ip2d* pt;
    info.sum_ip3d     += ip3d * pt;
    info.sum_ip3d_sig += ip3d_sig * pt;
    info.sum_ip2d_sig += ip2d_sig * pt;
    info.sum_chi      += chi * pt;
  }

  info.ntracks = format.ntracks[jetidx];

  return info;
}

VertexInfo
JetVertexInfo( const EMJJetFormat& format, const unsigned jetidx )
{
  VertexInfo info;

  info.avg_rho  = 0;
  info.avg_z    = 0;
  info.avg_mass = 0;
  info.avg_mult = 0;
  info.max_rho  = 0;


  for( unsigned i = format.vertexidx[jetidx];
       i < format.vertexidx[jetidx] + format.nvertex[jetidx]; ++i ){
    const double x    = format.vertex_x[i];
    const double y    = format.vertex_y[i];
    const double z    = format.vertex_z[i];
    const double m    = format.vertex_mass[i];
    const double mult = format.vertex_mult[i];

    const double rho = std::sqrt( x*x + y*y );

    info.avg_mass += m;
    info.avg_rho  += rho * m;
    info.avg_mult += mult * m;
    info.avg_z    += z * m;
    info.max_rho   = std::max( info.max_rho, rho );
  }

  info.nvertex = format.nvertex[jetidx];

  info.avg_rho  /= info.avg_mass;
  info.avg_mult /= info.avg_mass;
  info.avg_z    /= info.avg_mass;
  info.avg_mass /= info.nvertex;

  return info;
}


GenInfo
JetGenInfo( const EMJJetFormat& format, const unsigned jetidx )
{
  GenInfo info;

  info.avg_rho    = 0;
  info.max_rho    = 0;
  info.avg_mass   = 0;
  info.avg_tracks = 0;

  info.ngen = format.ngen[jetidx];


  unsigned genidx = 0;

  for( unsigned i = 0; i < jetidx; ++i ){
    genidx += format.ngen[i];
  }




  for( unsigned i = 0; i < genidx + format.ngen[jetidx]; ++i ){
    const double x   = format.gen_decay_x[i];
    const double y   = format.gen_decay_y[i];
    const double z   = format.gen_decay_z[i];
    const double rho = std::sqrt( x*x + y*y );

    const double m = format.gen_vis_mass[i];

    info.avg_rho  += rho *m;
    info.avg_mass += m;


    info.max_rho = std::max( info.max_rho, rho );
  }


  info.avg_rho  /= info.avg_mass;
  info.avg_mass /= info.ngen;

  // assert( !(info.ngen != 0 && info.avg_rho != 0) );

  return info;
}
