#include "UserUtils/Common/interface/ArgumentExtender.hpp"
#include "UserUtils/EDMUtils/interface/ParseEDM.hpp"
#include "UserUtils/PhysUtils/interface/MCHelper.hpp"
#include "UserUtils/PlotUtils/interface/Flat2DCanvas.hpp"

#include "EMJ/Analyze/interface/EMJGenHelper.hpp"

#include "DataFormats/FWLite/interface/ChainEvent.h"
#include "DataFormats/FWLite/interface/Handle.h"
#include "DataFormats/HepMCCandidate/interface/GenParticle.h"
#include "DataFormats/PatCandidates/interface/Jet.h"
#include "DataFormats/VertexReco/interface/Vertex.h"


#include "Math/VectorUtil.h"

#include <iostream>


int
main( int argc, char* argv[] )
{
  usr::po::options_description desc(
    "Emerging Jets Analysis Dumping BJets indices in MINIAOD file" );
  desc.add_options()
    ( "input,i", usr::po::multivalue<std::string>(),
    "GENSIM Files for information extraction" )
  ;

  usr::ArgumentExtender args;
  args.AddOptions( desc );
  args.ParseOptions( argc, argv );

  // Setting up the variables
  const std::vector<std::string> input = args.ArgList<std::string>( "input" );

  fwlite::ChainEvent evt( input );
  fwlite::Handle<std::vector<reco::GenParticle> > genHandle;


  TH2D dark_q("dark_q","dark_q", 80,0,0.8,100,0,60);
  TH2D dm_decay("dm_decay", "dm_decay", 80,0,0.8,100,0,60);

  for( evt.toBegin(); !evt.atEnd(); ++evt ){
    genHandle.getByLabel( evt, "genParticles" );

    const usr::mc::ParticleParser pp(
      []( const reco::Candidate* x )->bool {
      if( x->numberOfMothers() != 1 ){ return false; }
      if( !IsHVMediator( x->mother( 0 ) ) ){ return false; }
      return IsHVQuark( x );
      });

    const auto dark_q_list = usr::mc::FindAll( genHandle.ref(), usr::mc::pp(
      []( const reco::Candidate* x )->bool {
      if( x->numberOfMothers() != 1 ){ return false; }
      if( !IsHVMediator( x->mother( 0 ) ) ){ return false; }
      return IsHVQuark( x );
      })
      );


    for( const auto& dark_q : dark_q_list ){
      const auto hs_vertex = dark_q->vertex();
      const auto dq_vec = dark_q->p4().Vect();
      // Three vector part of dark quark vector, for "ideal" jet axis.

      const auto last        = usr::mc::GetLastInChain( dark_q );
      const auto dark_mesons = usr::mc::FindDecendants( dark_q, usr::mc::pp(
        [] ( const reco::Candidate* x )->bool {
          if( !IsHVMeson(x) ){return false; }
          for( unsigned i = 0 ; i < x->numberOfMothers() ; ++i ){
            if( IsHVMeson(x->mother(i))){return false; }
          }
          return true;
      } ));

      const auto sm_prod = usr::mc::FindDecendants( dark_q, usr::mc::pp(
        [](const reco::Candidate* x )->bool{
          if( abs(x->pdgId()>=6) ){return false; }
          for( unsigned i = 0 ; i < x->numberOfMothers() ; ++i ){
            if( IsHVMeson(x->mother(i))){ return true;}
          }
          return false;
        }));

      // Running over the dark mesons.
      for(const auto&  dm : dark_mesons ){
        const auto p = dm->daughter(0)->vertex() - hs_vertex;

        dm_decay.Fill( ROOT::Math::VectorUtil::DeltaR(p,dq_vec),
                       sqrt(ROOT::Math::VectorUtil::ProjVector(p,dq_vec).mag2())
                       );

      }
    }
  }

  usr::plt::Flat2DCanvas c;

  c.PlotHist( dm_decay, usr::plt::Plot2DF( usr::plt::density ) );

  c.SaveAsPDF("test.pdf");

  return 0;

}
