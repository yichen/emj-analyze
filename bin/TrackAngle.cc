#include "UserUtils/Common/interface/ArgumentExtender.hpp"
#include "UserUtils/Common/interface/STLUtils.hpp"

#include "DataFormats/FWLite/interface/ChainEvent.h"
#include "DataFormats/FWLite/interface/Event.h"
#include "DataFormats/FWLite/interface/Handle.h"
#include "DataFormats/Math/interface/deltaR.h"
#include "DataFormats/PatCandidates/interface/Jet.h"
#include "DataFormats/PatCandidates/interface/MET.h"
#include "DataFormats/PatCandidates/interface/PackedCandidate.h"
#include "DataFormats/TrackReco/interface/Track.h"


#include "SimDataFormats/GeneratorProducts/interface/GenEventInfoProduct.h"
#include "SimDataFormats/GeneratorProducts/interface/LHEEventProduct.h"
#include <Pythia8/Pythia.h>

#include "EMJ/Analyze/interface/EMJObjectSelect.hpp"
#include "PhysicsTools/Heppy/interface/PdfWeightProducerTool.h"

#include "TFile.h"
#include "TTree.h"
#include <algorithm>
#include <vector>

int
main( int argc, char* argv[] )
{
  usr::po::options_description desc( "PDF parton dumping" );
  desc.add_options()
    ( "input,i",
    usr::po::multivalue<std::string>(),
    "Input AOD root file to generate the event display" )
    ( "input_list,f",
    usr::po::value<std::string>(),
    "Input AOD root file list" )
  ;
  usr::ArgumentExtender args;
  args.AddOptions( desc );
  args.ParseOptions( argc, argv );

  fwlite::ChainEvent evt( args.ArgList<std::string>(
                            "input" ) );

  fwlite::Handle<std::vector<reco::Track> > trackHandle;

  unsigned evt_idx = 0;
  for( evt.toBegin(); !evt.atEnd() && evt_idx < 10; ++evt, ++evt_idx ){
    trackHandle.getByLabel( evt, "generalTracks" );

    for( const auto& trk : *trackHandle ){
      std::cout << trk.pt() << " " << trk.eta() << " " << trk.residuals().pullX(
        0 )                                                                        //
                << std::endl;
    }
    std::cout << std::endl;
  }

  return 0;
}
