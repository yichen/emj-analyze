#include "UserUtils/Common/interface/ArgumentExtender.hpp"
#include "UserUtils/EDMUtils/interface/ParseEDM.hpp"
#include "UserUtils/PlotUtils/interface/Simple1DCanvas.hpp"

#include "DataFormats/FWLite/interface/ChainEvent.h"
#include "DataFormats/FWLite/interface/Handle.h"
#include "DataFormats/HepMCCandidate/interface/GenParticle.h"
#include "DataFormats/PatCandidates/interface/PackedCandidate.h"
#include "DataFormats/PatCandidates/interface/PackedGenParticle.h"
#include "DataFormats/TrackReco/interface/Track.h"
#include "DataFormats/VertexReco/interface/Vertex.h"
#include "EMJ/Analyze/interface/EMJObjectSelect.hpp"

#include "TRandom3.h"
#include <iostream>
#include <vector>

int
main( int argc, char* argv[] )
{
  usr::po::options_description desc(
    "Printing a list of tracks with a specific pattern" );
  desc.add_options()
    ( "input,i",
    usr::po::multivalue<std::string>(),
    "Input MINIAOD root file to generate the event display" )
    ( "maxevents,m",
    usr::po::defvalue<unsigned>( -1 ),
    "Maximum number of events to print" )
  ;
  usr::ArgumentExtender args;
  args.AddOptions( desc );
  args.ParseOptions( argc, argv );

  // Setting up the variables
  const auto     input     = args.ArgList<std::string>( "input" );
  const unsigned maxevents = args.Arg<unsigned>( "maxevents" );

  fwlite::ChainEvent                              evt( input );
  fwlite::Handle<std::vector<pat::PackedGenParticle> > genHandle;

  unsigned i = 0;
  for( evt.toBegin() ; !evt.atEnd() && i < maxevents ; ++evt, ++i ){
    genHandle.getByLabel( evt, "packedGenParticles" );

    std::cout << "Current event" << std::endl;
    for( const auto& gen : genHandle.ref() ){
      if( gen.status() != 1 ){continue;}
      std::cout << gen.vx() << " " << gen.vy() << " " << gen.vz() << std::endl;
    }
  }

  return 0;
}
