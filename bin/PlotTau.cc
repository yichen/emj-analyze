#include "UserUtils/PlotUtils/interface/Simple1DCanvas.hpp"

#include "TGraph.h"
#include <fstream>
#include <sstream>

int main( int argc, char** argv )
{
  const std::string infile  = argv[1];
  const std::string outfile = argv[2];
  const std::string type    = argv[3];
  std::string line;
  std::ifstream fin( infile, std::ios::in );
  std::vector<double> mass;
  std::vector<double> pi0;
  std::vector<double> pic;
  std::vector<double> k0;
  std::vector<double> kc;

  while( std::getline( fin, line ) ){
    double m, p0, pc, k1, k2;
    std::stringstream ss( line );
    ss >> m >> p0 >> pc >> k1 >> k2;
    mass.push_back( m );
    pi0.push_back( p0 );
    pic.push_back( pc );
    k0.push_back( k1 );
    kc.push_back( k2 );
  }

  TGraph g_pi0( mass.size(), mass.data(), pi0.data() );
  TGraph g_pic( mass.size(), mass.data(), pic.data() );
  TGraph g_k0( mass.size(), mass.data(), k0.data() );
  TGraph g_kc( mass.size(), mass.data(), kc.data() );

  usr::plt::Simple1DCanvas c;

  const std::string sm = type == "up" ? "uct"  : "dsb";

  c.PlotGraph( g_pi0,
    usr::plt::PlotType( usr::plt::simplefunc ),
    usr::plt::TrackY( usr::plt::tracky::both ),
    usr::plt::EntryText( "#pi^{0}_{dark} #rightarrow q#bar{q}" ),
    usr::plt::LineColor( usr::plt::col::red ) );
  c.PlotGraph( g_pic,
    usr::plt::PlotType( usr::plt::simplefunc ),
    usr::plt::EntryText(
      usr::fstr( "#pi^{#pm}_{dark} #rightarrow %c#bar{%c}", sm[0], sm[1] ) ),
    usr::plt::LineColor( usr::plt::col::orange ) );
  c.PlotGraph( g_k0,
    usr::plt::PlotType( usr::plt::simplefunc ),
    usr::plt::EntryText(
      usr::fstr("K^{0}_{dark} #rightarrow %c#bar{%c}", sm[0], sm[2] )),
    usr::plt::LineColor( usr::plt::col::blue ) );
  c.PlotGraph( g_kc,
    usr::plt::PlotType( usr::plt::simplefunc ),
    usr::plt::EntryText(
      usr::fstr("K^{0}_{dark} #rightarrow %c#bar{%c}", sm[1], sm[2] ) ),
    usr::plt::LineColor( usr::plt::col::cyan ) );


  c.Pad().Xaxis().SetTitle( "Dark meson mass [GeV]" );
  c.Pad().Yaxis().SetTitle( "Meson lifetime c#tau [mm]" );
  c.SetLogy( 1 );
  c.SaveAsPDF( outfile );
}
