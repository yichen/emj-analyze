#include "UserUtils/Common/interface/ArgumentExtender.hpp"
#include "UserUtils/EDMUtils/interface/ParseEDM.hpp"
#include "UserUtils/PhysUtils/interface/MCHelper.hpp"
#include "UserUtils/PlotUtils/interface/Simple1DCanvas.hpp"

#include "DataFormats/FWLite/interface/ChainEvent.h"
#include "DataFormats/FWLite/interface/Handle.h"
#include "DataFormats/HepMCCandidate/interface/GenParticle.h"
#include "DataFormats/TrackReco/interface/Track.h"
#include "DataFormats/VertexReco/interface/Vertex.h"

#include "EMJ/Analyze/interface/EMJObjectSelect.hpp"

#include "TH1D.h"


int
main( int argc, char* argv[] )
{
  usr::po::options_description desc(
    "Emerging jet analysis tailored simplified event display" );
  desc.add_options()
    ( "input,i", usr::po::multivalue<std::string>(), "Input AOD root file to run the VertexPT2 filter" )
  ;

  usr::ArgumentExtender args;
  args.AddOptions( desc );
  args.ParseOptions( argc, argv );

  // Setting up the variables
  const std::vector<std::string> input = args.ArgList<std::string>( "input" );

  fwlite::ChainEvent evt( input );
  fwlite::Handle<std::vector<reco::Vertex> > vertexHandle;
  fwlite::Handle <std::vector<reco::GenParticle> > genHandle;
  fwlite::Handle<std::vector<reco::Track> > trackHandle;

  unsigned pass  = 0;
  unsigned total = 0;

  TH1D GoodReco( "GoodReco", "", 40, 0, 1 );
  TH1D BadReco( "BadReco", "", 40, 0, 1 );


  for( evt.toBegin(); !evt.atEnd(); ++evt ){
    vertexHandle.getByLabel( evt, "offlinePrimaryVertices" );
    genHandle.getByLabel( evt, "genParticles" );
    trackHandle.getByLabel( evt, "generalTracks" );
    std::cout << "Event: " << total  << "/" << evt.size() << std::endl;

    total++;
    unsigned max_idx    = 0;
    unsigned max_pt2sum = 0;

    // PT^2 sum filter
    for( unsigned i = 0; i < vertexHandle->size(); ++i ){
      const auto& vertex = vertexHandle->at( i );

      double pt2sum = 0;

      for( auto it = vertex.tracks_begin(); it != vertex.tracks_end(); ++it ){
        const auto track = *it;
        if( vertex.trackWeight( track ) > 0.5 ){
          pt2sum += track->pt() * track->pt();
        }
      }

      if( pt2sum > max_pt2sum ){
        max_pt2sum = pt2sum;
        max_idx    = i;
      }
    }

    if( max_idx != 0 ){
      std::cout << evt.id().run() << ":"
                << evt.id().luminosityBlock() << ":"
                << evt.id().event() << "   "
                << max_idx << std::endl;
      continue;
    } else {
      pass++;
    }

    // Filtering on the first vertex in event
    const auto& vertex = vertexHandle.ref().front();
    if( vertex.z() > 15 ){ continue; }
    if( vertex.isFake() ){ continue; }

    const double gen_z = usr::mc::GetGenVertex( genHandle.ref() ).z();
    const double pv_z  = vertex.z();

    double n_prompt = 0;
    double n_tracks = 0;

    for( const auto track : trackHandle.ref() ){
      // Basic track quality filters
      if( track.pt() < 1.0 ){ continue;  }
      if( !track.quality( reco::TrackBase::highPurity ) ){ continue; }


      n_tracks = n_tracks + 1;
      if( fabs( track.referencePoint().z() - pv_z ) < 0.01 ){
        n_prompt = n_prompt + 1;
      }
    }


    if( fabs( gen_z  - pv_z ) < 0.01  ){
      GoodReco.Fill( n_prompt / n_tracks );
    } else {
      BadReco.Fill( n_prompt/ n_tracks );
    }
  }

  std::cout << pass << "/" << total << std::endl;

  usr::plt::Simple1DCanvas c;
  c.PlotHist( GoodReco,
    usr::plt::EntryText( "Well reco." ),
    usr::plt::PlotType( usr::plt::hist ),
    usr::plt::LineColor( usr::plt::col::blue ) );
  c.PlotHist( BadReco,
    usr::plt::EntryText( "Poorly reco." ),
    usr::plt::PlotType( usr::plt::hist ),
    usr::plt::LineColor( usr::plt::col::red ) );
  c.Pad().SetLogy(1);

  c.SaveAsPDF( "MYCOMPARE.pdf" );

  return 0;

}
