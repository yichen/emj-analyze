#include "UserUtils/Common/interface/ArgumentExtender.hpp"
#include "UserUtils/PlotUtils/interface/Simple1DCanvas.hpp"

#include "TFile.h"
#include "TH1D.h"

int
main( int argc, char* argv[] )
{
  usr::po::options_description
    desc( "Plotting the decay flavor summary for a dark quark" );
  desc.add_options()
    ( "input,i", usr::po::reqvalue<std::string>(), "Input ROOT file" )
    ( "hist,h", usr::po::defvalue<std::string>( "GenHistogram/Summary" ),
    "Location of histogram in root file" )
    ( "output,o", usr::po::defvalue<std::string>( "DecaySummary.pdf" ),
    "Output PDF file" )
    ( "tag,t", usr::po::defvalue<std::string>( "" ),
    "Additional string to display on the plot" )
  ;

  usr::ArgumentExtender args;
  args.AddOptions( desc );
  args.ParseOptions( argc, argv );

  TFile* file   = TFile::Open( args.Arg<std::string>( "input" ).c_str() );
  TH1D* summary = (TH1D*)( file->Get( args.Arg<std::string>( "hist" ).c_str() ) );
  TH1D dq( "DQ",     "", 32, 0, 32 );
  TH1D dm( "DM",     "", 32, 0, 32 );
  TH1D smq( "SMQ",   "", 32, 0, 32 );
  TH1D smm( "SMM",   "", 32, 0, 32 );
  TH1D ss( "STABLE", "", 32, 0, 32 );

  for( unsigned i = 1; i <= 31; ++i  ){
    if( i <= 3 ){
      dq.SetBinContent( i, summary->GetBinContent( i ) );
    } else if( i <= 7 ){
      dm.SetBinContent( i, summary->GetBinContent( i ) );
    } else if( i <= 14 ){
      smq.SetBinContent( i, summary->GetBinContent( i ) );
    } else if( i <= 24 ){
      smm.SetBinContent( i, summary->GetBinContent( i ) );
    } else if( i <= 32 ){
      ss.SetBinContent( i, summary->GetBinContent( i ) );
    }
  }

  usr::plt::Simple1DCanvas c = usr::plt::Simple1DCanvas(
    usr::plt::len::a4paperheight(),
    usr::plt::len::a4paperheight() /2 );

  c.PlotHist( dq,
    usr::plt::PlotType( usr::plt::hist ),
    usr::plt::FillColor( usr::plt::col::cyan ),
    usr::plt::LineColor( usr::plt::col::cyan ) );
  c.PlotHist( dm,
    usr::plt::PlotType( usr::plt::hist ),
    usr::plt::FillColor( usr::plt::col::orange ),
    usr::plt::LineColor( usr::plt::col::orange ) );
  c.PlotHist( smq,
    usr::plt::PlotType( usr::plt::hist ),
    usr::plt::FillColor( usr::plt::col::limegreen ),
    usr::plt::LineColor( usr::plt::col::limegreen ) );
  c.PlotHist( smm,
    usr::plt::PlotType( usr::plt::hist ),
    usr::plt::FillColor( usr::plt::col::pink ),
    usr::plt::LineColor( usr::plt::col::pink ) );
  c.PlotHist( ss,
    usr::plt::PlotType( usr::plt::hist ),
    usr::plt::FillColor( usr::plt::col::violet, 0.2 ),
    usr::plt::LineColor( usr::plt::col::violet, 0.2 ) );

  auto SetLabel = [&c]( const int bin, const std::string& text )->void {
                    const double x_pos = c.Xaxis().GetBinCenter( bin );
                    const double y_pos = c.Pad().GetDataMin() *1.3;
                    c.Pad().WriteAtData( x_pos, y_pos,
                      text,
                      usr::plt::TextAngle( 90 ),
                      usr::plt::TextSize( 8 ),
                      usr::plt::TextAlign( usr::plt::align::center_left )
                      );
                  };

  c.Yaxis().SetTitle( "Number of Particles" );
  c.Xaxis().SetTitle( "Flavor group" );
  c.Xaxis().SetLabelSize( 0 );
  c.DrawCMSLabel( "Simulation" );
  c.DrawLuminosity( args.Arg<std::string>( "tag" ) );


  SetLabel( 1,         "Q_{dark,1}" );
  SetLabel( 2,         "Q_{dark,2}" );
  SetLabel( 3,         "Q_{dark,3}" );

  SetLabel( 4,         "Dark #pi^{0}" );
  SetLabel( 5,         "Dark #pi^{#pm}" );
  SetLabel( 6,         "Dark K^{#pm}" );
  SetLabel( 7,         "Dark K^{#pm}" );

  SetLabel( 1+ 7,      "down"    );
  SetLabel( 1+ 8,      "up"      );
  SetLabel( 1+ 9,      "strange" );
  SetLabel( 1+ 10,     "charm"   );
  SetLabel( 1+ 11,     "bottom"  );
  SetLabel( 1+ 12,     "top"     );
  SetLabel( 1+ 13,     "gluon"   );

  SetLabel( 1+ 0 + 14, "Light meson" );
  SetLabel( 1+ 1 + 14, "s meson" );
  SetLabel( 1+ 2 + 14, "c meson" );
  SetLabel( 1+ 3 + 14, "b meson" );
  SetLabel( 1+ 4 + 14, "proton" );
  SetLabel( 1+ 5 + 14, "neutron" );
  SetLabel( 1+ 6 + 14, "light bayron" );
  SetLabel( 1+ 7 + 14, "s baryon" );
  SetLabel( 1+ 8 + 14, "c baryon" );
  SetLabel( 1+ 9 + 14, "b baryon" );

  SetLabel( 1+ 0 + 24, "#pi^{#pm}" );
  SetLabel( 1+ 1 + 24, "K^{0}_{L}" );
  SetLabel( 1+ 2 + 24, "K^{#pm}" );
  SetLabel( 1+ 3 + 24, "proton" );
  SetLabel( 1+ 4 + 24, "neutron" );
  SetLabel( 1+ 5 + 24, "electron" );
  SetLabel( 1+ 6 + 24, "muon" );
  SetLabel( 1+ 7 + 24, "others" );

  c.Pad().WriteAtData( 2, usr::plt::GetYmax( dq ),
    "Dark quarks",
    usr::plt::TextSize( 12 ),
    usr::plt::TextAlign( usr::plt::align::bottom_center ) );

  c.Pad().DrawVLine( 3, usr::plt::LineColor( usr::plt::col::darkgray ),
    usr::plt::LineStyle( usr::plt::sty::lindotted ) );

  c.Pad().WriteAtData( 5.5, usr::plt::GetYmax( dm ),
    "Dark mesons",
    usr::plt::TextSize( 12 ),
    usr::plt::TextAlign( usr::plt::align::bottom_center ) );

  c.Pad().DrawVLine( 7, usr::plt::LineColor( usr::plt::col::darkgray ),
    usr::plt::LineStyle( usr::plt::sty::lindotted ) );

  c.Pad().WriteAtData( 10, usr::plt::GetYmax( smq ),
    "m_{D}#rightarrow SM q/g",
    usr::plt::TextSize( 12 ),
    usr::plt::TextAlign( usr::plt::align::bottom_center ) );

  c.Pad().DrawVLine( 14, usr::plt::LineColor( usr::plt::col::darkgray ),
    usr::plt::LineStyle( usr::plt::sty::lindotted ) );

  c.Pad().WriteAtData( 18.5, usr::plt::GetYmax( smm ),
    "m_{D}#rightarrow meson_{SM}",
    usr::plt::TextSize( 12 ),
    usr::plt::TextAlign( usr::plt::align::bottom_center ) );

  c.Pad().DrawVLine( 24, usr::plt::LineColor( usr::plt::col::darkgray ),
    usr::plt::LineStyle( usr::plt::sty::lindotted ) );

  c.Pad().WriteAtData( 27.5, usr::plt::GetYmax( ss ),
    "stable",
    usr::plt::TextSize( 12 ),
    usr::plt::TextAlign( usr::plt::align::bottom_center ) );


  c.SetLogy( kTRUE );
  c.SaveAsPDF( args.Arg<std::string>( "output" ) );
  c.TCanvas_().SaveAs( ( "old" + args.Arg<std::string>( "output" ) ).c_str() );

  return 0;
}
