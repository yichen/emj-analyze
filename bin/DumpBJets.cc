#include "UserUtils/Common/interface/ArgumentExtender.hpp"
#include "UserUtils/EDMUtils/interface/ParseEDM.hpp"
#include "UserUtils/PhysUtils/interface/MCHelper.hpp"
#include "UserUtils/PlotUtils/interface/Simple1DCanvas.hpp"

#include "DataFormats/FWLite/interface/ChainEvent.h"
#include "DataFormats/FWLite/interface/Handle.h"
#include "DataFormats/HepMCCandidate/interface/GenParticle.h"
#include "DataFormats/PatCandidates/interface/Jet.h"
#include "DataFormats/VertexReco/interface/Vertex.h"

#include <iostream>


int
main( int argc, char* argv[] )
{
  usr::po::options_description desc(
    "Emerging Jets Analysis Dumping BJets indices in MINIAOD file" );
  desc.add_options()
    ( "input,i", usr::po::multivalue<std::string>(), "MINIAOD Events for dumping information" )
  ;

  usr::ArgumentExtender args;
  args.AddOptions( desc );
  args.ParseOptions( argc, argv );

  // Setting up the variables
  const std::vector<std::string> input = args.ArgList<std::string>( "input" );

  fwlite::ChainEvent evt( input );

  for( evt.toBegin(); !evt.atEnd(); ++evt ){
    fwlite::Handle<std::vector<pat::Jet> > jetHandle;
    jetHandle.getByLabel( evt, "slimmedJets" );
    std::cout << evt.id().run() << ":"
              << evt.id().luminosityBlock() << ":"
              << evt.id().event() << "  |  " << std::flush;

    for( unsigned i = 0; i < jetHandle.ref().size(); ++i ){
      const auto& jet = jetHandle.ref().at( i );
      if( !jet.genParton() ){ continue; }
      if( fabs( jet.genParton()->pdgId() ) != 5 ){ continue; }

      std::cout << " " << i << std::flush;
    }

    std::cout << std::endl;
  }

  return 0;
}
