#include "UserUtils/EDMUtils/interface/EDHistogramAnalyzer.hpp"

#include "DataFormats/HepMCCandidate/interface/GenParticle.h"
#include "DataFormats/Math/interface/deltaR.h"
#include "DataFormats/PatCandidates/interface/Jet.h"
#include "DataFormats/PatCandidates/interface/PackedCandidate.h"

// For tracking information
#include "FWCore/Framework/interface/ESHandle.h"
#include "TrackingTools/IPTools/interface/IPTools.h"
#include "TrackingTools/Records/interface/TransientTrackRecord.h"
#include "TrackingTools/TransientTrack/interface/TransientTrackBuilder.h"

#include "DataFormats/GeometrySurface/interface/Line.h"
#include "EMJ/Analyze/interface/EMJObjectSelect.hpp"

#include "TLorentzVector.h"

class EMJVariablesHist : public usr::EDHistogramAnalyzer
{
public:
  explicit EMJVariablesHist( const edm::ParameterSet& );
  ~EMJVariablesHist(){}

  static void fillDescriptions( edm::ConfigurationDescriptions& descriptions );

private:
  void beginJob() override;
  void
       doBeginRun_( const edm::Run&, const edm::EventSetup& ) override {}
  void analyze( const edm::Event&, const edm::EventSetup& ) override;
  void
  doEndRun_( const edm::Run&, const edm::EventSetup& ) override {}
  void
  endJob() override {}

  // ----------member data ---------------------------
  // tokens
  edm::EDGetToken tok_ak4jet;
  edm::EDGetToken tok_pfcand;
  edm::EDGetToken tok_lost;
  edm::EDGetToken tok_lostel;
  edm::EDGetToken tok_disp;
  edm::EDGetToken tok_pv;
  edm::EDGetToken tok_gen;

  bool RunJetVar( const pat::Jet&                          jet,
                  const reco::Vertex&                      pv,
                  const std::vector<reco::Track>&          tracks,
                  const std::vector<reco::TransientTrack>& ttracks,
                  const std::string&                       prefix  );

};

//
// constructors and destructor
//
EMJVariablesHist::EMJVariablesHist( const edm::ParameterSet& iConfig ) :
  usr::EDHistogramAnalyzer( iConfig ),
  tok_ak4jet( GetToken<std::vector<pat::Jet> >( "AK4Jet" ) ),
  tok_pfcand(  GetToken<std::vector<pat::PackedCandidate> >( "PFCandidate" ) ),
  tok_lost( GetToken<std::vector<pat::PackedCandidate> >( "LostTracks" ) ),
  tok_lostel( GetToken<std::vector<pat::PackedCandidate> >( "LostEleTracks" ) ),
  tok_disp( GetToken<std::vector<reco::Track> >( "DisplacedTracks" ) ),
  tok_pv( GetToken<std::vector<reco::Vertex> >( "PrimaryVertex" ) ),
  tok_gen( GetToken<std::vector<reco::GenParticle> >( "GenPart" ) )
{
}

void
EMJVariablesHist::beginJob()
{
  BookHist1D( "PVTrackF", 100, 0, 1 );

  for( const std::string& prefix : {"DQ_", "SM_", "PU_" } ){
    BookHist1D( prefix + "DN",        100, 0,  100 );
    BookHist1D( prefix + "LogIP2D",   24,  -5, 1   );
    BookHist1D( prefix + "NumTk",     50,  0,  50  );
    BookHist1D( prefix + "Alpha3D",   50,  0,  1   );
    BookHist1D( prefix + "AvgTkPt",   100, 0,  20  );
    BookHist1D( prefix + "TkPtFrac",  50,  0,  1.5 );
    BookHist1D( prefix + "ClosestDR", 30,  0,  0.6 );
    BookHist1D( prefix + "JetIP3D",   100, 0,  50  );
    BookHist1D( prefix + "AvgDN",     100, 0,  50  );
    BookHist1D( prefix + "PTAvgDN",   100, 0,  50  );
  }
}

// Custom analysis helper functions

static std::vector<reco::Track> MakeTrackedCandidates(
  const reco::Vertex&                      pv,
  const std::vector<pat::PackedCandidate>& source1,
  const std::vector<pat::PackedCandidate>& source2,
  const std::vector<pat::PackedCandidate>& source3,
  const std::vector<reco::Track>&          source4,
  const TransientTrackBuilder&             ttBuilder );

void
EMJVariablesHist::analyze( const edm::Event&      event,
                           const edm::EventSetup& setup )
{
  edm::Handle<std::vector<pat::PackedCandidate> > candHandle;
  edm::Handle<std::vector<pat::PackedCandidate> > trackHandle;
  edm::Handle<std::vector<pat::PackedCandidate> > eleTrackHandle;
  edm::Handle<std::vector<reco::Track> > dtrackHandle;
  edm::Handle<std::vector<reco::GenParticle> > genHandle;
  edm::Handle<std::vector<pat::Jet> > jetHandle;
  edm::Handle<std::vector<reco::Vertex> > pvHandle;

  event.getByToken( tok_pfcand, candHandle );
  event.getByToken( tok_lost,   trackHandle );
  event.getByToken( tok_lostel, eleTrackHandle );
  event.getByToken( tok_gen,    genHandle );
  event.getByToken( tok_ak4jet, jetHandle );
  event.getByToken( tok_pv,     pvHandle );
  event.getByToken( tok_disp,   dtrackHandle );

  edm::ESHandle<TransientTrackBuilder> ttBuilder;
  setup.get<TransientTrackRecord>().get( "TransientTrackBuilder", ttBuilder );

  const int pv_idx = GetPrimaryVertex( *pvHandle );
  if( pv_idx < 0 ){ return; }
  const auto pv    = pvHandle->at( pv_idx );
  const auto darkq = FindDarkQuark( *genHandle );
  const auto smq   = FindSMQuark( *genHandle );

  const std::vector<reco::Track> tracklist
    = MakeTrackedCandidates( pv, *candHandle, *trackHandle, *eleTrackHandle
                           , *dtrackHandle,  *ttBuilder );
  const std::vector<reco::TransientTrack> transTrackList
    = MakeTTList( tracklist, *ttBuilder );

  //Hist( "PVTrackF" ).Fill( PVTrackFraction( pv, transTrackList ) );


  int dq_index = -1;
  double dq_DR = 1000;
  int sm_index = -1;
  double sm_DR = 1000;
  int pu_index = -1;


  for( unsigned i = 0; i < jetHandle->size(); ++i ){
    const auto& jet = jetHandle->at( i );

    if( !IsGoodJet( jet, nullptr ) ){ continue; }
    if( darkq && deltaR( jet, *darkq ) < dq_DR ){
      dq_index = i;
      dq_DR    = deltaR( jet, *darkq );
    } else if( smq && deltaR( jet, *smq ) < sm_DR ){
      sm_index = i;
      sm_DR    = deltaR( jet, *smq );
    } else if( pu_index == -1 ){
      pu_index = i;
    }

  }

  if( dq_index >= 0 ){
    Hist( "DQ_ClosestDR" ).Fill( dq_DR );
    RunJetVar( jetHandle->at( dq_index )
             , pv, tracklist, transTrackList, "DQ_" );
  }

  if( sm_index >= 0  ){
    Hist( "SM_ClosestDR" ).Fill( sm_DR );
    RunJetVar( jetHandle->at( sm_index )
             , pv, tracklist, transTrackList, "SM_" );

  }
  if( pu_index >= 0 ){
    RunJetVar( jetHandle->at( pu_index )
             , pv, tracklist, transTrackList, "PU_" );
  }
}

bool
EMJVariablesHist::RunJetVar(
  const pat::Jet&                          jet,
  const reco::Vertex&                      pv,
  const std::vector<reco::Track>&          tracks,
  const std::vector<reco::TransientTrack>& ttracks,
  const std::string&                       prefix  )
{
  assert( tracks.size() == ttracks.size() );

  const math::XYZVector jet_p = jet.momentum();
  const GlobalVector direction( jet_p.x(), jet_p.y(), jet_p.z() );

  unsigned ntracks    = 0;
  double ip2d_mean    = 0;
  double alpha3d      = 0;
  double trackpt_sum  = 0;
  double trackpt_avg  = 0;
  double trackpt_frac = 0;
  double ip3d_sum     = 0;
  double dn_sum       = 0;
  double dn_wsum      = 0;

  for( unsigned i = 0; i < tracks.size(); ++i ){
    const auto& track  = tracks.at( i );
    const auto& ttrack = ttracks.at( i );

    // Additional track selection
    if( !TrackMatchJet( jet, ttrack ) ){ continue; }
    // If track takes up too much of the jet.
    if( track.pt() > jet.pt() * 0.6 ){ return false; }

    ++ntracks;

    // Getting impact parameter information
    auto ip3d_p = IPTools::absoluteImpactParameter3D( ttrack, pv );
    auto ip2d_p = IPTools::absoluteTransverseImpactParameter( ttrack, pv );

    const double ip3d     = ip3d_p.second.value();
    const double ip2d     = ip2d_p.second.value();
    const double ip2d_sig = ip2d_p.second.significance();

    // const double ip2d_sig = ip2d_p.second.significance();
    const double ipz = std::sqrt( ip3d*ip3d - ip2d*ip2d );
    const double DN  = std::sqrt( ipz*ipz / 0.0001 + ip2d_sig*ip2d_sig );

    ip3d_sum  += track.pt() * DN;
    ip2d_mean += ip2d;
    if( DN < 4.0 ){
      alpha3d += track.pt();
    }
    trackpt_sum += track.pt();
    dn_sum      += DN;
    dn_wsum     += track.pt() * DN;

    Hist( prefix + "DN" ).Fill( DN );
  }

  if( ntracks == 0 ){ return false; }

  ip2d_mean   /= ntracks;
  alpha3d     /= trackpt_sum;
  trackpt_avg  = trackpt_sum / ntracks;
  trackpt_frac = trackpt_sum / jet.pt();
  ip3d_sum    /= trackpt_sum;
  dn_wsum     /= dn_sum;
  dn_sum      /= ntracks;

  Hist( prefix + "LogIP2D" ).Fill( std::log( ip2d_mean )/std::log( 10 ) );
  Hist( prefix + "NumTk" ).Fill( ntracks );
  Hist( prefix + "Alpha3D" ).Fill( alpha3d );
  Hist( prefix + "AvgTkPt" ).Fill( trackpt_avg );
  Hist( prefix + "TkPtFrac" ).Fill( trackpt_frac );
  Hist( prefix + "JetIP3D" ).Fill( ip3d_sum );
  Hist( prefix + "AvgDN" ).Fill( dn_sum );
  Hist( prefix + "PTAvgDN" ).Fill( dn_sum );

  return true;
}

std::vector<reco::Track>
MakeTrackedCandidates(
  const reco::Vertex&                      pv,
  const std::vector<pat::PackedCandidate>& source1,
  const std::vector<pat::PackedCandidate>& source2,
  const std::vector<pat::PackedCandidate>& source3,
  const std::vector<reco::Track>&          source4,
  const TransientTrackBuilder&             ttBuilder )
{
  std::vector<reco::Track> ans;

  for( const auto& cand : source1 ){
    if( !cand.hasTrackDetails() ){ continue; }
    const auto track = cand.pseudoTrack();
    if( !IsGoodTrack( track, pv, ttBuilder ) ){ continue; }
    ans.push_back( track );
  }


  for( const auto& cand : source2 ){
    if( !cand.hasTrackDetails() ){ continue; }
    const auto track = cand.pseudoTrack();
    if( !IsGoodTrack( track, pv, ttBuilder  ) ){ continue; }
    ans.push_back( track );
  }

  for( const auto& cand : source3 ){
    if( !cand.hasTrackDetails() ){ continue; }
    const auto track = cand.pseudoTrack();
    if( !IsGoodTrack( track, pv, ttBuilder ) ){ continue; }
    ans.push_back( track );
  }

  for( const auto& track : source4 ){
    if( !IsGoodTrack( track, pv, ttBuilder  ) ){continue; }
    ans.push_back( track );
  }

  return ans;
}

void
EMJVariablesHist::fillDescriptions( edm::ConfigurationDescriptions&
                                    descriptions )
{
  edm::ParameterSetDescription desc;
  desc.add<edm::InputTag>(
    "AK4Jet", edm::InputTag( "slimmedJets" ) );
  desc.add<edm::InputTag>(
    "PFCandidate", edm::InputTag( "packedPFCandidates" ) );
  desc.add<edm::InputTag>(
    "LostTracks", edm::InputTag( "lostTracks" ) );
  desc.add<edm::InputTag>(
    "LostEleTracks", edm::InputTag( "lostTracks", "eleTracks" ) );
  desc.add<edm::InputTag>(
    "DisplacedTracks", edm::InputTag( "displacedStandAloneMuons" ) );
  desc.add<edm::InputTag>(
    "PrimaryVertex", edm::InputTag( "offlineSlimmedPrimaryVertices" ) );
  desc.add<edm::InputTag>(
    "GenPart", edm::InputTag( "prunedGenParticles" ) );

  descriptions.add( "EMJVariablesHist", desc );
}


DEFINE_FWK_MODULE( EMJVariablesHist );
