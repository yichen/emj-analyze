#include "UserUtils/Common/interface/STLUtils/StringUtils.hpp"
#include "UserUtils/Common/interface/STLUtils/VectorUtils.hpp"
#include "UserUtils/EDMUtils/interface/EDHistogramAnalyzer.hpp"
#include "UserUtils/PhysUtils/interface/MCHelper.hpp"
#include "UserUtils/PhysUtils/interface/TrackVertex.hpp"

#include "DataFormats/HepMCCandidate/interface/GenParticle.h"
#include "DataFormats/Math/interface/deltaR.h"
#include "DataFormats/PatCandidates/interface/Jet.h"
#include "DataFormats/PatCandidates/interface/PackedCandidate.h"

// For tracking information
#include "FWCore/Framework/interface/ESHandle.h"
#include "TrackingTools/IPTools/interface/IPTools.h"
#include "TrackingTools/Records/interface/TransientTrackRecord.h"
#include "TrackingTools/TransientTrack/interface/TransientTrackBuilder.h"

#include "DataFormats/GeometrySurface/interface/Line.h"


#include "EMJ/Analyze/interface/EMJObjectSelect.hpp"
#include "EMJ/Analyze/interface/EMJVertexFormat.hpp"

#include "TLorentzVector.h"
#include "TTree.h"

#include <fstream>

struct TrackInfo;
struct VertexInfo;
struct GenInfo;

class EMJVertex : public usr::EDHistogramAnalyzer
{
public:
  explicit EMJVertex( const edm::ParameterSet& );
  ~EMJVertex(){}

  static void fillDescriptions( edm::ConfigurationDescriptions& descriptions );

private:
  void beginJob() override;
  void
       doBeginRun_( const edm::Run&, const edm::EventSetup& ) override {}
  void analyze( const edm::Event&, const edm::EventSetup& ) override;
  void
  doEndRun_( const edm::Run&, const edm::EventSetup& ) override {}
  void
  endJob() override {}

  // ----------member data ---------------------------
  // tokens
  edm::EDGetToken tok_ak4jet;
  edm::EDGetToken tok_pfcand;
  edm::EDGetToken tok_lost;
  edm::EDGetToken tok_lostel;
  edm::EDGetToken tok_disp;
  edm::EDGetToken tok_pv;
  edm::EDGetToken tok_gen;

  std::string txt_file;

  int mode;
  enum MODE
  {
    EMJ_SIGNAL = 0,
    TTBAR      = 1,
    QCD        = 2
  };


  std::string FindPrefix( unsigned                              index,
                          const reco::Jet&                      jet,
                          const std::vector<reco::GenParticle>& genlist );
  const reco::GenParticle* FindGenParton(
    const pat::Jet&                       jet,
    const std::vector<reco::GenParticle>& genlist );

  void PrintExtraordinary(
    const pat::Jet&                                         jet,
    const TrackInfo&                                        trackinfo,
    const VertexInfo&                                       vertexinfo,
    const GenInfo&                                          geninfo,
    const edm::Event&                                       event,
    const unsigned                                          jetindex,
    const std::string&                                      prefix,
    const std::vector<std::vector<const reco::Candidate*> > genvertex,
    const std::vector<reco::GenParticle>&                   genlist
    );
};

//
// constructors and destructor
//
EMJVertex::EMJVertex( const edm::ParameterSet& iConfig ) :
  usr::EDHistogramAnalyzer( iConfig ),
  tok_ak4jet( GetToken<std::vector<pat::Jet> >( "AK4Jet" ) ),
  tok_pfcand(  GetToken<std::vector<pat::PackedCandidate> >( "PFCandidate" ) ),
  tok_lost( GetToken<std::vector<pat::PackedCandidate> >( "LostTracks" ) ),
  tok_lostel( GetToken<std::vector<pat::PackedCandidate> >( "LostEleTracks" ) ),
  tok_disp( GetToken<std::vector<reco::Track> >( "DisplacedTracks" ) ),
  tok_pv( GetToken<std::vector<reco::Vertex> >( "PrimaryVertex" ) ),
  tok_gen( GetToken<std::vector<reco::GenParticle> >( "GenPart" ) ),
  txt_file( iConfig.getParameter<std::string>( "txtfile" ) ),
  mode( iConfig.getParameter<int>( "mode" ) )
{
  /***** MADITORY!! DO NOT REMOVE  *********************************************/
}

void
EMJVertex::beginJob()
{
  static const std::vector<std::string> emj   = {"DQ_", "SM_", "PU_"};
  static const std::vector<std::string> ttbar = {"BJET_", "LIGHT_"};
  static const std::vector<std::string> qcd   = {"J1_", "J2_", "J3_", "J4_"};

  const std::vector<std::string> prefix = mode == EMJ_SIGNAL ? emj :
                                          mode == TTBAR ? ttbar :
                                          qcd;

  for( const auto p : prefix ){
    BookHist2D( p+"TrackMult_v_ChiAvg",      50,  0, 150, 100, 0, 100 );
    BookHist2D( p+"TrackMult_v_IP2D",        100, 0, 2,   50,  0, 50  );
    BookHist2D( p+"TrackMult_v_IP3D",        100, 0, 2,   50,  0, 50  );
    BookHist2D( p+"IP2D_v_IP2Dsig",          100, 0, 100, 100, 0, 2   );
    BookHist2D( p+"IP2D_v_DeltaR",           100, 0, 0.5, 100, 0, 2   );
    BookHist2D( p+"IP3D_v_IP3Dsig",          100, 0, 100, 100, 0, 2   );
    BookHist2D( p+"TrackMult_v_NVtx",        20,  0, 20,  100, 0, 100 );
    BookHist2D( p+"TrackMult_v_NVtxAlt",     20,  0, 20,  100, 0, 100 );
    BookHist2D( p+"TrackMult_v_VtxMMult",    20,  0, 20,  100, 0, 100 );
    BookHist2D( p+"TrackMult_v_VtxAltMMult", 20,  0, 20,  100, 0, 100 );
    BookHist2D( p+"IP2D_v_VtxPtRho",         200, 0, 10,  100, 0, 2   );
    BookHist2D( p+"IP2D_v_VtxAltPtRho",      200, 0, 10,  100, 0, 2   );
    BookHist2D( p+"IP2D_v_VtxMRho",          200, 0, 10,  100, 0, 2   );
    BookHist2D( p+"IP2D_v_VtxAltMRho",       200, 0, 10,  100, 0, 2   );
    BookHist2D( p+"IP2D_v_VtxMaxRho",        200, 0, 10,  100, 0, 2   );
    BookHist2D( p+"IP2D_v_VtxAltMaxRho",     200, 0, 10,  100, 0, 2   );
    BookHist2D( p+"VtxMRho_v_VtxMMult",      20,  0, 20,  100, 0, 10  );
    BookHist2D( p+"VtxMRho_v_VtxAltMMult",   20,  0, 20,  100, 0, 10  );
    BookHist2D( p+"NVtx_v_NGenVtx",          20,  0, 20,  15,  0, 15  );
    BookHist2D( p+"NVtxAlt_v_NGenVtx",       20,  0, 20,  15,  0, 15  );
    BookHist2D( p+"VtxMRho_v_GenVtxMRho",    100, 0, 20,  100, 0, 20  );
    BookHist2D( p+"VtxAltMRho_v_GenVtxMRho", 100, 0, 20,  100, 0, 20  );
  }

}

// Containers for information summary
struct TrackInfo
{
  double sum_pt;
  double avg_ip3d;
  double avg_ipz;
  double avg_ip2d;
  double avg_ip3d_sig;
  double avg_ip2d_sig;
  double avg_chi;
  double avg_deltaR;

  unsigned ntracks;
};

struct VertexInfo
{
  unsigned nvertex;

  double sum_mass;
  double avgm_rho;
  double avgm_z;
  double avgm_pt;
  double avgm_mult;

  double sum_pt;
  double avgpt_rho;
  double avgpt_z;
  double avgpt_mass;
  double avgpt_mult;

  double max_rho;
};

struct GenInfo
{
  unsigned ngen;
  double   sum_mass;
  double   avgm_rho;
  double   avgm_pt;
  double   avgm_mult;
  double   sum_pt;
  double   avgpt_rho;
  double   avgpt_mass;
  double   avgpt_mult;

  double max_rho;
};

static TrackInfo JetTrackInfo(
  const pat::Jet&                          jet,
  const reco::Vertex&                      primary_vertex,
  const std::vector<reco::TransientTrack>& tracklist );
static VertexInfo JetVertexInfo(
  const pat::Jet&                     jet,
  const std::vector<TransientVertex>& vertexlist );
static GenInfo JetGenInfo(
  const pat::Jet&                                          jet,
  const std::vector<std::vector<const reco::Candidate*> >& gen_vertex );

void
EMJVertex::analyze( const edm::Event&      event,
                    const edm::EventSetup& setup )
{
  edm::Handle<std::vector<pat::PackedCandidate> > candHandle;
  edm::Handle<std::vector<pat::PackedCandidate> > trackHandle;
  edm::Handle<std::vector<pat::PackedCandidate> > eleTrackHandle;
  edm::Handle<std::vector<reco::Track> > dtrackHandle;
  edm::Handle<std::vector<reco::GenParticle> > genHandle;
  edm::Handle<std::vector<pat::Jet> > jetHandle;
  edm::Handle<std::vector<reco::Vertex> > pvHandle;

  event.getByToken( tok_pfcand, candHandle );
  event.getByToken( tok_lost,   trackHandle );
  event.getByToken( tok_lostel, eleTrackHandle );
  event.getByToken( tok_gen,    genHandle );
  event.getByToken( tok_ak4jet, jetHandle );
  event.getByToken( tok_pv,     pvHandle );
  event.getByToken( tok_disp,   dtrackHandle );

  edm::ESHandle<TransientTrackBuilder> ttBuilder;
  setup.get<TransientTrackRecord>().get( "TransientTrackBuilder", ttBuilder );

  const int pv_idx = GetPrimaryVertex( *pvHandle );
  if( pv_idx < 0 ){ return; }
  const auto pv = pvHandle->at( pv_idx );

  // Making the master tracklist.
  std::vector<reco::Track> tracklist;
  ExtendTrackList( tracklist, pv, *candHandle,     *ttBuilder );
  ExtendTrackList( tracklist, pv, *trackHandle,    *ttBuilder );
  ExtendTrackList( tracklist, pv, *eleTrackHandle, *ttBuilder );
  ExtendTrackList( tracklist, pv, *dtrackHandle,   *ttBuilder );
  const std::vector<reco::TransientTrack> transTrackList
    = MakeTTList( tracklist, *ttBuilder );


  for( unsigned i = 0; i < jetHandle->size() && i < 6; ++i ){
    const auto& jet = jetHandle->at( i );

    if( !IsGoodJet( jet, nullptr ) ){
      continue;
    }

    const auto p = FindPrefix( i, jet, *genHandle );

    if( p == "" ){ continue; }

    const auto jet_tracks     = MatchJetTracks( jet, transTrackList );
    const auto jet_vertices   = MakeJetVertices( jet, pv, jet_tracks );
    const auto jet_vertex_alt = SelectVertex( jet, pv
                                            , ManualVertexLoop( jet_tracks ) );
    const auto gen_vertices
      = FindVertexChildren( FindGenParton( jet, *genHandle ) );

    // Getting the per-jet information summary
    const auto trkinfo  = JetTrackInfo(  jet, pv, jet_tracks );
    const auto vtxinfo  = JetVertexInfo( jet, jet_vertices   );
    const auto avtxinfo = JetVertexInfo( jet, jet_vertex_alt );
    const auto geninfo  = JetGenInfo(    jet, gen_vertices   );

    Hist2D( p + "TrackMult_v_ChiAvg"      ).Fill( trkinfo.avg_chi,  trkinfo.ntracks );
    Hist2D( p + "TrackMult_v_IP2D"        ).Fill( trkinfo.avg_ip2d, trkinfo.ntracks );
    Hist2D( p + "TrackMult_v_IP3D"        ).Fill( trkinfo.avg_ip3d, trkinfo.ntracks );
    Hist2D( p + "IP2D_v_IP2Dsig"          ).Fill( trkinfo.avg_ip2d_sig, trkinfo.avg_ip2d );
    Hist2D( p + "IP3D_v_IP3Dsig"          ).Fill( trkinfo.avg_ip3d_sig, trkinfo.avg_ip3d );
    Hist2D( p + "IP2D_v_DeltaR"           ).Fill( trkinfo.avg_deltaR, trkinfo.avg_ip2d );
    Hist2D( p + "TrackMult_v_NVtx"        ).Fill( vtxinfo.nvertex, trkinfo.ntracks    );
    Hist2D( p + "TrackMult_v_NVtxAlt"     ).Fill( avtxinfo.nvertex, trkinfo.ntracks    );
    Hist2D( p + "TrackMult_v_VtxMMult"    ).Fill( vtxinfo.avgm_mult, trkinfo.ntracks    );
    Hist2D( p + "TrackMult_v_VtxAltMMult" ).Fill( avtxinfo.avgm_mult, trkinfo.ntracks    );
    Hist2D( p + "IP2D_v_VtxPtRho"         ).Fill( vtxinfo.avgpt_rho, trkinfo.avg_ip2d );
    Hist2D( p + "IP2D_v_VtxAltPtRho"      ).Fill( avtxinfo.avgpt_rho, trkinfo.avg_ip2d );
    Hist2D( p + "IP2D_v_VtxMRho"          ).Fill( vtxinfo.avgm_rho, trkinfo.avg_ip2d );
    Hist2D( p + "IP2D_v_VtxAltMRho"       ).Fill( avtxinfo.avgm_rho, trkinfo.avg_ip2d );
    Hist2D( p + "IP2D_v_VtxMaxRho"        ).Fill( vtxinfo.max_rho, trkinfo.avg_ip2d );
    Hist2D( p + "IP2D_v_VtxAltMaxRho"     ).Fill( avtxinfo.max_rho, trkinfo.avg_ip2d );
    Hist2D( p + "VtxMRho_v_VtxMMult"      ).Fill( vtxinfo.avgm_mult, vtxinfo.avgm_rho );
    Hist2D( p + "VtxMRho_v_VtxAltMMult"   ).Fill( avtxinfo.avgm_mult, vtxinfo.avgm_rho );
    Hist2D( p + "NVtx_v_NGenVtx"          ).Fill( geninfo.ngen, vtxinfo.nvertex   );
    Hist2D( p + "NVtxAlt_v_NGenVtx"       ).Fill( geninfo.ngen, avtxinfo.nvertex   );
    Hist2D( p + "VtxMRho_v_GenVtxMRho"    ).Fill( geninfo.avgm_rho, vtxinfo.avgm_rho   );
    Hist2D( p + "VtxAltMRho_v_GenVtxMRho" ).Fill( geninfo.avgm_rho, avtxinfo.avgm_rho   );


    PrintExtraordinary( jet
                      , trkinfo, vtxinfo, geninfo
                      , event, i, p
                      , gen_vertices, *genHandle );
  }


}


std::string
EMJVertex::FindPrefix( const unsigned                        index,
                       const reco::Jet&                      jet,
                       const std::vector<reco::GenParticle>& genlist )
{
  const pat::Jet& pjet = dynamic_cast<const pat::Jet&>( jet );
  if( mode == EMJ_SIGNAL ){
    if( index > 5 ){ return ""; }

    const auto emj_parton                 = FindEMJPartons( genlist );
    double closest_dr                     = 0.8;
    const reco::Candidate* closest_parton = nullptr;

    for( const auto parton : emj_parton ){
      const double dr = deltaR( jet, *parton );
      if( dr < closest_dr  && dr < 0.3 ){
        closest_dr     = dr;
        closest_parton = parton;
      }
    }

    if( closest_parton == nullptr ){
      return "PU_";
    }

    if( closest_dr > 0.3 ){ return "PU_";}
    if( abs( closest_parton->pdgId() ) > 4900000 ){ return "DQ_"; }
    if( abs( closest_parton->pdgId() ) < 6 ){ return "SM_"; }
    return "PU_";


  } else if( mode == TTBAR ){
    const auto btag
      = pjet.bDiscriminator( "pfCombinedInclusiveSecondaryVertexV2BJetTags" );
    if( index > 5 ){
      return "";
    } else if( btag > 0.8484 ||
               ( pjet.genParton() &&  abs( pjet.genParton()->pdgId() ) == 5 ) ){
      return "BJET_";
    } else {
      return "LIGHT_";
    }
  } else {
    switch( index ){
    case 0: return "J1_";
    case 1: return "J2_";
    case 2: return "J3_";
    case 3: return "J4_";
    default:  return "";
    }
  }
}

const reco::GenParticle*
EMJVertex::FindGenParton( const pat::Jet&                       jet,
                          const std::vector<reco::GenParticle>& genlist )
{
  if( mode == EMJ_SIGNAL ){
    const auto emj_parton                 = FindEMJPartons( genlist );
    double closest_dr                     = 0.8;
    const reco::Candidate* closest_parton = nullptr;

    for( const auto parton : emj_parton ){
      const double dr = deltaR( jet, *parton );
      if( dr < closest_dr && dr < 0.3 ){
        closest_dr     = dr;
        closest_parton = parton;
      }
    }

    return dynamic_cast<const reco::GenParticle*>( closest_parton );
  } else {
    return dynamic_cast<const reco::GenParticle*>( jet.genParton() );
  }
}

TrackInfo
JetTrackInfo( const pat::Jet&                          jet,
              const reco::Vertex&                      pv,
              const std::vector<reco::TransientTrack>& tracklist )
{
  TrackInfo ans;
  ans.sum_pt       = 0;
  ans.avg_ip3d     = 0;
  ans.avg_ipz      = 0;
  ans.avg_ip2d     = 0;
  ans.avg_ip3d_sig = 0;
  ans.avg_ip2d_sig = 0;
  ans.avg_chi      = 0;
  ans.avg_deltaR   = 0;

  ans.ntracks = tracklist.size();

  for( const auto ttrack : tracklist ){
    auto ip3d_p = IPTools::absoluteImpactParameter3D( ttrack, pv );
    auto ip2d_p = IPTools::absoluteTransverseImpactParameter( ttrack, pv );

    const double ip2d     = ip2d_p.second.value();
    const double ip3d     = ip3d_p.second.value();
    const double ip2d_sig = ip2d_p.second.significance();
    const double ip3d_sig = ip3d_p.second.significance();
    const double dR       = deltaR( ttrack.track(), jet );

    // const double ip2d_sig = ip2d_p.second.significance();
    const double ipz = std::sqrt( ip3d*ip3d - ip2d*ip2d );
    const double DN  = std::sqrt( ipz*ipz / 0.0001 + ip2d_sig*ip2d_sig );

    ans.sum_pt       += ttrack.track().pt();
    ans.avg_ip3d     += ip3d *ttrack.track().pt();
    ans.avg_ipz      += ipz *ttrack.track().pt();
    ans.avg_ip2d     += ip2d *ttrack.track().pt();
    ans.avg_ip3d_sig += ip3d_sig *ttrack.track().pt();
    ans.avg_ip2d_sig += ip2d_sig  *ttrack.track().pt();
    ans.avg_chi      += DN *ttrack.track().pt();
    ans.avg_deltaR   += dR * ttrack.track().pt();
  }

  ans.avg_ip3d     /= ans.sum_pt;
  ans.avg_ipz      /= ans.sum_pt;
  ans.avg_ip2d     /= ans.sum_pt;
  ans.avg_ip3d_sig /= ans.sum_pt;
  ans.avg_ip2d_sig /= ans.sum_pt;
  ans.avg_chi      /= ans.sum_pt;
  ans.avg_deltaR   /= ans.sum_pt;


  return ans;
}

VertexInfo
JetVertexInfo( const pat::Jet&                     jet,
               const std::vector<TransientVertex>& vertexlist )
{
  VertexInfo ans;
  ans.sum_mass   = 0;
  ans.avgm_rho   = 0;
  ans.avgm_z     = 0;
  ans.avgm_pt    = 0;
  ans.avgm_mult  = 0;
  ans.sum_pt     = 0;
  ans.avgpt_rho  = 0;
  ans.avgpt_z    = 0;
  ans.avgpt_mass = 0;
  ans.avgpt_mult = 0;
  ans.max_rho    = 0;


  ans.nvertex = vertexlist.size();

  for( const auto vertex : vertexlist ){
    const auto p4 = TransientVertexP4( vertex );

    const double mass = p4.M();
    const double pt   = p4.Pt();

    const double x   = vertex.position().x();
    const double y   = vertex.position().y();
    const double z   = vertex.position().z();
    const double rho = std::sqrt( x*x* +y*y );

    ans.sum_mass  += mass;
    ans.avgm_rho  += rho* mass;
    ans.avgm_z    += z * mass;
    ans.avgm_pt   += pt * mass;
    ans.avgm_mult += vertex.originalTracks().size() * mass;

    ans.sum_pt     += pt;
    ans.avgpt_rho  += rho * pt;
    ans.avgpt_z    += z * pt;
    ans.avgpt_mass += mass * pt;
    ans.avgpt_mult += vertex.originalTracks().size() * pt;

    ans.max_rho = std::max( rho, ans.max_rho );
  }

  ans.avgm_rho  /= ans.sum_mass;
  ans.avgm_z    /= ans.sum_mass;
  ans.avgm_pt   /= ans.sum_mass;
  ans.avgm_mult /= ans.sum_mass;

  ans.avgpt_rho  /= ans.sum_pt;
  ans.avgpt_z    /= ans.sum_pt;
  ans.avgpt_mass /= ans.sum_pt;
  ans.avgpt_mult /= ans.sum_pt;

  return ans;
}

GenInfo
JetGenInfo(
  const pat::Jet&                                          jet,
  const std::vector<std::vector<const reco::Candidate*> >& gen_vertex )
{
  GenInfo ans;

  ans.sum_mass   = 0;
  ans.avgm_rho   = 0;
  ans.avgm_pt    = 0;
  ans.avgm_mult  = 0;
  ans.sum_pt     = 0;
  ans.avgpt_rho  = 0;
  ans.avgpt_mass = 0;
  ans.avgpt_mult = 0;

  for( const auto group : gen_vertex ){
    TLorentzVector p4;

    for( const auto part : group ){
      TLorentzVector t;
      t.SetPtEtaPhiE( part->pt(), part->eta(), part->phi(), part->energy() );
      p4 += t;
    }

    const double mass = p4.M();
    const double pt   = p4.Pt();
    const double x    = group.at( 0 )->vertex().x();
    const double y    = group.at( 0 )->vertex().y();
    const double rho  = std::sqrt( x*x+y*y );

    ans.sum_mass   += mass;
    ans.avgm_rho   += rho * mass;
    ans.avgm_pt    += pt * mass;
    ans.avgm_mult  += group.size() * mass;
    ans.sum_pt     += pt;
    ans.avgpt_rho  += rho * pt;
    ans.avgpt_mass += mass * pt;
    ans.avgpt_mult += group.size() * pt;

    ans.max_rho = std::max( rho, ans.max_rho );
  }

  ans.avgm_rho   /= ans.sum_mass;
  ans.avgm_pt    /= ans.sum_mass;
  ans.avgm_mult  /= ans.sum_mass;
  ans.avgpt_rho  /= ans.sum_pt;
  ans.avgpt_mass /= ans.sum_pt;
  ans.avgpt_mult /= ans.sum_pt;

  ans.ngen = gen_vertex.size();

  return ans;
}


void
EMJVertex::PrintExtraordinary (
  const pat::Jet&                                         jet,
  const TrackInfo&                                        trackinfo,
  const VertexInfo&                                       vertexinfo,
  const GenInfo&                                          geninfo,
  const edm::Event&                                       event,
  const unsigned                                          jetindex,
  const std::string&                                      prefix,
  const std::vector<std::vector<const reco::Candidate*> > genvertex,
  const std::vector<reco::GenParticle>&                   genlist )
{
  if( txt_file == "" ){ return;}

  std::fstream fs;
  fs.open( txt_file, std::ios_base::app );

  auto PrintLine = [&fs, &event, &jetindex]( const std::string& x ){
                     fs << usr::fstr( "[%u:%u:%u] %u %s\n",
                       event.id().run(),
                       event.id().luminosityBlock(),
                       event.id().event(),
                       jetindex,
                       x );
                   };

  if( prefix == "SM_" ){
    if( vertexinfo.avgm_rho < 0.3 && geninfo.avgm_rho > 10 ){
      PrintLine( "Large Gen Vertex" );

      std::vector<const reco::GenParticle*> ptrlist;

      for( const auto& gen : genlist ){
        ptrlist.push_back( &gen );
      }

      const auto parton = FindGenParton( jet, genlist );

      fs << std::find( ptrlist.begin(), ptrlist.end(), parton ) - ptrlist.begin() << std::endl;

      for( const auto group : genvertex ){
        for( const auto part : group ){
          fs
            << std::find( ptrlist.begin(), ptrlist.end(), part ) - ptrlist.begin()
            << " " << std::flush;
        }

        fs << std::endl;
      }
    }
  } else if( prefix != "SM_" && prefix != "DQ_" && prefix != "PU_" ){
    if( trackinfo.avg_ip2d < 0.1 && vertexinfo.avgm_rho > 5 ){
      PrintLine( "Large displace, no IP2D" );
    }
  }


}

void
EMJVertex::fillDescriptions( edm::ConfigurationDescriptions&
                             descriptions )
{
  edm::ParameterSetDescription desc;
  desc.add<edm::InputTag>(
    "AK4Jet", edm::InputTag( "slimmedJets" ) );
  desc.add<edm::InputTag>(
    "PFCandidate", edm::InputTag( "packedPFCandidates" ) );
  desc.add<edm::InputTag>(
    "LostTracks", edm::InputTag( "lostTracks" ) );
  desc.add<edm::InputTag>(
    "LostEleTracks", edm::InputTag( "lostTracks", "eleTracks" ) );
  desc.add<edm::InputTag>(
    "DisplacedTracks", edm::InputTag( "displacedStandAloneMuons" ) );
  desc.add<edm::InputTag>(
    "PrimaryVertex", edm::InputTag( "offlineSlimmedPrimaryVertices" ) );
  desc.add<edm::InputTag>(
    "GenPart", edm::InputTag( "prunedGenParticles" ) );
  desc.add<int>(         "mode",    2  );
  desc.add<std::string>( "txtfile", "event_list.txt"  );

  descriptions.add( "EMJVertex", desc );
}


DEFINE_FWK_MODULE( EMJVertex );
