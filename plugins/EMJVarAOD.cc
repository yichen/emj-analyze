#include "UserUtils/EDMUtils/interface/EDHistogramAnalyzer.hpp"
#include "UserUtils/PhysUtils/interface/MCHelper.hpp"

#include "DataFormats/HepMCCandidate/interface/GenParticle.h"
#include "DataFormats/JetReco/interface/PFJet.h"
#include "DataFormats/Math/interface/deltaR.h"
#include "DataFormats/ParticleFlowCandidate/interface/PFCandidate.h"
#include "DataFormats/TrackReco/interface/Track.h"
#include "DataFormats/VertexReco/interface/Vertex.h"

// For tracking information
#include "FWCore/Framework/interface/ESHandle.h"
#include "TrackingTools/IPTools/interface/IPTools.h"
#include "TrackingTools/Records/interface/TransientTrackRecord.h"
#include "TrackingTools/TransientTrack/interface/TransientTrackBuilder.h"


#include "EMJ/Analyze/interface/EMJObjectSelect.hpp"


class EMJVarAOD : public usr::EDHistogramAnalyzer
{
public:
  explicit EMJVarAOD( const edm::ParameterSet& );
  ~EMJVarAOD(){}

  static void fillDescriptions( edm::ConfigurationDescriptions& descriptions );

private:
  void beginJob() override;
  void
       doBeginRun_( const edm::Run&, const edm::EventSetup& ) override {}
  void analyze( const edm::Event&, const edm::EventSetup& ) override;
  void
  doEndRun_( const edm::Run&, const edm::EventSetup& ) override {}
  void
  endJob() override {}

  // ----------member data ---------------------------
  // tokens
  edm::EDGetToken tok_ak4jet;
  edm::EDGetToken tok_pv;
  edm::EDGetToken tok_gen;

  std::vector<edm::EDGetToken> tokens_track;

  bool RunJetVar( const reco::PFJet&                       jet,
                  const reco::Vertex&                      pv,
                  const std::vector<reco::Track>&          tracks,
                  const std::vector<reco::TransientTrack>& ttracks,
                  const std::string&                       prefix  );

};

//
// constructors and destructor
//
EMJVarAOD::EMJVarAOD( const edm::ParameterSet& iConfig ) :
  usr::EDHistogramAnalyzer( iConfig ),
  tok_ak4jet( GetToken<std::vector<reco::PFJet> >( "AK4Jet" ) ),
  tok_pv( GetToken<std::vector<reco::Vertex> >( "PrimaryVertex" ) ),
  tok_gen( GetToken<std::vector<reco::GenParticle> >( "GenPart" ) )
{
  for( const auto tag :
       iConfig.getParameter<std::vector<edm::InputTag> >( "TrackCollections" ) ){
    tokens_track.push_back( consumes<std::vector<reco::Track> >( tag ) );
  }
}

void
EMJVarAOD::beginJob()
{
  BookHist1D( "PVTrackF_Good", 100, 0, 1   );
  BookHist1D( "PVTrackF_Poor", 100, 0, 1   );
  BookHist2D( "PVFuzzy_v_Z",    100, 0, 0.5, 100, 0, 0.1 );
  BookHist2D( "PVJetFuzzy_v_Z", 100, 0, 0.5, 100, 0, 5 );

  BookHist1D( "DQ_DN",         100, 0,  100 );
  BookHist1D( "DQ_LogIP2D",    24,  -5, 1   );
  BookHist1D( "DQ_NumTk",      50,  0,  50  );
  BookHist1D( "DQ_Alpha3D",    50,  0,  1   );
  BookHist1D( "DQ_AvgTkPt",    100, 0,  20  );
  BookHist1D( "DQ_TkPtFrac",   50,  0,  1.5 );
  BookHist1D( "DQ_ClosestEta", 30,  0,  0.6 );

  BookHist1D( "SM_DN",         100, 0,  100 );
  BookHist1D( "SM_LogIP2D",    70,  -5, 2   );
  BookHist1D( "SM_NumTk",      100, 0,  100 );
  BookHist1D( "SM_Alpha3D",    50,  0,  1   );
  BookHist1D( "SM_AvgTkPt",    100, 0,  20  );
  BookHist1D( "SM_TkPtFrac",   50,  0,  1.5 );
  BookHist1D( "SM_ClosestEta", 30,  0,  0.6 );

}

// Custom analysis helper functions

static void AppendTracks( std::vector<reco::Track>&       selected_tracks,
                          const std::vector<reco::Track>& tracks );

void
EMJVarAOD::analyze( const edm::Event&      event,
                    const edm::EventSetup& setup )
{
  edm::Handle<std::vector<reco::Track> > trackHandle;
  edm::Handle<std::vector<reco::PFJet> > jetHandle;
  edm::Handle<std::vector<reco::Vertex> > pvHandle;
  edm::Handle<std::vector<reco::GenParticle> > genHandle;

  event.getByToken( tok_gen,    genHandle );
  event.getByToken( tok_ak4jet, jetHandle );
  event.getByToken( tok_pv,     pvHandle );

  edm::ESHandle<TransientTrackBuilder> ttBuilder;
  setup.get<TransientTrackRecord>().get( "TransientTrackBuilder", ttBuilder );

  // Early exit for bad primary vertex
  const int pv_idx = GetPrimaryVertex( *pvHandle );
  if( pv_idx < 0 ){ return; }
  const auto pv    = pvHandle->at( pv_idx );
  const auto darkq = FindDarkQuark( *genHandle );
  const auto smq   = FindSMQuark( *genHandle );

  // Creating master list
  std::vector<reco::Track> tracklist;

  for( const auto& token : tokens_track ){
    event.getByToken( token, trackHandle );
    AppendTracks( tracklist, *trackHandle );
  }

  const auto gen_vtx = usr::mc::GetGenVertex( *genHandle );

  // Getting the relevant tracks and track collection.
  std::vector<reco::PFJet> selected_jets;
  std::vector<std::vector<reco::Track> > jet_tracks_list;
  GetSelectedJetTracks( *jetHandle, tracklist, selected_jets, jet_tracks_list );

  const double pv_track_fraction = PVTrackFraction( pv, jet_tracks_list );
  if( fabs( gen_vtx.z() - pv.z() ) < 0.01  ){
    Hist( "PVTrackF_Good" ).Fill( pv_track_fraction );
  } else {
    Hist( "PVTrackF_Poor" ).Fill( pv_track_fraction );
  }

  // Skipping events that would be filtered out in final event selection.
  if( pv_track_fraction < 0.1 ){ return; }

  Hist2D( "PVFuzzy_v_Z" ).Fill( PVClosestZ( pv, *pvHandle ), PVFuzzyness( pv, *ttBuilder ) );
  Hist2D( "PVJetFuzzy_v_Z" ).Fill( PVClosestZ( pv, *pvHandle ), PVJetFuzzyness( pv, jet_tracks_list, *ttBuilder ) );




}

void
AppendTracks(
  std::vector<reco::Track>&       selectedTracks,
  const std::vector<reco::Track>& source )
{
  for( const auto& track : source ){
    if( track.pt() < 1.0 ){ continue; }
    if( !track.quality( reco::TrackBase::highPurity ) ){ continue; }
    selectedTracks.push_back( track );
  }
}



void
EMJVarAOD::fillDescriptions( edm::ConfigurationDescriptions&
                             descriptions )
{
  edm::ParameterSetDescription desc;
  desc.add<edm::InputTag>(
    "AK4Jet", edm::InputTag( "ak4PFJetsCHS" ) );
  desc.add<std::vector<edm::InputTag> >(
    "TrackCollections", {
    edm::InputTag( "generalTracks" )
    , edm::InputTag( "displacedTracks" )
  } );
  // desc.add<edm::InputTag>( "ParticleFlow", edm::InputTag( "particleFlow" ) );
  desc.add<edm::InputTag>(
    "PrimaryVertex", edm::InputTag( "offlinePrimaryVertices" ) );
  desc.add<edm::InputTag>(
    "GenPart", edm::InputTag( "genParticles" ) );

  descriptions.add( "EMJVarAOD", desc );
}


DEFINE_FWK_MODULE( EMJVarAOD );
