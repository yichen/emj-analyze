#include "UserUtils/Common/interface/STLUtils/VectorUtils.hpp"
#include "UserUtils/EDMUtils/interface/EDHistogramAnalyzer.hpp"
#include "UserUtils/PhysUtils/interface/MCHelper.hpp"

#include "DataFormats/HepMCCandidate/interface/GenParticle.h"
#include "DataFormats/JetReco/interface/GenJet.h"
#include "DataFormats/Math/interface/deltaR.h"

// #include "EMJ/Analyze/interface/EMJObjectSelect.hpp"
#include "EMJ/Analyze/interface/EMJGenHelper.hpp"

#include "Math/VectorUtil.h"


class GenHistogram : public usr::EDHistogramAnalyzer
{
public:
  explicit GenHistogram( const edm::ParameterSet& );
  ~GenHistogram(){}

  static void fillDescriptions( edm::ConfigurationDescriptions& descriptions );

private:
  void beginJob() override;
  void
       doBeginRun_( const edm::Run&, const edm::EventSetup& ) override {}
  void analyze( const edm::Event&, const edm::EventSetup& ) override;
  void
  doEndRun_( const edm::Run&, const edm::EventSetup& ) override {}
  void
  endJob() override {}

  // ----------member data ---------------------------
  // tokens
  edm::EDGetToken tok_gen;


  // Common Helper functions
  void BookCommonHist();

  // Dark quark decendants stuff
  void BookDarkQuarkHist();
  void BookDarkMesonHist();
  void BookSMQuarkHist();
  void BookSMMesonHist();
  void BookFinalHist();
  // void BookFinalCandHist();

  void BookSMShowerHist();
  void BookSMFinalHist();
  // void BookSMFinalCandHist();
  void BookHeavyJetFinalHist();
  // void BookHeavyJetFinalCandHist();

  void BookParticleCommonHist( const std::string& prefix );
  void BookFinalCommonHist( const std::string& prefix );

  // Filling functions all take a single dark quark as argument
  void FillDarkQuarkHist( const reco::Candidate* );
  void FillDarkMesonHist( const reco::Candidate*, const reco::Candidate* );
  void FillSMQuarkHist( const reco::Candidate*, const reco::Candidate* );
  void FillSMMesonHist( const reco::Candidate*, const reco::Candidate* );
  void FillFinalHist( const reco::Candidate*, const reco::Candidate* );
  // void FillFinalCandHist( const reco::Candate*, const reco::Candidate* );
  void FillSMShowerHist( const reco::Candidate* );
  void FillSMFinalHist( const reco::Candidate* );
  // void FillSMFinalCandHist( const reco::Candidate* );
  void FillHeavyJetFinalHist( const reco::Candidate* );

  void FillParticleCommonHist( const std::string&     prefix,
                               const reco::Candidate* dark_q,
                               const reco::Candidate* x  );
  void FillFinalCommonHist( const std::string&     prefix,
                            const reco::Candidate* dark_q,
                            const reco::Candidate* x );
  void FillQuarkSummaryHist( const std::string&                         prefix,
                             const reco::Candidate*                     dark_q,
                             const std::vector<const reco::Candidate*>& x  );
};

GenHistogram::GenHistogram( const edm::ParameterSet& iConfig ) :
  usr::EDHistogramAnalyzer( iConfig ),
  tok_gen( GetToken<std::vector<reco::GenParticle> >( "GenParticle" ) )
{
}

void
GenHistogram::beginJob()
{
  std::cout << "Starting Gen histogram stuff" << std::endl;

  // Dark quark property measurements
  BookCommonHist();
  BookDarkQuarkHist();
  BookDarkMesonHist();
  BookSMQuarkHist();
  BookSMMesonHist();
  BookFinalHist();

  // Accompanying SM shower stuff
  BookSMShowerHist();
  BookSMFinalHist();
  BookHeavyJetFinalHist();

  // Quark quark meson properties measurement.
}

void
GenHistogram::analyze( const edm::Event&      event,
                       const edm::EventSetup& setup )
{
  edm::Handle<std::vector<reco::GenParticle> > genHandle;
  event.getByToken( tok_gen, genHandle );

  // Getting the primodial dark quark:
  // - Must have a unique parent that is a HV mediator
  // - Must have an index that is a HV quark.
  const auto dark_q_list = usr::mc::FindAll( *genHandle,
    usr::mc::pp( []( const reco::Candidate* x )->bool {
    if( x->numberOfMothers() != 1 ){ return false; }
    if( !IsHVMediator( x->mother( 0 ) ) &&
        !usr::mc::IsSMHiggs( x->mother( 0 ) ) ){
      return false;
    }
    return IsHVQuark( x );
  } ) );

  // Getting the primodial SM quark,
  // - Most have a unique parent that is the HV mediator.
  // - Must have PDG id compatible with SM quark
  const auto sm_q_list = usr::mc::FindAll( *genHandle,
    usr::mc::pp( []( const reco::Candidate* x )->bool {
    if( x->numberOfMothers() != 1 ){ return false; }
    if( !IsHVMediator( x->mother( 0 ) ) ){return false; }
    return usr::mc::IsSMQuark( x );
  } ) );

  assert( dark_q_list.size() == 2 );

  FillDarkQuarkHist( dark_q_list[0] );
  FillDarkQuarkHist( dark_q_list[1] );
  FillDarkMesonHist( dark_q_list[0], dark_q_list[1] );
  FillSMQuarkHist( dark_q_list[0], dark_q_list[1] );
  FillSMMesonHist( dark_q_list[0], dark_q_list[1] );
  FillFinalHist( dark_q_list[0], dark_q_list[1] );

  if( sm_q_list.size() == 2 ){
    for( const auto sm_q : sm_q_list ){
      FillSMShowerHist( sm_q );
      FillSMFinalHist( sm_q );
      if( abs( sm_q->pdgId() ) >= 5 ){
        FillHeavyJetFinalHist( sm_q );
      }
    }
  }

}

void
GenHistogram::BookCommonHist()
{
  // Only common histogram is a summary histogram of particle types;
  BookHist1D( "Summary", 50, 0, 50 );
}

void
GenHistogram::BookDarkQuarkHist()
{
  BookHist1D( "DarkQuarkPt",  200, 0,  1600 );
  BookHist1D( "DarkQuarkEta", 80,  -4, 4 );
}

void
GenHistogram::FillDarkQuarkHist( const reco::Candidate* dark_q )
{
  Hist( "DarkQuarkPt" ).Fill( dark_q->pt() );
  Hist( "DarkQuarkEta" ).Fill( dark_q->eta() );

  // Index to fill in summary
  Hist( "Summary" ).Fill( abs( dark_q->pdgId() ) - 4900101  );
}

// All daughter particle type will follow similar histogram structures:
void
GenHistogram::BookParticleCommonHist( const std::string& prefix )
{
  // Per particle filling.
  BookHist1D( prefix + "Pt",            200, 0,  1000 );
  BookHist1D( prefix + "Eta",           80,  -4, 4    );
  BookHist1D( prefix + "RelPt",         200, 0,  50   );
  BookHist1D( prefix + "RelFracPt",     100, 0,  5    );
  BookHist1D( prefix + "DR",            320, 0,  3.2  );
  BookHist1D( prefix + "PtDR",          320, 0,  3.2  );
  BookHist1D( prefix + "DecayR",        50,  0,  100  );
  BookHist1D( prefix + "DecayRho",      50,  0,  100  );

  // Per quark summary status
  BookHist1D( prefix + "Mult",          200, 0,  200  );
  BookHist1D( prefix + "R04NumFrac",    50,  0,  1    );
  BookHist1D( prefix + "R04EnergyFrac", 50,  0,  1    );
  BookHist1D( prefix + "R04PtFrac",     50,  0,  1    );
  BookHist1D( prefix + "R08NumFrac",    50,  0,  1    );
  BookHist1D( prefix + "R08EnergyFrac", 50,  0,  1    );
  BookHist1D( prefix + "R08PtFrac",     50,  0,  1    );
}

void
GenHistogram::FillParticleCommonHist( const std::string&     prefix,
                                      const reco::Candidate* dark_q,
                                      const reco::Candidate* x )
{
  const auto dp4 = dark_q->p4();
  const auto p4  = x->p4();
  Hist( prefix + "Pt" ).Fill( p4.pt() );
  Hist( prefix + "Eta" ).Fill( p4.eta() );
  Hist( prefix + "DR" ).Fill( ROOT::Math::VectorUtil::DeltaR( p4, dp4 ) );
  Hist( prefix + "PtDR" ).Fill(
    ROOT::Math::VectorUtil::DeltaR( p4, dp4 ), p4.pt() / dp4.pt() );
  Hist( prefix + "RelPt" ).Fill(
    ROOT::Math::VectorUtil::Perp( p4.Vect(), dp4.Vect() ) );
  Hist( prefix + "RelFracPt" ).Fill(
    ROOT::Math::VectorUtil::Perp( p4, dp4 ) / p4.pt() );
  Hist( prefix + "DecayR" ).Fill(
    ( x->vertex() - dark_q->vertex() ).R() );
  Hist( prefix + "DecayRho" ).Fill(
    ( x->vertex() - dark_q->vertex() ).R() );
}

void
GenHistogram::FillQuarkSummaryHist(
  const std::string&                         prefix,
  const reco::Candidate*                     dark_q,
  const std::vector<const reco::Candidate*>& desc )
{
  Hist( prefix + "Mult" ).Fill( desc.size() );

  double n04          = 0;
  double n08          = 0;
  double pt_total     = 0;
  double pt04         = 0;
  double pt08         = 0;
  double energy_total = 0;
  double energy04     = 0;
  double energy08     = 0;

  for( const auto p : desc ){
    pt_total     += p->pt();
    energy_total += p->energy();
    const double dr = ROOT::Math::VectorUtil::DeltaR( p->p4(), dark_q->p4() );
    if( dr < 0.4 ){
      n04      += 1.0;
      pt04     += p->pt();
      energy04 += p->energy();
    }
    if( dr < 0.8 ){
      n08      += 1.0;
      pt08     += p->pt();
      energy08 += p->energy();
    }
  }

  assert( pt04 <= pt_total );
  assert( energy04 <= energy_total );
  assert( pt08 <= pt_total );
  assert( energy08 <= energy_total );

  Hist( prefix + "R04NumFrac" ).Fill( n04/ desc.size() );
  Hist( prefix + "R04PtFrac" ).Fill( pt04/ pt_total );
  Hist( prefix + "R04EnergyFrac" ).Fill( energy04/ energy_total );
  Hist( prefix + "R08NumFrac" ).Fill( n08/ desc.size() );
  Hist( prefix + "R08PtFrac" ).Fill( pt08/ pt_total );
  Hist( prefix + "R08EnergyFrac" ).Fill( energy08/ energy_total );
}

// Since all dark mesons are decedants of all dark quarks due to dark color
// connections used in the showering calculations, we need a custom algorithm to
// get assign dark meson to the primordial dark quarks. Here we are assigned
// which ever quarks is closer in the delta R space.
static std::vector<std::vector<const reco::Candidate*> >
FindDarkMesons( const reco::Candidate* dark_q1, const reco::Candidate* dark_q2 )
{
  static auto is_dark_meson = usr::mc::pp( []( const reco::Candidate* x )->bool {
    if( !IsHVMeson( x ) ){ return false; }
    if( HasHVDaughter( x ) ){ return false; }
    return true;
  } );

  std::vector<std::vector<const reco::Candidate*> > ans = {{}, {}};

  auto mesons_1 = usr::mc::FindDecendants( dark_q1, is_dark_meson );
  auto mesons_2 = usr::mc::FindDecendants( dark_q2, is_dark_meson );

  mesons_1.insert( mesons_1.end(), mesons_2.begin(), mesons_2.end() );
  const auto dark_mesons = usr::RemoveDuplicate( mesons_1 );

  for( const auto m : dark_mesons ){
    const double dr1 = deltaR( *dark_q1, *m );
    const double dr2 = deltaR( *dark_q2, *m );

    if( dr1 < dr2 ){
      ans[0].push_back( m );
    } else {
      ans[1].push_back( m );
    }
  }

  return ans;
}


// Dark meson specifics;
void
GenHistogram::BookDarkMesonHist()
{
  BookParticleCommonHist( "DarkMeson" );
}

void
GenHistogram::FillDarkMesonHist( const reco::Candidate* dark_q1,
                                 const reco::Candidate* dark_q2 )
{
  static auto summary_index = []( const reco::Candidate* x )->int {
                                return abs( x->pdgId() ) == 4900111 ? 3 :
                                       abs( x->pdgId() ) == 4900113 ? 4 :
                                       abs( x->pdgId() ) == 4900211 ? 5 :
                                       abs( x->pdgId() ) == 4900213 ? 6 :
                                       -1;
                              };
  const auto dark_meson_lists = FindDarkMesons( dark_q1, dark_q2 );

  for( const auto m : dark_meson_lists[0] ){
    FillParticleCommonHist( "DarkMeson", dark_q1, m );
    Hist( "Summary" ).Fill( summary_index( m ) );
  }

  for( const auto m : dark_meson_lists[1] ){
    FillParticleCommonHist( "DarkMeson", dark_q2, m );
    Hist( "Summary" ).Fill( summary_index( m ) );
  }

  FillQuarkSummaryHist( "DarkMeson", dark_q1, dark_meson_lists[0] );
  FillQuarkSummaryHist( "DarkMeson", dark_q2, dark_meson_lists[1] );
}


/// For decay producs othe dark mesons, It is handy to have the function return a
/// single list.
static std::vector<const reco::Candidate*>
FindDarkMesonsDescendants( const std::vector<const reco::Candidate*> dm_list,
                           usr::mc::pp&                              func )
{
  std::vector<const reco::Candidate*> ans;

  for( const auto dm : dm_list ){
    const auto temp = usr::mc::FindDecendants( dm, func );
    ans.insert( ans.end(), temp.begin(), temp.end() );
  }

  return usr::RemoveDuplicate( ans );
}

/// Standard model quark result
void
GenHistogram::BookSMQuarkHist()
{
  BookParticleCommonHist( "SMQuark" );

  BookHist1D( "SMQuarkHeavyMult", 20, 0, 20 );
}

void
GenHistogram::FillSMQuarkHist( const reco::Candidate* dark_q1,
                               const reco::Candidate* dark_q2 )
{
  static auto summary_index =
    []( const reco::Candidate* x )->int {
      return abs( x->pdgId() ) == usr::mc::DOWN_QUARK_ID    ? 7 :
             abs( x->pdgId() ) == usr::mc::UP_QUARK_ID      ? 8 :
             abs( x->pdgId() ) == usr::mc::STRANGE_QUARK_ID ? 9 :
             abs( x->pdgId() ) == usr::mc::CHARM_QUARK_ID   ? 10 :
             abs( x->pdgId() ) == usr::mc::BOTTOM_QUARK_ID  ? 11 :
             abs( x->pdgId() ) == usr::mc::TOP_QUARK_ID     ? 12 :
             abs( x->pdgId() ) == usr::mc::GLUON_ID         ? 13 :
             -1;
    };
  // SM quarks of interest should be the direct decendants of the dark mesons.
  static auto sm_quark = usr::mc::pp( []( const reco::Candidate* x )->bool {
    if( !usr::mc::IsSMQuark( x ) && !usr::mc::IsSMGluon( x ) ){ return false;}
    if( !HasHVMother( x ) ){ return false; }
    return true;
  } );

  // Starting from dark meson list
  const auto dark_meson_lists = FindDarkMesons( dark_q1, dark_q2 );

  const auto sm1 = FindDarkMesonsDescendants( dark_meson_lists[0], sm_quark );
  const auto sm2 = FindDarkMesonsDescendants( dark_meson_lists[1], sm_quark );

  FillQuarkSummaryHist( "SMQuark", dark_q1, sm1 );
  FillQuarkSummaryHist( "SMQuark", dark_q2, sm2 );

  unsigned n_heavy = 0;

  for( const auto q : sm1 ){
    FillParticleCommonHist( "SMQuark", dark_q1, q );
    Hist( "Summary" ).Fill( summary_index( q ) );
    if( abs( q->pdgId() ) >= 5 ){ n_heavy++; }
  }

  Hist( "SMQuarkHeavyMult" ).Fill( n_heavy );
  n_heavy = 0;

  for( const auto q : sm2 ){
    FillParticleCommonHist( "SMQuark", dark_q2, q );
    Hist( "Summary" ).Fill( summary_index( q ) );
    if( abs( q->pdgId() ) >= 5 ){ n_heavy++; }
  }

  Hist( "SMQuarkHeavyMult" ).Fill( n_heavy );
}

/// Standard model mesons
void
GenHistogram::BookSMMesonHist()
{
  BookParticleCommonHist( "SMMeson" );
}

void
GenHistogram::FillSMMesonHist( const reco::Candidate* dark_q1,
                               const reco::Candidate* dark_q2 )
{
  static auto summary_index
    = []( const reco::Candidate* x )->int {
        const int id = abs( x->pdgId() );
        return id % 10000 < 300   ? 0 + 14 : // Light meson
               id % 10000 < 400   ? 1 + 14 : // Strange meson
               id % 10000 < 500   ? 2 + 14 : // Charm meson
               id % 10000 < 600   ? 3 + 14 : // Bottom meson
               id % 10000 == 2212 ? 4 + 14 : // proton
               id % 10000 == 2112 ? 5 + 14 : // Neutron
               id % 10000 < 3000  ? 6 + 14 : // Light baryon
               id % 10000 < 4000  ? 7 + 14 : // Strange baryon
               id % 10000 < 5000  ? 8 + 14 : // Charm baryon
               id % 10000 < 6000  ? 9 + 14 : // Bottom baryon
               -1;// 10 + 13; // End 23
      };

  // The SM quarks of interest are the first SM meson in the decay change.
  // Their parent should be SM quarks
  static auto sm_meson = usr::mc::pp( []( const reco::Candidate* x )->bool {
    if( !usr::mc::IsSMHadron( x ) ){ return false; }
    if( !usr::mc::HasMother( x, usr::mc::IsSMQuark ) &&
        !usr::mc::HasMother( x, usr::mc::IsSMGluon ) ){ return false; }
    return true;
  } );
  // Starting from dark meson list
  const auto dark_meson_lists = FindDarkMesons( dark_q1, dark_q2 );

  const auto sm1 = FindDarkMesonsDescendants( dark_meson_lists[0], sm_meson );
  const auto sm2 = FindDarkMesonsDescendants( dark_meson_lists[1], sm_meson );

  FillQuarkSummaryHist( "SMMeson", dark_q1, sm1 );
  FillQuarkSummaryHist( "SMMeson", dark_q2, sm2 );

  for( const auto m : sm1 ){
    FillParticleCommonHist( "SMMeson", dark_q1, m );
    Hist( "Summary" ).Fill( summary_index( m ) );
  }

  for( const auto m : sm2 ){
    FillParticleCommonHist( "SMMeson", dark_q2, m );
    Hist( "Summary" ).Fill( summary_index( m ) );
  }

}

/// Final state particles
void
GenHistogram::BookFinalHist()
{
  BookParticleCommonHist( "Final" );
  BookFinalCommonHist( "Final" );
}

void
GenHistogram::BookFinalCommonHist( const std::string& prefix )
{
  BookHist1D( prefix + "VtxRho",   100, 0,  100 );
  BookHist1D( prefix + "VtxR",     100, 0,  100 );
  BookHist1D( prefix + "VtxLogR",  100, -8, 5 );
  BookHist1D( prefix + "VtxDR",    150, 0,  1.5 );
  BookHist1D( prefix + "VtxAngle", 150, 0,  1.5 );
  BookHist1D( prefix + "VtxIP",    100, 0,  10 );
  BookHist1D( prefix + "VtxLogIP", 100, -8, 5 );
}


void
GenHistogram::FillFinalHist( const reco::Candidate* dark_q1,
                             const reco::Candidate* dark_q2 )
{
  static auto summary = []( const reco::Candidate* x )->int {
                          return abs( x->pdgId() ) == 211 ?  0 + 24 : // pi+-
                                 abs( x->pdgId() ) == 130 ?  1 + 24 : // K-long
                                 abs( x->pdgId() ) == 321 ?  2 + 24 : // Charged k-on
                                 abs( x->pdgId() ) == 2212 ? 3 + 24 : // Proton
                                 abs( x->pdgId() ) == 2112 ? 4 + 24 : // Neutron
                                 abs( x->pdgId() ) == 11 ?   5 + 24 : // electron
                                 abs( x->pdgId() ) == 13 ?   6 + 24 : // muon
                                 7 + 24;// Others
                        };
  // The final state particles of should be charged and have status code 1
  static auto final = usr::mc::pp( []( const reco::Candidate* x )->bool {
    if( x->charge() == 0 ){ return false; }
    if( x->status() != 1 ){ return false; }
    return true;
  } );

  // Starting from dark meson list
  const auto dark_meson_lists = FindDarkMesons( dark_q1, dark_q2 );

  const auto f1 = FindDarkMesonsDescendants( dark_meson_lists[0], final );
  const auto f2 = FindDarkMesonsDescendants( dark_meson_lists[1], final );

  FillQuarkSummaryHist( "Final", dark_q1, f1 );
  FillQuarkSummaryHist( "Final", dark_q2, f2 );

  for( const auto p : f1 ){
    FillParticleCommonHist( "Final", dark_q1, p );
    FillFinalCommonHist( "Final", dark_q1, p );
    Hist( "Summary" ).Fill( summary( p ) );
  }

  for( const auto p : f2 ){
    FillParticleCommonHist( "Final", dark_q2, p );
    FillFinalCommonHist( "Final", dark_q2, p );
    Hist( "Summary" ).Fill( summary( p ) );
  }
}


void
GenHistogram::FillFinalCommonHist(
  const std::string&     prefix,
  const reco::Candidate* dark_q,
  const reco::Candidate* p )
{
  // Getting the vertex information
  const auto pv   = dark_q->vertex();
  const auto sv   = p->vertex();
  const auto dist = sv - pv;
  const auto axis = dark_q->p4().Vect();

  const auto angle = ROOT::Math::VectorUtil::Angle( dist, axis );

  Hist( prefix + "VtxRho" ).Fill( dist.rho() );
  Hist( prefix + "VtxLogR" ).Fill( log10( dist.r() ) );
  Hist( prefix + "VtxR" ).Fill( dist.r() );
  Hist( prefix + "VtxDR" ).Fill( ROOT::Math::VectorUtil::DeltaR( dist, axis ) );
  Hist( prefix + "VtxAngle" ).Fill( angle );
  Hist( prefix + "VtxIP" ).Fill( dist.R() * sin( angle ) );
  Hist( prefix + "VtxLogIP" ).Fill( log10( dist.R() *sin( angle ) ) );
}

void
GenHistogram::BookSMShowerHist()
{
  BookParticleCommonHist( "SMShower" );
}


void
GenHistogram::FillSMShowerHist( const reco::Candidate* sm_q )
{
  const auto meson_list = usr::mc::FindDecendants( sm_q,
    usr::mc::pp( []( const reco::Candidate* x )->bool {
    if( !usr::mc::IsSMHadron( x ) ){ return false; }
    if( !usr::mc::HasMother( x, usr::mc::IsSMQuark ) ){ return false; }
    return true;
  } ) );

  for( const auto meson : meson_list ){
    FillParticleCommonHist( "SMShower", sm_q, meson );
//    const int final_id = abs( final->pdgId() );
//    const int summary  = final_id == 211 ?  0 + 23 : // pi+-
//                         final_id == 130 ?  1 + 23 : // K-long
//                         final_id == 321 ?  2 + 23 : // Charged k-on
//                         final_id == 2212 ? 3 + 23 : // Proton
//                         final_id == 2112 ? 4 + 23 : // Neutron
//                         final_id == 11 ?   5 + 23 : // electron
//                         final_id == 13 ?   6 + 23 : // muon
//                         7 + 24;  // Others
//    Hist( "Summary" ).Fill( summary );
  }

  FillQuarkSummaryHist( "SMShower", sm_q, meson_list );
}

void
GenHistogram::BookSMFinalHist()
{
  BookParticleCommonHist( "SMFinal" );
  BookFinalCommonHist( "SMFinal" );
}

void
GenHistogram::FillSMFinalHist( const reco::Candidate* sm_q )
{
  const auto final_list = usr::mc::FindDecendants( sm_q,
    usr::mc::pp( [] ( const reco::Candidate* x )->bool {
    if( x->charge() != 0 ){ return false; }
    if( x->status() == 1 ){ return false; }
    return true;
  } ) );

  for( const auto final : final_list ){
    FillParticleCommonHist( "SMFinal", sm_q, final );
    FillFinalCommonHist( "SMFinal", sm_q, final );
  }
}

void
GenHistogram::BookHeavyJetFinalHist()
{
  BookParticleCommonHist( "HeavyJetFinal" );
  BookFinalCommonHist( "HeavyJetFinal" );
}

void
GenHistogram::FillHeavyJetFinalHist( const reco::Candidate* sm_q )
{
  const auto final_list = usr::mc::FindDecendants( sm_q,
    usr::mc::pp( [] ( const reco::Candidate* x )->bool {
    if( x->charge() != 0 ){ return false; }
    if( x->status() == 1 ){ return false; }
    return true;
  } ) );

  for( const auto final : final_list ){
    FillParticleCommonHist( "HeavyJetFinal", sm_q, final );
    FillFinalCommonHist( "HeavyJetFinal", sm_q, final );
  }

}


/// Stuff for EDM plugin

void
GenHistogram::fillDescriptions( edm::ConfigurationDescriptions&
                                descriptions )
{
  edm::ParameterSetDescription desc;
  desc.add<edm::InputTag>( "GenParticle", edm::InputTag( "genParticles" ) );
  desc.add<edm::InputTag>( "GenJet",      edm::InputTag( "ak4GenJetsNoNu" ) );
  descriptions.add( "GenHistogram", desc );
}

DEFINE_FWK_MODULE( GenHistogram );
