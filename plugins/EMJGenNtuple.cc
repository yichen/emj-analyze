#include "UserUtils/Common/interface/STLUtils/VectorUtils.hpp"
#include "UserUtils/EDMUtils/interface/EDNtupleProducer.hpp"
#include "UserUtils/PhysUtils/interface/MCHelper.hpp"

#include "DataFormats/HepMCCandidate/interface/GenParticle.h"
#include "DataFormats/Math/interface/deltaR.h"
#include "DataFormats/TrackReco/interface/Track.h"
#include "DataFormats/VertexReco/interface/Vertex.h"

#include "EMJ/Analyze/interface/EMJGenHelper.hpp"

static const int is_hs_quark  = 0;
static const int is_final_hvm = 1;
static const int is_first_smq = 2;
static const int is_first_smm = 3;
static const int others       = -1;


class EMJGenNtuple : public usr::EDNtupleProducer
{
public:
  explicit EMJGenNtuple( const edm::ParameterSet& );
  ~EMJGenNtuple(){}

  static void fillDescriptions( edm::ConfigurationDescriptions& descriptions );

private:
  void
  beginJob() override {}
  void
  doBeginRun_( const edm::Run&, const edm::EventSetup& ) override {}
  void
  doEndRun_( const edm::Run&, const edm::EventSetup& ) override {}
  void
  endJob() override {}

  bool np_analyze( const edm::Event&, const edm::EventSetup& ) override;

  // ----------member data ---------------------------
  // tokens
  edm::EDGetToken                     tok_gen;
  std::vector<const reco::Candidate*> _added_parts;

  void FillGenNtuple( const reco::Candidate* x,
                      const int              parent,
                      const int              status );
};

EMJGenNtuple::EMJGenNtuple( const edm::ParameterSet& iConfig ) :
  usr::EDNtupleProducer( iConfig, "EMJGen" ),
  tok_gen              ( GetToken<std::vector<reco::GenParticle> >(
                           "GenParticle" ) )
{
  AddCollection<float>( "Gen_pt"        );
  AddCollection<float>( "Gen_eta"       );
  AddCollection<float>( "Gen_phi"       );
  AddCollection<float>( "Gen_energy"    );
  AddCollection<int>(   "Gen_charge"    );
  AddCollection<int>(   "Gen_pdgId"     );
  AddCollection<int>(   "Gen_status"    );
  AddCollection<int>(   "Gen_parent"    );
  AddCollection<int>(   "Gen_emjstatus" );
  AddCollection<float>( "Gen_pos_x"     );
  AddCollection<float>( "Gen_pos_y"     );
  AddCollection<float>( "Gen_pos_z"     );

  // AddCollection<int>(   "Gen_origIdx"   );
  // AddCollection<float>( "Gen_pt" );
}


bool
EMJGenNtuple::np_analyze( const edm::Event&      event,
                          const edm::EventSetup& setup )
{
  edm::Handle<std::vector<reco::GenParticle> > genHandle;
  event.getByToken( tok_gen, genHandle );

  _added_parts.clear();

  // Getting the primodial dark quark:
  // - Must have a unique parent that is a HV mediator
  // - Must have an index that is a HV quark.
  const auto dq_list = usr::mc::FindAll( *genHandle,
                                         usr::mc::pp( []( const reco::Candidate*
                                                          x )->bool {
    if( x->numberOfMothers() != 1 ){ return false; }
    if( !IsHVMediator( x->mother( 0 ) ) ){ return false; }
    return IsHVQuark( x );
  } ) );

  // Getting the primodial SM quark,
  // - Most have a unique parent that is the HV mediator.
  // - Must have PDG id compatible with SM quark
  const auto sm_q_list = usr::mc::FindAll( *genHandle,
                                           usr::mc::pp( []( const reco::
                                                            Candidate* x )->bool
  {
    if( x->numberOfMothers() != 1 ){ return false; }
    if( !IsHVMediator( x->mother( 0 ) ) ){return false; }
    return usr::mc::IsSMQuark( x );
  } ) );

  assert( dq_list.size() == 2 );
  assert( sm_q_list.size() == 2 );

  const auto dark_q0 = dq_list[0]->pdgId() > 0 ?
                       dq_list[0] :
                       dq_list[1];
  const auto dark_q1 = dq_list[0]->pdgId() > 0 ?
                       dq_list[1] :
                       dq_list[0];
  const auto sm_q0 = sm_q_list[0]->pdgId() > 0 ?
                     sm_q_list[1] :
                     sm_q_list[0];
  const auto sm_q1 = sm_q_list[0]->pdgId() > 0 ?
                     sm_q_list[0] :
                     sm_q_list[1];

  FillGenNtuple( dark_q0, -1, is_hs_quark );
  FillGenNtuple( dark_q1, -1, is_hs_quark );
  FillGenNtuple( sm_q0,   -1, is_hs_quark );
  FillGenNtuple( sm_q1,   -1, is_hs_quark );

  // The dark mesons of interest are here the ones that decay back to the
  // standard model particles. Due to the way dark connections works, we will
  // need to sort them by delta matching with the dark quark, not simply by the
  // descendance
  static auto is_dark_meson = usr::mc::pp( []( const reco::Candidate* x )->bool {
    if( !IsHVMeson(
          x ) ){ return false; }
    if( HasHVDaughter( x ) ){ return false; }
    return true;
  } );

  auto mesons_0 = usr::mc::FindDecendants( dark_q0, is_dark_meson );
  auto mesons_1 = usr::mc::FindDecendants( dark_q1, is_dark_meson );

  auto meson_list = std::vector<const reco::Candidate*>( mesons_0.begin(),
                                                         mesons_0.end() );
  meson_list.insert( meson_list.end(), mesons_1.begin(), mesons_1.end() );
  meson_list = usr::RemoveDuplicate( meson_list );
  mesons_0.clear();
  mesons_1.clear();

  for( const auto m : meson_list ){
    const double dr0 = deltaR( *dark_q0, *m );
    const double dr1 = deltaR( *dark_q1, *m );

    if( dr0 < dr1 ){
      mesons_0.push_back( m );
    } else {
      mesons_1.push_back( m );
    }
  }

  // Here we are saving the entire decay tree, but 2 special topologies are noted
  // down. In using the EMJ status code:
  // - The immediate SM quark decay products.
  // - The SM meson decay product of the SM quarks

  auto Status = []( const reco::Candidate* x )->int {
                  if( usr::mc::IsSMQuark( x ) && HasHVMother( x ) ){
                    return is_first_smq;
                  } else if( usr::mc::IsSMHadron( x ) &&
                             usr::mc::HasMother( x, usr::mc::IsSMQuark ) ){
                    return is_first_smm;
                  }
                  return others;
                };

  for( const auto m : mesons_0 ){
    FillGenNtuple( m, 0, is_final_hvm );
    const auto p_list = usr::mc::FindDecendants( m,
                                                 usr::mc::Any,
                                                 usr::mc::NoTermination );

    for( const auto p : p_list ){
      FillGenNtuple( p, 0, Status( p ) );
    }
  }

  for( const auto m : mesons_1 ){
    FillGenNtuple( m, 1, is_final_hvm );
    const auto p_list = usr::mc::FindDecendants( m,
                                                 usr::mc::Any,
                                                 usr::mc::NoTermination );

    for( const auto p : p_list ){
      FillGenNtuple( p, 1, Status( p ) );
    }
  }

  const auto p_list2 = usr::mc::FindDecendants( sm_q0,
                                                usr::mc::Any,
                                                usr::mc::NoTermination );

  for( const auto p : p_list2 ){
    FillGenNtuple( p, 2, Status( p ) );
  }

  const auto p_list3 = usr::mc::FindDecendants( sm_q1,
                                                usr::mc::Any,
                                                usr::mc::NoTermination );

  for( const auto p : p_list3 ){
    FillGenNtuple( p, 3, Status( p ) );
  }

  return true;
}


void
EMJGenNtuple::FillGenNtuple( const reco::Candidate* x,
                             const int              parent,
                             const int              emjstatus )
{
  if( !usr::FindValue( _added_parts, x ) ){
    Col<float>( "Gen_pt" ).push_back( x->pt() );
    Col<float>( "Gen_eta" ).push_back( x->eta() );
    Col<float>( "Gen_phi" ).push_back( x->phi() );
    Col<float>( "Gen_energy" ).push_back( x->energy() );
    Col<int>(   "Gen_charge" ).push_back( x->charge() );
    Col<int>(   "Gen_pdgId"  ).push_back( x->pdgId() );
    Col<int>(   "Gen_status" ).push_back( x->status() );
    Col<int>(   "Gen_parent" ).push_back( parent );
    Col<int>(   "Gen_emjstatus" ).push_back( emjstatus );
    Col<float>( "Gen_pos_x" ).push_back( x->vertex().x() );
    Col<float>( "Gen_pos_y" ).push_back( x->vertex().y() );
    Col<float>( "Gen_pos_z" ).push_back( x->vertex().z() );
    _added_parts.push_back( x );
  }
}


void
EMJGenNtuple::fillDescriptions( edm::ConfigurationDescriptions&descriptions )
{
  edm::ParameterSetDescription desc;
  desc.add<edm::InputTag>( "GenParticle", edm::InputTag( "genParticles" ) );

  // desc.add<edm::InputTag>( "GenJet",      edm::InputTag( "ak4GenJetsNoNu" ) );
  descriptions.add( "EMJGenNtuple", desc );
}


DEFINE_FWK_MODULE( EMJGenNtuple );
