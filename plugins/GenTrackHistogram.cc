#include "UserUtils/Common/interface/STLUtils/VectorUtils.hpp"
#include "UserUtils/EDMUtils/interface/EDHistogramAnalyzer.hpp"
#include "UserUtils/PhysUtils/interface/MCHelper.hpp"

#include "DataFormats/HepMCCandidate/interface/GenParticle.h"
#include "DataFormats/JetReco/interface/GenJet.h"
#include "DataFormats/Math/interface/deltaR.h"
#include "DataFormats/PatCandidates/interface/PackedGenParticle.h"
#include "TrackingTools/IPTools/interface/IPTools.h"
#include "TrackingTools/Records/interface/TransientTrackRecord.h"
#include "TrackingTools/TransientTrack/interface/TransientTrackBuilder.h"

// #include "EMJ/Analyze/interface/EMJObjectSelect.hpp"
#include "EMJ/Analyze/interface/EMJGenHelper.hpp"
#include "EMJ/Analyze/interface/EMJObjectSelect.hpp"

#include "Math/VectorUtil.h"

class GenTrackHistogram : public usr::EDHistogramAnalyzer
{
public:
  explicit GenTrackHistogram( const edm::ParameterSet& );
  ~GenTrackHistogram(){}

  static void fillDescriptions( edm::ConfigurationDescriptions& descriptions );

private:
  void beginJob() override;
  void
  doBeginRun_( const edm::Run&, const edm::EventSetup& ) override {}
  void analyze( const edm::Event&, const edm::EventSetup& ) override;
  void
  doEndRun_( const edm::Run&, const edm::EventSetup& ) override {}
  void
  endJob() override {}

  // ----------member data ---------------------------
  // tokens
  edm::EDGetToken tok_gen;
  edm::EDGetToken tok_hgen;

  // edm::EDGetToken tok_gtrk;
  edm::EDGetToken tok_pfcand;
  edm::EDGetToken tok_lost;
  edm::EDGetToken tok_pv;

  // Common Helper functions
  void BookHist();

  std::vector<pat::PackedGenParticle> MakeGenList(
    const std::vector<pat::PackedGenParticle>& list  ) const;
  std::vector<const reco::Track*> MatchTrack(
    const std::vector<pat::PackedGenParticle>&,
    const std::vector<reco::Track>&,
    const double )  const;

  void FillReco( const reco::Track&,
                 const reco::Vertex&,
                 const TransientTrackBuilder&  );
  void FillGen( const pat::PackedGenParticle&,
                const reco::Candidate::Point&,
                const TransientTrackBuilder& );

  void FillMatched( const std::vector<pat::PackedGenParticle>&,
                    const reco::Candidate::Point&,
                    const std::vector<const reco::Track*>&,
                    const reco::Vertex&,
                    const TransientTrackBuilder& );
};

static reco::Candidate::Point
GenVertex( const pat::PackedGenParticle& gen, const reco::Candidate::Point& pv )
{
  if( gen.mother( 0 ) ){
    return gen.mother( 0 )->vertex();
  } else {
    return pv;
  }
}


static double
RecoIP2D( const reco::Track&           track,
          const reco::Vertex&          pv,
          const TransientTrackBuilder& ttBuilder )
{
  auto           ttrack = ttBuilder.build( track );
  Global3DVector dir( track.px(), track.py(), track.pz() );
  const auto     ip2d_p = IPTools::signedTransverseImpactParameter( ttrack,
                                                                    dir,
                                                                    pv );
  return ip2d_p.second.value();
}


static double
RecoIPZ( const reco::Track& track, const reco::Vertex& pv )
{
  return track.dz( pv.position() );
}


//
static double
GenIP2D( const pat::PackedGenParticle& gen,
         const reco::Candidate::Point& pv,
         const TransientTrackBuilder&  ttBuilder )
{
  const auto        genvtx = GenVertex( gen, pv );
  const reco::Track track( 10.0, // chi2
                           10, // dof,
                           genvtx, // reference point
                           gen.p4().Vect(), //three vector,
                           gen.charge(), // charge
                           reco::TrackBase::CovarianceMatrix() // empty covariance matrix
                           );
  const reco::Vertex vtx( pv, reco::Vertex::Error() );
  auto               ttrack = ttBuilder.build( track );
  Global3DVector     dir( gen.px(), gen.py(), gen.pz() );
  const auto         ip2d_p = IPTools::signedTransverseImpactParameter( ttrack,
                                                                        dir,
                                                                        vtx );
  return ip2d_p.second.value();
}


GenTrackHistogram::GenTrackHistogram( const edm::ParameterSet& iConfig ) :
  usr::EDHistogramAnalyzer( iConfig ),
  tok_gen                 ( GetToken<std::vector<pat::PackedGenParticle> >(
                              "GenParticle" ) ),
  tok_hgen                ( GetToken<std::vector<reco::GenParticle> >(
                              "GenHardParticle" ) ),

  // tok_gtrk                ( GetToken<std::vector<reco::Track> >(
  //                             "GeneralTracks" )),

  tok_pfcand              (  GetToken<std::vector<pat::PackedCandidate> >(
                               "PFCandidate" ) ),
  tok_lost                (  GetToken<std::vector<pat::PackedCandidate> >(
                               "LostTracks" ) ),
  tok_pv                  ( GetToken<std::vector<reco::Vertex> >(
                              "PrimaryVertex" ) )
{}


void
GenTrackHistogram::beginJob()
{
  BookHist();
}


void
GenTrackHistogram::analyze( const edm::Event&      event,
                            const edm::EventSetup& setup )
{
  auto genHHandle =
    MakeHandle<std::vector<reco::GenParticle> >( event, tok_hgen );
  auto genHandle = MakeHandle<std::vector<pat::PackedGenParticle> >( event,
                                                                     tok_gen );
  auto pvHandle = MakeHandle<std::vector<reco::Vertex> >( event, tok_pv );
  auto pfHandle = MakeHandle<std::vector<pat::PackedCandidate> >( event,
                                                                  tok_pfcand );
  auto lostHandle = MakeHandle<std::vector<pat::PackedCandidate> >( event,
                                                                    tok_lost );

  // event.getByToken( tok_gtrk,   gtrkHandle );

  edm::ESHandle<TransientTrackBuilder> ttBuilder;
  setup.get<TransientTrackRecord>().get( "TransientTrackBuilder", ttBuilder );

  // vertex selection has a well reconstructed primary vertex
  if( pvHandle->size() == 0 ){ return; }
  if( pvHandle->at( 0 ).isFake() ){ return; }
  if( fabs( pvHandle->at( 0 ).z() ) > 15 ){ return; }
  const auto pv = pvHandle->at( 0 );

  // Getting the GenLevel verset
  const auto genpv = genHHandle->at( 0 ).daughter( 0 )->vertex();
  Hist( "GenPVZ" ).Fill( genpv.z() );
  Hist( "GenPVR" ).Fill( genpv.rho() );

  // Getting the tracks of interest
  std::vector<reco::Track> tracks;
  ExtendTrackList( tracks, pv, *pfHandle,   *ttBuilder );
  ExtendTrackList( tracks, pv, *lostHandle, *ttBuilder );

  // Sort tracks according to PT
  std::sort( tracks.begin(),
             tracks.end(), []( const reco::Track& x, const reco::Track& y ){
    return x.pt() > y.pt();
  } );

  // Getting the gen particles of interest
  auto gens = MakeGenList( *genHandle );

  // Filling in the non reco-matched histograms
  for( const auto& track : tracks ){
    FillReco( track, pv, *ttBuilder );
  }

  for( const auto& gen : gens ){
    FillGen( gen, genpv, *ttBuilder );
  }

  const auto gen_tracks = MatchTrack( gens, tracks, 0.1 );
  FillMatched( gens, genpv, gen_tracks, pv, *ttBuilder );

  const auto gen_all_tracks = MatchTrack( gens, tracks, 2.0 );
  for( unsigned i = 0 ; i < gens.size() ; ++i ){
    const auto gen       = gens.at( i );
    const auto track_ptr = gen_all_tracks.at( i );
    if( track_ptr == nullptr ){ continue; }
    const auto   track     = *track_ptr;
    const double gen_ip2d  = GenIP2D( gen, genpv, *ttBuilder );
    const double reco_ip2d = RecoIP2D( track, pv, *ttBuilder );
    const double reco_ipz  = RecoIPZ( track, pv );
    Hist2D( "HasMatch_Prod_v_PTDiff" ).Fill( reco_ip2d * track.pt(),
                                             ( track.pt()-gen.pt())
                                             / gen.pt() );
  }
}


std::vector<pat::PackedGenParticle>
GenTrackHistogram::MakeGenList(
  const std::vector<pat::PackedGenParticle>& list  )
const
{
  std::vector<pat::PackedGenParticle> ans;
  std::copy_if( list.begin(),
                list.end(),
                std::back_inserter(
                  ans ), []( const pat::PackedGenParticle& x ){
    if( x.charge() == 0 ){ return false; }
    if( x.status() != 1 ){ return false; }
    if( x.pt() <= 1.0 ){ return false; }
    return true;
  } );
  return ans;
}


std::vector<const reco::Track*>
GenTrackHistogram::MatchTrack( const std::vector<pat::PackedGenParticle>& gens,
                               const std::vector<reco::Track>&            tracks,
                               const double                               range )
const
{
  std::vector<const reco::Track*> ans;
  std::vector<bool>               used( tracks.size(), false );

  for( const auto gen : gens ){
    unsigned best_match = -1;
    double   min_deltar = 0.1;

    for( unsigned i = 0; i < tracks.size(); ++i ){
      const auto track = tracks.at( i );
      if( used.at( i ) ){ continue; }
      if( track.pt() > ( 1+range ) * gen.pt() ){ continue; }
      if( track.pt() < ( 1-range ) * gen.pt() ){ continue;}
      if( deltaR( gen, track ) > min_deltar ){ continue; }
      min_deltar = deltaR( gen, track );
      best_match = i;
    }

    if( best_match == (unsigned)( -1 ) ){
      ans.push_back( nullptr );
    } else {
      used[best_match] = true;
      ans.push_back( &tracks.at( best_match ) );
    }
  }

  assert( ans.size() == gens.size());

  return ans;
}


void
GenTrackHistogram::BookHist()
{
  // Generic 2D histograms for GEN particles
  BookHist2D( "Gen_IP2D_v_PT",           200, -1, 1,  200, 1,  10 );
  BookHist1D( "GenRho", 200, 0, 5 );
  BookHist1D( "GenPt", 200, 0, 10 );

  BookHist2D( "Reco_IP2D_v_PT",          200, -1, 1,  200, 1,  10 );

  // Efficiency tracking part GEN
  BookHist2D( "HasMatch_Gen_IP2D_v_PT",  200, -1, 1,  200, 1,  10 );
  BookHist2D( "HasMatch_Reco_IP2D_v_PT", 200, -1, 1,  200, 1,  10 );
  BookHist2D( "HasMatch_Prod_v_PTDiff", 300, -6, 6, 100, -1, 1 );

  // Profiling GEN-RECO comparison dependence
  BookHist2D( "IP2D", 200, -0.1,  0.1, 200, -0.1,  0.1 );
  BookHist2D( "PT",   100,    1,    5, 100,    1,    5 );
  BookHist2D( "Prod", 300,   -6,    6, 300,   -6,    6 );
  BookHist2D( "Eta",  100,    -3,   3, 100,   -3,    3 );
  BookHist1D( "DeltaR",  100,    0, 0.1 );

  // Other quality plots
  BookHist1D( "GenPVZ", 200, -10, 10 );
  BookHist1D( "GenPVR", 200, 0,   5 );
}


void
GenTrackHistogram::FillGen( const pat::PackedGenParticle& gen,
                            const reco::Candidate::Point& pv,
                            const TransientTrackBuilder&  ttBuilder )
{
  const double ip2d = GenIP2D( gen, pv, ttBuilder );
  Hist2D( "Gen_IP2D_v_PT" ).Fill( ip2d, gen.pt() );
  Hist( "GenRho" ).Fill( ( GenVertex( gen, pv )-pv ).Rho() );
  Hist( "GenPt" ).Fill( gen.pt() );
}


void
GenTrackHistogram::FillReco( const reco::Track&           track,
                             const reco::Vertex&          pv,
                             const TransientTrackBuilder& ttBuilder )
{
  const double ip2d = RecoIP2D( track, pv, ttBuilder );
  const double ipz  = RecoIPZ( track, pv );
  Hist2D( "Reco_IP2D_v_PT" ).Fill( ip2d, track.pt() );

  const auto pattern = TrackPixelPattern( track );

  if( !HasHist2D( "TrackProd_"+pattern ) ){
    BookHist2D( "TrackProd_"+pattern, 300,  -6, 6, 100, -0.5, +0.5 );
  }
  Hist2D( "TrackProd_"+pattern ).Fill( ip2d * track.pt(), ipz );
}


void
GenTrackHistogram::FillMatched( const std::vector<pat::PackedGenParticle>& gens,
                                const reco::Candidate::Point&              genpv,
                                const std::vector<const reco::Track*>&     tracks,
                                const reco::Vertex&                        pv,
                                const TransientTrackBuilder&               ttBuilder )
{
  for( unsigned i = 0; i < gens.size(); ++i ){
    const auto gen  = gens.at( i );
    const auto tptr = tracks.at( i );
    if( !tptr ){ continue; }
    const auto track = *tptr;

    const double gen_ip2d  = GenIP2D( gen, genpv, ttBuilder );
    const double reco_ip2d = RecoIP2D( track, pv, ttBuilder );
    const double reco_ipz  = RecoIPZ( track, pv );
    Hist2D( "HasMatch_Gen_IP2D_v_PT" ).Fill( gen_ip2d, gen.pt() );
    Hist2D( "HasMatch_Reco_IP2D_v_PT" ).Fill( reco_ip2d, track.pt() );
    Hist2D( "IP2D" ).Fill( gen_ip2d, reco_ip2d );
    Hist2D( "PT" ).Fill( gen.pt(), track.pt() );
    Hist2D( "Prod" ).Fill( gen_ip2d * gen.pt(), reco_ip2d * track.pt() );
    Hist2D( "Eta" ).Fill( gen.eta(), track.eta() );
    Hist( "DeltaR" ).Fill( deltaR( gen, track ));

    const auto pattern = TrackPixelPattern( track );

    if( !HasHist2D( "HasMatch_TrackProd_"+pattern ) ){
      BookHist2D( "HasMatch_TrackProd_"+pattern, 300,  -6, 6, 100, -0.5, +0.5 );
    }
    Hist2D( "HasMatch_TrackProd_"+pattern ).Fill(
      reco_ip2d * track.pt(),
      reco_ipz );
  }
}


void
GenTrackHistogram::fillDescriptions(
  edm::ConfigurationDescriptions&descriptions )
{
  edm::ParameterSetDescription desc;

  // desc.add<edm::InputTag>(
  //   "GeneralTracks",
  //   edm::InputTag( "generalTracks" ) );
  desc.add<edm::InputTag>(
    "PFCandidate",
    edm::InputTag( "packedPFCandidates" ) );
  desc.add<edm::InputTag>(
    "LostTracks",
    edm::InputTag( "lostTracks" ) );
  desc.add<edm::InputTag>(
    "PrimaryVertex",
    edm::InputTag( "offlineSlimmedPrimaryVertices" ) );
  desc.add<edm::InputTag>(
    "GenParticle",
    edm::InputTag( "packedGenParticles" ) );
  desc.add<edm::InputTag>(
    "GenHardParticle",
    edm::InputTag( "prunedGenParticles" ) );
  descriptions.add( "GenTrackHistogram", desc );
}


DEFINE_FWK_MODULE( GenTrackHistogram );
