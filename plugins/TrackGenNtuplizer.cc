#include "UserUtils/Common/interface/STLUtils/VectorUtils.hpp"
#include "UserUtils/EDMUtils/interface/EDNtupleProducer.hpp"
#include "UserUtils/PhysUtils/interface/MCHelper.hpp"

#include "DataFormats/HepMCCandidate/interface/GenParticle.h"
#include "DataFormats/ParticleFlowCandidate/interface/PFCandidate.h"
#include "DataFormats/ParticleFlowReco/interface/PFDisplacedVertex.h"
#include "DataFormats/PatCandidates/interface/PackedCandidate.h"
#include "DataFormats/PatCandidates/interface/PackedGenParticle.h"
#include "DataFormats/TrackerRecHit2D/interface/SiPixelRecHit.h"
#include "DataFormats/TrackingRecHit/interface/TrackingRecHit.h"
#include "DataFormats/TrackReco/interface/DeDxHitInfo.h"
#include "DataFormats/TrackReco/interface/Track.h"
#include "DataFormats/VertexReco/interface/Vertex.h"
#include "SimDataFormats/TrackingAnalysis/interface/TrackingParticle.h"
#include "SimDataFormats/TrackingAnalysis/interface/TrackingVertexContainer.h"

#include "Geometry/TrackerGeometryBuilder/interface/TrackerGeometry.h"
#include \
  "RecoLocalTracker/ClusterParameterEstimator/interface/PixelClusterParameterEstimator.h"
#include "RecoLocalTracker/Records/interface/TkPixelCPERecord.h"
#include "TrackingTools/IPTools/interface/IPTools.h"
#include "TrackingTools/Records/interface/TransientTrackRecord.h"
#include "TrackingTools/TransientTrack/interface/TransientTrackBuilder.h"

#include "Math/Vector4D.h"
#include "Math/VectorUtil.h"
#include "TRandom3.h"

class TrackGenNtuplizer : public usr::EDNtupleProducer
{
public:
  explicit TrackGenNtuplizer( const edm::ParameterSet& );
  ~TrackGenNtuplizer(){}

  static void fillDescriptions( edm::ConfigurationDescriptions& descriptions );

private:
  bool np_analyze( const edm::Event&, const edm::EventSetup& ) override;

  // ----------member data ---------------------------
  // tokens
  edm::EDGetToken tok_gen;
  edm::EDGetToken tok_trk;
  edm::EDGetToken tok_pv;
  edm::EDGetToken tok_displacedvtx;
  edm::EDGetToken tok_trkTruth;

  void AddEventCollection();
  void FillEvent( const edm::Event& evt );

  void AddVertexCollection();
  void FillVertices( const std::vector<reco::Vertex>& vtx );

  void AddTrackCollection();
  void FillTracks( const std::vector<reco::Track>& trk_coll,
                   const reco::Vertex&             pv,
                   const TransientTrackBuilder&    ttbuilder );

  void AddDisplacedVertexCollection();
  void FillDisplacedVertices(
    const std::vector<reco::PFDisplacedVertex>& dpvertex,
    const reco::Vertex&                         pv,
    const TransientTrackBuilder&                ttbuilder );

  void AddGenCollection();
  void FillGens( const std::vector<reco::GenParticle>& trk_coll,
                 const TransientTrackBuilder&          ttbuilder );

  void AddMCTrackCollection();
  void FillMCTrackCollection( const TrackingVertexCollection&       mcvtx,
                              const std::vector<reco::GenParticle>& gencoll,
                              const TransientTrackBuilder&          ttbuilder );
};


TrackGenNtuplizer::TrackGenNtuplizer( const edm::ParameterSet& set ) :
  EDNtupleProducer( set, "PreSelection" ),
  tok_gen         ( GetToken<std::vector<reco::GenParticle> >(
                      "GenParticle" ) ),
  tok_trk         (  GetToken<std::vector<reco::Track> >(
                       "Tracks" ) ),
  tok_pv          ( GetToken<std::vector<reco::Vertex> >(
                      "PrimaryVertex" ) ),
  tok_displacedvtx( GetToken<reco::PFDisplacedVertexCollection>(
                      "DisplacedVertex" )),
  tok_trkTruth    ( GetToken<TrackingVertexCollection >( "TrkVertex" ))
{
  AddEventCollection();
  AddVertexCollection();
  AddTrackCollection();
  AddDisplacedVertexCollection();
  AddGenCollection();
  AddMCTrackCollection();
}


bool
TrackGenNtuplizer::np_analyze( const edm::Event&      evt,
                               const edm::EventSetup& setup )
{
  auto genHandle =
    MakeHandle<std::vector<reco::GenParticle> >( evt, tok_gen );
  auto pvHandle   = MakeHandle<std::vector<reco::Vertex> >( evt, tok_pv );
  auto trkHandle  = MakeHandle<std::vector<reco::Track> >( evt, tok_trk );
  auto dispHandle = MakeHandle<std::vector<reco::PFDisplacedVertex> >( evt,
                                                                       tok_displacedvtx );
  auto mcvtxHandle = MakeHandle<TrackingVertexCollection>( evt, tok_trkTruth );

  edm::ESHandle<TransientTrackBuilder> ttBuilder;
  setup.get<TransientTrackRecord>().get( "TransientTrackBuilder", ttBuilder );

  // Skip Events that doesn't have a well reconstructed primary vertex
  if( pvHandle->size() == 0 ){ return false; }
  if( pvHandle->at( 0 ).isFake() ){ return false; }
  if( fabs( pvHandle->at( 0 ).z() ) > 15 ){ return false; }
  const auto pv = pvHandle->at( 0 );

  FillEvent( evt );
  FillVertices( *pvHandle );
  FillTracks( *trkHandle, pv, *ttBuilder );
  FillDisplacedVertices( *dispHandle, pv, *ttBuilder );
  FillGens( *genHandle, *ttBuilder );
  FillMCTrackCollection( *mcvtxHandle, *genHandle, *ttBuilder );

  return true;
}


void
TrackGenNtuplizer::AddEventCollection()
{
  AddValue<int>( "RunNum" );
  AddValue<int>( "LumiNum" );
  AddValue<int>( "EventNum" );
}


void
TrackGenNtuplizer::FillEvent( const edm::Event& evt )
{
  SetValue<int>( "RunNum", evt.id().run() );
  SetValue<int>( "LumiNum", evt.id().luminosityBlock() );
  SetValue<int>( "EventNum", evt.id().event() );
}


void
TrackGenNtuplizer::AddVertexCollection()
{
  auto add_fcoll = [this]( const std::string& name ){
                     this->AddCollection<float>( "PrimaryVertices_"+name );
                   };
  auto add_icoll = [this]( const std::string& name ){
                     this->AddCollection<int>( "PrimaryVertices_"+name );
                   };
  auto add_bcoll = [this]( const std::string& name ){
                     this->AddCollection<bool>( "PrimaryVertices_"+name );
                   };
  add_fcoll( "x" );
  add_fcoll( "y" );
  add_fcoll( "z" );
  add_fcoll( "t" );

  add_bcoll( "isValid" );
  add_bcoll( "isFake" );

  add_fcoll( "ndof" );
  add_fcoll( "chi2" );
  add_fcoll( "xError" );
  add_fcoll( "yError" );
  add_fcoll( "zError" );
  add_fcoll( "tError" );
  add_icoll( "ntracks" );

  // add_coll( "ndof" );
}


void
TrackGenNtuplizer::FillVertices( const std::vector<reco::Vertex>& vtx_list )
{
  auto add_vtx_fvar = [this]( const std::string& name, const float x ){
                        this->Col<float>( "PrimaryVertices_"+name ).push_back(
                          x );
                      };
  auto add_vtx_ivar = [this]( const std::string& name, const int x ){
                        this->Col<int>( "PrimaryVertices_"
                                        +name ).push_back( x );
                      };
  auto add_vtx_bvar = [this]( const std::string& name, const bool x ){
                        this->Col<bool>( "PrimaryVertices_"
                                         +name ).push_back( x );
                      };
  for( const auto& vtx : vtx_list ){
    add_vtx_fvar( "x", vtx.x() );
    add_vtx_fvar( "y", vtx.y() );
    add_vtx_fvar( "z", vtx.z() );
    add_vtx_fvar( "t", vtx.t() );

    add_vtx_bvar( "isValid", vtx.isValid() );
    add_vtx_bvar( "isFake", vtx.isFake() );

    add_vtx_fvar( "ndof", vtx.ndof() );
    add_vtx_fvar( "chi2", vtx.chi2() );
    add_vtx_fvar( "xError", vtx.xError() );
    add_vtx_fvar( "yError", vtx.yError() );
    add_vtx_fvar( "zError", vtx.zError() );
    add_vtx_fvar( "tError", vtx.tError() );
    add_vtx_ivar( "ntracks", vtx.nTracks() );
  }
}


void
TrackGenNtuplizer::AddTrackCollection()
{
  auto add_fcoll = [this]( const std::string& name ){
                     this->AddCollection<float>( "Tracks_"+name );
                   };
  auto add_icoll = [this]( const std::string& name ){
                     this->AddCollection<int>( "Tracks_"+name );
                   };
  auto add_bcoll = [this]( const std::string& name ){
                     this->AddCollection<bool>( "Tracks_"+name );
                   };

  auto add_hit_fcoll = [this]( const std::string& name ){
                         this->AddCollection<float>( "RecHits_"+name );
                       };
  auto add_hit_icoll = [this]( const std::string& name ){
                         this->AddCollection<int>( "RecHits_"+name );
                       };
  auto add_hit_bcoll = [this]( const std::string& name ){
                         this->AddCollection<bool>( "RecHits_"+name );
                       };

  add_fcoll( "pt" );
  add_fcoll( "eta" );
  add_fcoll( "phi" );
  add_fcoll( "refPoint_x" );
  add_fcoll( "refPoint_y" );
  add_fcoll( "refPoint_z" );
  add_icoll( "charge" );

  add_fcoll( "normalizedChi2" );
  add_fcoll( "ptError" );
  add_fcoll( "etaError" );
  add_fcoll( "phiError" );
  add_fcoll( "qoverpError" );

  add_fcoll( "dz" );
  add_fcoll( "dzError" );
  add_fcoll( "dxy" );
  add_fcoll( "dxyError" );
  add_fcoll( "ip2d" );
  add_fcoll( "ip2dsig" );
  add_fcoll( "ip3d" );
  add_fcoll( "ip3dsig" );
  add_fcoll( "ip2dsmear" );
  add_fcoll( "ip3dsmear" );

  add_icoll( "found" );
  add_icoll( "lost" );
  add_icoll( "quality" );
  add_icoll( "pixelhits" );
  add_icoll( "trkhits" );
  add_icoll( "algo" );
  add_icoll( "origAlgo" );
  add_icoll( "stopReason" );
  add_bcoll( "isDisplaced" );
  add_fcoll( "Displaced_x" );
  add_fcoll( "Displaced_y" );
  add_fcoll( "Displaced_z" );

  // Residual stuff.
  add_icoll( "nRecHits" );

  add_hit_icoll( "rhtype" );
  add_hit_bcoll( "valid" );
  add_hit_fcoll( "globalRError" );
  add_hit_fcoll( "globalPhiError" );
  add_hit_fcoll( "globalZError" );
  add_hit_fcoll( "globalR" );
  add_hit_fcoll( "globalPhi" );
  add_hit_fcoll( "globalZ" );
  add_hit_fcoll( "localErrorxx" );
  add_hit_fcoll( "localErrorxy" );
  add_hit_fcoll( "localErroryy" );
  add_hit_fcoll( "localR" );
  add_hit_fcoll( "localPhi" );
  add_hit_fcoll( "localZ" );

  // Specific to pixel Hits
  add_hit_icoll( "qBin" );
  add_hit_fcoll( "probQ" );
  add_hit_fcoll( "probXY" );
  add_hit_bcoll( "hasBadPixel" );
  add_hit_bcoll( "isEdge" );

  // Specific to pixel clusters
  add_hit_icoll( "clusterSize" );
  add_hit_icoll( "clusterSizeX" );
  add_hit_icoll( "clusterSizeY" );
  add_hit_icoll( "clusterCharge" );
  add_hit_fcoll( "clusterX" );
  add_hit_fcoll( "clusterY" );
  add_hit_fcoll( "clusterSplitErrorX" );
  add_hit_fcoll( "clusterSplitErrorY" );
}


static int
PixelHitPattern( const reco::Track& track )
{
  int         ans     = 0;
  const auto& pattern =  track.hitPattern();

  for( int i = 0;
       i < pattern.numberOfAllHits( reco::HitPattern::TRACK_HITS ) &&
       ans < 10000000;
       i++ ){
    const uint32_t hit =
      pattern.getHitPattern( reco::HitPattern::TRACK_HITS, i );

    if( !pattern.validHitFilter( hit ) ){ continue; }
    if( !pattern.pixelHitFilter( hit ) ){ continue; }

    uint16_t layer = pattern.getLayer( hit );
    layer = pattern.pixelBarrelHitFilter( hit ) ?
            layer :
            pattern.pixelEndcapHitFilter( hit ) ?
            layer+4 :
            0;
    ans = ( 10 * ans )+layer;
  }
  return ans;
}


static int
TrackerHitPattern( const reco::Track& track )
{
  int         ans     = 0;
  const auto& pattern =  track.hitPattern();

  for( int i = 0;
       i < pattern.numberOfAllHits( reco::HitPattern::TRACK_HITS ) &&
       ans < 10000000;
       i++ ){
    const uint32_t hit =
      pattern.getHitPattern( reco::HitPattern::TRACK_HITS, i );

    if( !pattern.validHitFilter( hit ) ){ continue; }
    if( !pattern.stripTIBHitFilter( hit ) &&
        !pattern.stripTIDHitFilter( hit ) ){
      continue;
    }

    uint16_t layer = pattern.getLayer( hit );
    layer = pattern.stripTIBHitFilter( hit ) ?
            layer :
            pattern.stripTIDHitFilter( hit ) ?
            layer+4 :
            0;
    ans = ( 10 * ans )+layer;
  }

  return ans;
}


void
TrackGenNtuplizer::FillTracks( const std::vector<reco::Track>& trk_coll,
                               const reco::Vertex&             pv,
                               const TransientTrackBuilder&    ttbuilder )
{
  auto add_trk_fvar = [this]( const std::string& name, const float x ){
                        this->Col<float>( "Tracks_"+name ).push_back( x );
                      };
  auto add_trk_ivar = [this]( const std::string& name, const int x ){
                        this->Col<int>( "Tracks_"+name ).push_back( x );
                      };

  auto add_hit_fvar = [this]( const std::string& name, const float x ){
                        this->Col<float>( "RecHits_"+name ).push_back( x );
                      };
  auto add_hit_ivar = [this]( const std::string& name, const float x ){
                        this->Col<int>( "RecHits_"+name ).push_back( x );
                      };
  auto add_hit_bvar = [this]( const std::string& name, const float x ){
                        this->Col<bool>( "RecHits_"+name ).push_back( x );
                      };
  TRandom3 rand;

  for( const auto& trk : trk_coll ){
    add_trk_fvar( "pt", trk.pt() );
    add_trk_fvar( "eta", trk.eta() );
    add_trk_fvar( "phi", trk.phi() );
    add_trk_fvar( "refPoint_x", trk.referencePoint().x() );
    add_trk_fvar( "refPoint_y", trk.referencePoint().y() );
    add_trk_fvar( "refPoint_z", trk.referencePoint().z() );

    add_trk_ivar( "charge", trk.charge() );
    add_trk_fvar( "normalizedChi2", trk.normalizedChi2() );
    add_trk_fvar( "ptError", trk.ptError() );
    add_trk_fvar( "etaError", trk.etaError() );
    add_trk_fvar( "phiError", trk.phiError() );
    add_trk_fvar( "qoverpError", trk.qoverpError() );

    // Displacement variables
    add_trk_fvar( "dxy",  trk.dxy( pv.position() ));
    add_trk_fvar( "dxyError", trk.dxyError( pv.position(), pv.error()));
    add_trk_fvar( "dz", trk.dz( pv.position() ));
    add_trk_fvar( "dzError", trk.dzError());

    const auto                  ttrk = ttbuilder.build( trk );
    ROOT::Math::PtEtaPhiMVector smeared( trk.pt(), trk.eta()+rand.Gaus( 0,
                                                                        0.002 ),
                                         trk.phi()+rand.Gaus( 0, 0.002 ), 0 );

    Global3DVector dir( trk.px(), trk.py(), trk.pz());
    Global3DVector sdir( smeared.px(), smeared.py(), smeared.pz() );

    const auto& ip2d =
      IPTools::signedTransverseImpactParameter( ttrk, dir, pv );
    const auto& ip2ds =
      IPTools::signedTransverseImpactParameter( ttrk, sdir, pv );
    const auto& ip3d  = IPTools::signedImpactParameter3D( ttrk, dir, pv );
    const auto& ip3ds = IPTools::signedImpactParameter3D( ttrk, sdir, pv );
    add_trk_fvar( "ip2d", ip2d.second.value());
    add_trk_fvar( "ip2dsig", ip2d.second.significance());
    add_trk_fvar( "ip3d", ip3d.second.value());
    add_trk_fvar( "ip3dsig", ip3d.second.significance());
    add_trk_fvar( "ip2dsmear", ip2ds.second.value());
    add_trk_fvar( "ip3dsmear", ip2ds.second.value());

    /// Additional qualities
    add_trk_ivar( "lost", trk.lost() );
    add_trk_ivar( "found", trk.found() );
    add_trk_ivar( "quality", trk.qualityMask() );
    add_trk_ivar( "pixelhits", PixelHitPattern( trk ));
    add_trk_ivar( "trkhits", TrackerHitPattern( trk ));
    add_trk_ivar( "algo", trk.algo());
    add_trk_ivar( "origAlgo", trk.originalAlgo());
    add_trk_ivar( "stopReason", trk.stopReason());

    // Rechit values
    unsigned nhits = 0;

    for( auto it = trk.recHitsBegin() ; it != trk.recHitsEnd(); ++it ){
      const auto& rechit_ptr = *it;
      const auto  pixhit_ptr = dynamic_cast<const SiPixelRecHit*>( rechit_ptr );
      if( pixhit_ptr == nullptr ){ continue; }   // Storing just the pixel hits for now
      const auto& rechit = *pixhit_ptr;
      nhits++;
      add_hit_ivar( "rhtype", rechit.type() );
      add_hit_bvar( "valid", rechit.isValid() );

      // Position information
      add_hit_fvar( "globalRError", rechit.errorGlobalR() );
      add_hit_fvar( "globalPhiError", rechit.errorGlobalRPhi() );
      add_hit_fvar( "globalZError", rechit.errorGlobalZ() );
      add_hit_fvar( "globalR", rechit.globalPosition().transverse() );
      add_hit_fvar( "globalPhi", rechit.globalPosition().phi().phi() );
      add_hit_fvar( "globalZ", rechit.globalPosition().z() );
      add_hit_fvar( "localErrorxx", rechit.localPositionError().xx() );
      add_hit_fvar( "localErrorxy", rechit.localPositionError().xy() );
      add_hit_fvar( "localErroryy", rechit.localPositionError().yy() );
      add_hit_fvar( "localR", rechit.localPosition().transverse() );
      add_hit_fvar( "localPhi", rechit.localPosition().phi().phi() );
      add_hit_fvar( "localZ", rechit.localPosition().z() );

      // Information specific to the pixel hits
      add_hit_ivar( "qBin", rechit.qBin() );
      add_hit_fvar( "probQ", rechit.probabilityQ() );
      add_hit_fvar( "probXY", rechit.probabilityXY() );
      add_hit_bvar( "hasBadPixel", rechit.hasBadPixels() );
      add_hit_bvar( "isEdge", rechit.isOnEdge() );

      // Adding SiPixelCluster information
      const auto& cluster_ref = rechit.cluster();
      if( !cluster_ref.isNull() ){
        const auto& cluster = *cluster_ref;
        add_hit_ivar( "clusterSize", cluster.size() );
        add_hit_ivar( "clusterSizeX", cluster.sizeX());
        add_hit_ivar( "clusterSizeY", cluster.sizeY());
        add_hit_ivar( "clusterCharge", cluster.charge() );
        add_hit_fvar( "clusterX", cluster.x() );
        add_hit_fvar( "clusterY", cluster.y() );
        add_hit_fvar( "clusterSplitErrorX", cluster.getSplitClusterErrorX() );
        add_hit_fvar( "clusterSplitErrorY", cluster.getSplitClusterErrorY() );
      } else {
        add_hit_ivar( "clusterSize", -1000 );
        add_hit_ivar( "clusterSizeX", -1000 );
        add_hit_ivar( "clusterSizeY", -1000 );
        add_hit_ivar( "clusterCharge", -1000 );
        add_hit_fvar( "clusterX", -1e9 );
        add_hit_fvar( "clusterY", -1e9 );
        add_hit_fvar( "clusterSplitErrorX", -1e9 );
        add_hit_fvar( "clusterSplitErrorY", -1e9 );
      }
    }
    add_trk_ivar( "nRecHits",  nhits );
  }
}


void
TrackGenNtuplizer::AddDisplacedVertexCollection()
{
  auto add_fcoll = [this]( const std::string& name ){
                     this->AddCollection<float>( "DVertices_"+name );
                   };
  auto add_icoll = [this]( const std::string& name ){
                     this->AddCollection<int>( "DVertices_"+name );
                   };

  auto add_trk_fcoll = [this]( const std::string& name ){
                         this->AddCollection<float>( "DVTracks_"+name );
                       };
  auto add_trk_icoll = [this]( const std::string& name ){
                         this->AddCollection<int>( "DVTracks_"+name );
                       };

  add_fcoll( "x" );
  add_fcoll( "y" );
  add_fcoll( "z" );
  add_icoll( "VertexType" );

  // Fitting objects
  add_fcoll( "ndof" );
  add_fcoll( "chi2" );
  add_fcoll( "xError" );
  add_fcoll( "yError" );
  add_fcoll( "zError" );

  // Track collection
  add_icoll( "nTracks" );

  add_trk_fcoll( "pt" );
  add_trk_fcoll( "eta" );
  add_trk_fcoll( "phi" );
  add_trk_fcoll( "refPoint_x" );
  add_trk_fcoll( "refPoint_y" );
  add_trk_fcoll( "refPoint_z" );
  add_trk_icoll( "charge" );

  add_trk_fcoll( "normalizedChi2" );
  add_trk_fcoll( "ptError" );
  add_trk_fcoll( "etaError" );
  add_trk_fcoll( "phiError" );
  add_trk_fcoll( "qoverpError" );

  add_trk_fcoll( "dz" );
  add_trk_fcoll( "dzError" );
  add_trk_fcoll( "dxy" );
  add_trk_fcoll( "dxyError" );
  add_trk_fcoll( "ip2d" );
  add_trk_fcoll( "ip2dsig" );
  add_trk_fcoll( "ip3d" );
  add_trk_fcoll( "ip3dsig" );

  add_trk_icoll( "found" );
  add_trk_icoll( "lost" );
  add_trk_icoll( "quality" );
  add_trk_icoll( "pixelhits" );
  add_trk_icoll( "trkhits" );
  add_trk_icoll( "algo" );
  add_trk_icoll( "stopReason" );

  add_trk_icoll( "TrackType" );
}


void
TrackGenNtuplizer::FillDisplacedVertices(
  const std::vector<reco::PFDisplacedVertex>& dpvertices,
  const reco::Vertex&                         pv,
  const TransientTrackBuilder&                ttbuilder )
{
  auto fill_fvar = [this]( const std::string& name, const float x ){
                     this->Col<float>( "DVertices_"+name ).push_back( x );
                   };
  auto fill_ivar = [this]( const std::string& name, const int x ){
                     this->Col<int>( "DVertices_"+name ).push_back( x );
                   };

  auto fill_trk_fvar = [this]( const std::string& name, const float x  ){
                         this->Col<float>( "DVTracks_"+name ).push_back( x );
                       };
  auto fill_trk_ivar = [this]( const std::string& name, const int x ){
                         this->Col<int>( "DVTracks_"+name ).push_back( x );
                       };

  for( auto& vtx : dpvertices ){
    fill_fvar( "x", vtx.x() );
    fill_fvar( "y", vtx.y() );
    fill_fvar( "z", vtx.z() );
    fill_ivar( "VertexType", vtx.vertexType() );

    // Fitting objects
    fill_fvar( "ndof", vtx.ndof() );
    fill_fvar( "chi2", vtx.chi2() );
    fill_fvar( "xError", vtx.xError() );
    fill_fvar( "yError", vtx.yError() );
    fill_fvar( "zError", vtx.zError() );
    fill_ivar( "nTracks", vtx.tracksSize() );

    for( unsigned trk_idx = 0 ; trk_idx < vtx.tracksSize() ; ++trk_idx ){
      fill_trk_ivar( "TrackType", vtx.trackTypes().at( trk_idx ) );
      const reco::Track& trk = *vtx.trackRefAt( trk_idx );
      fill_trk_fvar( "pt", trk.pt() );
      fill_trk_fvar( "eta", trk.eta() );
      fill_trk_fvar( "phi", trk.phi() );
      fill_trk_fvar( "refPoint_x", trk.referencePoint().x() );
      fill_trk_fvar( "refPoint_y", trk.referencePoint().y() );
      fill_trk_fvar( "refPoint_z", trk.referencePoint().z() );

      fill_trk_ivar( "charge", trk.charge() );
      fill_trk_fvar( "normalizedChi2", trk.normalizedChi2() );
      fill_trk_fvar( "ptError", trk.ptError() );
      fill_trk_fvar( "etaError", trk.etaError() );
      fill_trk_fvar( "phiError", trk.phiError() );
      fill_trk_fvar( "qoverpError", trk.qoverpError() );

      // Displacement variables
      fill_trk_fvar( "dxy",  trk.dxy( pv.position() ));
      fill_trk_fvar( "dxyError", trk.dxyError( pv.position(), pv.error()));
      fill_trk_fvar( "dz", trk.dz( pv.position() ));
      fill_trk_fvar( "dzError", trk.dzError());

      const auto     ttrk = ttbuilder.build( trk );
      Global3DVector dir( trk.px(), trk.py(), trk.pz());
      const auto&    ip2d = IPTools::signedTransverseImpactParameter( ttrk,
                                                                      dir,
                                                                      pv );
      const auto& ip3d = IPTools::signedImpactParameter3D( ttrk, dir, pv );
      fill_trk_fvar( "ip2d", ip2d.second.value());
      fill_trk_fvar( "ip2dsig", ip2d.second.significance());
      fill_trk_fvar( "ip3d", ip3d.second.value());
      fill_trk_fvar( "ip3dsig", ip3d.second.significance());

      /// Additional qualities
      fill_trk_ivar( "lost", trk.lost() );
      fill_trk_ivar( "found", trk.found() );
      fill_trk_ivar( "quality", trk.qualityMask() );
      fill_trk_ivar( "pixelhits", PixelHitPattern( trk ));
      fill_trk_ivar( "trkhits", TrackerHitPattern( trk ));
      fill_trk_ivar( "algo", trk.algo());
      fill_trk_ivar( "stopReason", trk.stopReason());
    }
  }

  return;
}


void
TrackGenNtuplizer::AddGenCollection()
{
  auto add_fcoll = [this]( const std::string& name ){
                     this->AddCollection<float>( "GenParticles_"+name );
                   };
  auto add_icoll = [this]( const std::string& name ){
                     this->AddCollection<int>( "GenParticles_"+name );
                   };

  // auto add_bcoll = [this]( const std::string& name ){
  //                    this->AddCollection<bool>( "GenParticles_"+name );
  //                  };

  // Generator primary vertex
  AddValue<float>( "GenVtx_x" );
  AddValue<float>( "GenVtx_y" );
  AddValue<float>( "GenVtx_z" );

  add_fcoll( "pt" );
  add_fcoll( "eta" );
  add_fcoll( "phi" );
  add_fcoll( "energy" );

  add_icoll( "pdgid" );
  add_icoll( "status" );
  add_icoll( "charge" );
  add_fcoll( "vertex_x" );
  add_fcoll( "vertex_y" );
  add_fcoll( "vertex_z" );
  add_fcoll( "ip2d" );
  add_fcoll( "ip3d" );
}


void
TrackGenNtuplizer::FillGens( const std::vector<reco::GenParticle>& gen_list,
                             const TransientTrackBuilder&          ttbuilder )
{
  auto add_gen_fvar = [this]( const std::string& name, const float x ){
                        this->Col<float>( "GenParticles_"+name ).push_back( x );
                      };
  auto add_gen_ivar = [this]( const std::string& name, const int x ){
                        this->Col<int>( "GenParticles_"+name ).push_back( x );
                      };

  // Getting the Gen-Level vertex
  const auto genpv = gen_list.at( 0 ).daughter( 0 )->vertex();
  SetValue( "GenVtx_x", genpv.x() );
  SetValue( "GenVtx_y", genpv.y() );
  SetValue( "GenVtx_z", genpv.z() );

  for( const auto& gen: gen_list ){
    if( gen.status() != 1 ){ continue; }// only keeping final state particles.
    add_gen_fvar( "pt", gen.pt() );
    add_gen_fvar( "eta", gen.eta() );
    add_gen_fvar( "phi", gen.phi() );
    add_gen_fvar( "energy", gen.energy() );

    add_gen_ivar( "pdgid", gen.pdgId());
    add_gen_ivar( "status", gen.status() );
    add_gen_ivar( "charge", gen.charge() );
    add_gen_fvar( "vertex_x", gen.vertex().x() );
    add_gen_fvar( "vertex_y", gen.vertex().y() );
    add_gen_fvar( "vertex_z", gen.vertex().z() );

    // Making dummy track for gen particles
    const reco::Track track( 10.0, // chi2
                             10, // dof,
                             gen.vertex(), // reference point
                             gen.p4().Vect(), //three vector,
                             gen.charge(), // charge
                             reco::TrackBase::CovarianceMatrix() // empty covariance matrix
                             );
    const reco::Vertex vtx( genpv, reco::Vertex::Error() );
    Global3DVector     dir( gen.px(), gen.py(), gen.pz() );
    auto               ttrk = ttbuilder.build( track );
    const auto&        ip2d = IPTools::signedTransverseImpactParameter( ttrk,
                                                                        dir,
                                                                        vtx );
    const auto& ip3d =
      IPTools::signedImpactParameter3D( ttrk, dir, vtx );
    add_gen_fvar( "ip2d", ip2d.second.value() );
    add_gen_fvar( "ip3d", ip3d.second.value() );
  }
}


void
TrackGenNtuplizer::AddMCTrackCollection()
{
  auto add_fcoll = [this]( const std::string& name ){
                     this->AddCollection<float>( "MCVertices_"+name );
                   };
  auto add_icoll = [this]( const std::string& name ){
                     this->AddCollection<int>( "MCVertices_"+name );
                   };

  auto add_strk_fcoll = [this]( const std::string& name ){
                          this->AddCollection<float>( "MCSourceTracks_"+name );
                        };
  auto add_strk_icoll = [this]( const std::string& name ){
                          this->AddCollection<int>( "MCSourceTracks_"+name );
                        };
  auto add_dtrk_fcoll = [this]( const std::string& name ){
                          this->AddCollection<float>( "MCDaughterTracks_"
                                                      +name );
                        };
  auto add_dtrk_icoll = [this]( const std::string& name ){
                          this->AddCollection<int>( "MCDaughterTracks_"+name );
                        };

  add_fcoll( "x" );
  add_fcoll( "y" );
  add_fcoll( "z" );
  add_icoll( "nSource" );
  add_icoll( "nDaughter" );

  add_strk_fcoll( "pt" );
  add_strk_fcoll( "eta" );
  add_strk_fcoll( "phi" );
  add_strk_fcoll( "energy" );
  add_strk_icoll( "charge" );
  add_strk_icoll( "pdgid" );
  add_strk_icoll( "status" );
  add_strk_fcoll( "ip2d" );
  add_strk_fcoll( "ip3d" );

  add_dtrk_fcoll( "pt" );
  add_dtrk_fcoll( "eta" );
  add_dtrk_fcoll( "phi" );
  add_dtrk_fcoll( "energy" );
  add_dtrk_icoll( "charge" );
  add_dtrk_icoll( "pdgid" );
  add_dtrk_icoll( "status" );
  add_dtrk_fcoll( "ip2d" );
  add_dtrk_fcoll( "ip3d" );
}


static double
MCTrkIP2D( const TrackingParticle&      p,
           const math::XYZPoint         genvtx,
           const TransientTrackBuilder& ttbuilder )
{
  // Making dummy track for gen particles
  const reco::Track track( 10.0,   // chi2
                           10,   // dof,
                           p.vertex(),   // reference point
                           p.p4().Vect(),   //three vector,
                           p.charge(),   // charge
                           reco::TrackBase::CovarianceMatrix()   // empty covariance matrix
                           );
  const reco::Vertex vtx( genvtx, reco::Vertex::Error() );
  Global3DVector     dir( p.px(), p.py(), p.pz() );
  auto               ttrk = ttbuilder.build( track );
  return IPTools::signedTransverseImpactParameter( ttrk,
                                                   dir,
                                                   vtx ).second.value();
  return 0;
}


static double
MCTrkIP3D( const TrackingParticle&      p,
           const math::XYZPoint         genvtx,
           const TransientTrackBuilder& ttbuilder )
{
  // Making dummy track for gen particles
  const reco::Track track( 10.0,   // chi2
                           10,   // dof,
                           p.vertex(),   // reference point
                           p.p4().Vect(),   //three vector,
                           p.charge(),   // charge
                           reco::TrackBase::CovarianceMatrix()   // empty covariance matrix
                           );
  const reco::Vertex vtx( genvtx, reco::Vertex::Error() );
  Global3DVector     dir( p.px(), p.py(), p.pz() );
  auto               ttrk = ttbuilder.build( track );
  return IPTools::signedImpactParameter3D( ttrk, dir, vtx ).second.value();
  return 0;
}


void
TrackGenNtuplizer::FillMCTrackCollection(
  const TrackingVertexCollection&       mcvtx,
  const std::vector<reco::GenParticle>& gen_list,
  const TransientTrackBuilder&          ttbuilder )
{
  auto fill_fvar = [this]( const std::string& name, const float x ){
                     this->Col<float>( "MCVertices_"+name ).push_back( x );
                   };
  auto fill_ivar = [this]( const std::string& name, const int x ){
                     this->Col<int>( "MCVertices_"+name ).push_back( x );
                   };
  auto fill_strk_fvar = [this]( const std::string& name, const float x ){
                          this->Col<float>( "MCSourceTracks_"+name ).push_back(
                            x );
                        };
  auto fill_strk_ivar = [this]( const std::string& name, const int x ){
                          this->Col<int>( "MCSourceTracks_"
                                          +name ).push_back( x );
                        };
  auto fill_dtrk_fvar = [this]( const std::string& name, const float x ){
                          this->Col<float>( "MCDaughterTracks_"
                                            +name ).push_back( x );
                        };
  auto fill_dtrk_ivar = [this]( const std::string& name, const int x ){
                          this->Col<int>( "MCDaughterTracks_"+name ).push_back(
                            x );
                        };
  const auto genpv = gen_list.at( 0 ).daughter( 0 )->vertex();
  for( const auto& vtx: mcvtx ){
    fill_fvar( "x", vtx.position().x() );
    fill_fvar( "y", vtx.position().y() );
    fill_fvar( "z", vtx.position().z() );
    fill_ivar( "nSource", vtx.nSourceTracks() );
    fill_ivar( "nDaughter", vtx.nDaughterTracks() );

    for( auto it = vtx.sourceTracks_begin();
         it != vtx.sourceTracks_end();
         ++it ){
      const auto& strk = **it;
      fill_strk_fvar( "pt", strk.pt() );
      fill_strk_fvar( "eta", strk.eta() );
      fill_strk_fvar( "phi", strk.phi() );
      fill_strk_fvar( "energy", strk.energy() );
      fill_strk_ivar( "charge", strk.charge() );
      fill_strk_ivar( "pdgid", strk.pdgId() );
      fill_strk_ivar( "status", strk.status() );
      fill_strk_fvar( "ip2d", MCTrkIP2D( strk, genpv, ttbuilder ));
      fill_strk_fvar( "ip3d", MCTrkIP3D( strk, genpv, ttbuilder ));
    }

    for( auto it = vtx.daughterTracks_begin();
         it != vtx.daughterTracks_end();
         ++it ){
      const auto& dtrk = **it;
      fill_dtrk_fvar( "pt", dtrk.pt() );
      fill_dtrk_fvar( "eta", dtrk.eta() );
      fill_dtrk_fvar( "phi", dtrk.phi() );
      fill_dtrk_fvar( "energy", dtrk.energy() );
      fill_dtrk_ivar( "charge", dtrk.charge() );
      fill_dtrk_ivar( "pdgid", dtrk.pdgId() );
      fill_dtrk_ivar( "status", dtrk.status() );
      fill_dtrk_fvar( "ip2d", MCTrkIP2D( dtrk, genpv, ttbuilder ));
      fill_dtrk_fvar( "ip3d", MCTrkIP3D( dtrk, genpv, ttbuilder ));
    }
  }
}


void
TrackGenNtuplizer::fillDescriptions(
  edm::ConfigurationDescriptions&descriptions )
{
  edm::ParameterSetDescription desc;

  desc.add<edm::InputTag>(
    "Tracks",
    edm::InputTag( "generalTracks" ) );

  desc.add<edm::InputTag>(
    "PrimaryVertex",
    edm::InputTag( "offlinePrimaryVertices" ) );
  desc.add<edm::InputTag>(
    "GenParticle",
    edm::InputTag( "genParticles" ) );
  desc.add<edm::InputTag>(
    "DeDxInfo",
    edm::InputTag( "dedxHitInfo" ) );
  desc.add<edm::InputTag>(
    "PFCandidate",
    edm::InputTag( "particleFlow" ));
  desc.add<edm::InputTag>(
    "DisplacedVertex",
    edm::InputTag( "particleFlowDisplacedVertex" ) );
  desc.add<edm::InputTag>(
    "TrkVertex",
    edm::InputTag( "mix", "MergedTrackTruth" ));
  descriptions.add( "TrackGenNtuplizer", desc );
}


DEFINE_FWK_MODULE( TrackGenNtuplizer );
