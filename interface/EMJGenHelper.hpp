#ifndef EMJ_ANALYZE_EMJGENHELPER_HPP
#define EMJ_ANALYZE_EMJGENHELPER_HPP

#include "DataFormats/HepMCCandidate/interface/GenParticle.h"

extern bool IsHVParticle( const reco::Candidate* );
extern bool IsHVMediator( const reco::Candidate* );
extern bool IsHVQuark( const reco::Candidate* );
extern bool IsHVMeson( const reco::Candidate* );
extern bool HasHVDaughter( const reco::Candidate* );
extern bool HasHVMother( const reco::Candidate* );

class HVParser
{
public:
  HVParser( const reco::Candidate*, const reco::Candidate* );
  bool operator()( const reco::Candidate*, const reco::Candidate* ) const;

};

#endif
