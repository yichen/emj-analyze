#ifndef EMJ_OBJECT_SELECT_HPP
#define EMJ_OBJECT_SELECT_HPP

#include "DataFormats/HepMCCandidate/interface/GenParticle.h"
#include "DataFormats/JetReco/interface/Jet.h"
#include "DataFormats/JetReco/interface/PFJet.h"
#include "DataFormats/PatCandidates/interface/PackedCandidate.h"
#include "DataFormats/TrackReco/interface/Track.h"
#include "DataFormats/VertexReco/interface/Vertex.h"
#include "RecoVertex/VertexPrimitives/interface/TransientVertex.h"
#include "TrackingTools/Records/interface/TransientTrackRecord.h"
#include "TrackingTools/TransientTrack/interface/TransientTrack.h"
#include "TrackingTools/TransientTrack/interface/TransientTrackBuilder.h"

#include <vector>

extern std::vector<const reco::Candidate*> FindEMJPartons(
  const std::vector<reco::GenParticle>& );

const reco::Candidate* FindDarkQuark( const std::vector<reco::GenParticle>& );
const reco::Candidate* FindSMQuark( const std::vector<reco::GenParticle>& );

extern std::vector<std::vector<const reco::Candidate*> > FindVertexChildren(
  const reco::GenParticle* );

int GetPrimaryVertex( const std::vector<reco::Vertex>& pvlist );

bool IsGoodTrack( const reco::Track&           track,
                  const reco::Vertex&          pv,
                  const TransientTrackBuilder& ttBuilder );


void ExtendTrackList( std::vector<reco::Track>&                tracklist,
                      const reco::Vertex&                      pv,
                      const std::vector<pat::PackedCandidate>& source,
                      const TransientTrackBuilder&             ttBuilder );

void ExtendTrackList( std::vector<reco::Track>&       tracklist,
                      const reco::Vertex&             pv,
                      const std::vector<reco::Track>& source,
                      const TransientTrackBuilder&    ttBuilder );


void GetSelectedJetTracks( const std::vector<reco::PFJet>&         jet_source,
                           const std::vector<reco::Track>&         track_source,
                           std::vector<reco::PFJet>&               jet_out,
                           std::vector<std::vector<reco::Track> >& track_out );


std::vector<reco::TransientTrack> MakeTTList(
  const std::vector<reco::Track>& source,
  const TransientTrackBuilder&    ttBuilder );

double PVTrackFraction( const reco::Vertex&                           pv,
                        const std::vector<std::vector<reco::Track> >& tracks );

double PVSumPt2( const reco::Vertex& pv );
double PVFuzzyness( const reco::Vertex&          pv,
                    const TransientTrackBuilder& ttbuilder );
double PVJetFuzzyness( const reco::Vertex&                           pv,
                       const std::vector<std::vector<reco::Track> >& jet_track_list,
                       const TransientTrackBuilder&                  ttbuilder );
double PVClosestZ( const reco::Vertex&              pv,
                   const std::vector<reco::Vertex>& vtxlist );

template<typename JETTYPE>
bool
IsGoodJet( const JETTYPE& jet, const reco::Candidate* darkq )
{
  // Basic quality cuts
  if( fabs( jet.eta() ) > 2.0 ){ return false; }
  if( jet.chargedEmEnergyFraction() > 0.9 ){ return false; }
  if( jet.neutralEmEnergyFraction() > 0.9 ){ return false; }

  // Match with dark quark
  if( darkq && deltaR( jet, *darkq ) > 0.4 ){ return false; }

  return true;
}


bool TrackMatchJet( const reco::Jet&            jet,
                    const reco::TransientTrack& ttrack );

std::vector<reco::TransientTrack> MatchJetTracks(
  const reco::Jet&                         jet,
  const std::vector<reco::TransientTrack>& tracklist );

std::vector<TransientVertex> ManualVertexLoop(
  const std::vector<reco::TransientTrack>& tracklist );

extern std::vector<TransientVertex> MakeJetVertices( const reco::Jet&,
                                                     const reco::Vertex&,
                                                     const std::vector<reco::TransientTrack>& tracklist );

std::vector<TransientVertex> SelectVertex( const reco::Jet&,
                                           const reco::Vertex&,
                                           const std::vector<TransientVertex>& );

std::string TrackPixelPattern( const reco::Track& track );


#endif
