#ifndef EMJVERTEXFORMAT
#define EMJVERTEXFORMAT

#include "TTree.h"

class EMJJetFormat
{
public:
  // Intrinsic jet variables
  static constexpr unsigned MAX_OBJS = 1024;
  static constexpr unsigned MAX_JETS = 10;

  unsigned njets;
  double pt[MAX_JETS];
  double eta[MAX_JETS];
  double phi[MAX_JETS];
  double energy[MAX_JETS];
  double btag[MAX_JETS];

  unsigned tot_tracks;
  unsigned ntracks[MAX_JETS];
  unsigned trackidx[MAX_JETS];
  double track_pt[MAX_OBJS];
  double track_eta[MAX_OBJS];
  double track_phi[MAX_OBJS];
  double track_ip2d[MAX_OBJS];
  double track_ip3d[MAX_OBJS];
  double track_ip3dsig[MAX_OBJS];
  double track_ip2dsig[MAX_OBJS];
  double track_deltaR[MAX_OBJS];
  int track_vertidx[MAX_OBJS];

  unsigned tot_vertices;
  unsigned nvertex[MAX_JETS];
  unsigned vertexidx[MAX_JETS];
  double vertex_x[MAX_OBJS];
  double vertex_y[MAX_OBJS];
  double vertex_z[MAX_OBJS];
  double vertex_xErr[MAX_OBJS];
  double vertex_yErr[MAX_OBJS];
  double vertex_zErr[MAX_OBJS];
  double vertex_mass[MAX_OBJS];
  unsigned vertex_mult[MAX_OBJS];

  unsigned tot_gens;
  int parton_pdgid[MAX_JETS];
  unsigned ngen[MAX_JETS];
  unsigned genidx[MAX_JETS];
  unsigned gen_nvis[MAX_OBJS];
  double gen_vis_pt[MAX_OBJS];
  double gen_vis_eta[MAX_OBJS];
  double gen_vis_phi[MAX_OBJS];
  double gen_vis_mass[MAX_OBJS];
  double gen_decay_x[MAX_OBJS];
  double gen_decay_y[MAX_OBJS];
  double gen_decay_z[MAX_OBJS];
  double gen_pt[MAX_OBJS];
  double gen_eta[MAX_OBJS];
  double gen_phi[MAX_OBJS];
  double gen_energy[MAX_OBJS];
  double gen_lifetime[MAX_OBJS];
  int gen_pdgid[MAX_OBJS];

  void
  AddToTree( TTree* tree )
  {
    tree->Branch( "njets",         &njets  );
    tree->Branch( "pt",            pt,     "pt[njets]/D" );
    tree->Branch( "eta",           eta,    "eta[njets]/D" );
    tree->Branch( "phi",           phi,    "phi[njets]/D" );
    tree->Branch( "energy",        energy, "energy[njets]/D" );
    tree->Branch( "btag",          btag,   "btag[njets]/D" );

    tree->Branch( "tot_tracks",    &tot_tracks );

    tree->Branch( "ntracks",       ntracks,       "ntracks[njets]/i" );
    tree->Branch( "trackidx",      trackidx,      "trackidx[njets]/i" );
    tree->Branch( "track_pt",      track_pt,      "track_pt[tot_tracks]/D" );
    tree->Branch( "track_eta",     track_eta,     "track_eta[tot_tracks]/D" );
    tree->Branch( "track_phi",     track_phi,     "track_phi[tot_tracks]/D" );
    tree->Branch( "track_ip2d",    track_ip2d,    "track_ip2d[tot_tracks]/D" );
    tree->Branch( "track_ip3d",    track_ip3d,    "track_ip3d[tot_tracks]/D" );
    tree->Branch( "track_ip2dsig", track_ip2dsig, "track_ip2dsig[tot_tracks]/D" );
    tree->Branch( "track_ip3dsig", track_ip3dsig, "track_ip3gsig[tot_tracks]/D" );
    tree->Branch( "track_vertidx", track_vertidx, "track_vertidx[tot_tracks]/I" );

    tree->Branch( "tot_vertices",  &tot_vertices );

    tree->Branch( "nvertex",       nvertex,      "nvertex[njets]/i" );
    tree->Branch( "vertexidx",     vertexidx,    "vertexidx[njets]/i" );
    tree->Branch( "vertex_x",      vertex_x,     "vertex_x[tot_vertices]/D" );
    tree->Branch( "vertex_y",      vertex_y,     "vertex_y[tot_vertices]/D" );
    tree->Branch( "vertex_z",      vertex_z,     "vertex_z[tot_vertices]/D" );
    tree->Branch( "vertex_xErr",   vertex_xErr,  "vertex_xErr[tot_vertices]/D" );
    tree->Branch( "vertex_yErr",   vertex_yErr,  "vertex_yErr[tot_vertices]/D" );
    tree->Branch( "vertex_zErr",   vertex_zErr,  "vertex_zErr[tot_vertices]/D" );
    tree->Branch( "vertex_mass",   vertex_mass,  "vertex_mass[tot_vertices]/D" );
    tree->Branch( "vertex_mult",   vertex_mult,  "vertex_mass[tot_vertices]/i" );

    tree->Branch( "tot_gens",      &tot_gens );
    tree->Branch( "parton_pdgid",  parton_pdgid, "parton_pdgid[njets]/I" );
    tree->Branch( "ngen",          ngen,         "ngen[njets]/i" );
    tree->Branch( "genidx",        genidx,       "genidx[njets]/i" );
    tree->Branch( "gen_vis_pt",    gen_vis_pt,   "gen_vis_pt[tot_gens]/D" );
    tree->Branch( "gen_vis_eta",   gen_vis_eta,  "gen_vis_eta[tot_gens]/D" );
    tree->Branch( "gen_vis_phi",   gen_vis_phi,  "gen_vis_phi[tot_gens]/D" );
    tree->Branch( "gen_vis_mass",  gen_vis_mass, "gen_vis_mass[tot_gens]/D" );
    tree->Branch( "gen_pt",        gen_pt,       "gen_pt[tot_gens]/D" );
    tree->Branch( "gen_eta",       gen_eta,      "gen_eta[tot_gens]/D" );
    tree->Branch( "gen_phi",       gen_phi,      "gen_phi[tot_gens]/D" );
    tree->Branch( "gen_energy",    gen_energy,   "gen_energy[tot_gens]/D" );
    tree->Branch( "gen_decay_x",   gen_decay_x,  "gen_decay_x[tot_gens]/D" );
    tree->Branch( "gen_decay_y",   gen_decay_y,  "gen_decay_y[tot_gens]/D" );
    tree->Branch( "gen_decay_z",   gen_decay_z,  "gen_decay_z[tot_gens]/D" );
    tree->Branch( "gen_pdgid",     gen_pdgid,    "gen_pdgid[tot_gens]/I" );
    tree->Branch( "gen_lifetime",  gen_lifetime, "gen_lifetime[tot_gens]/D" );
  }

  void
  LoadBranches( TTree* tree )
  {
    tree->SetBranchAddress( "njets",         &njets );
    tree->SetBranchAddress( "pt",            pt     );
    tree->SetBranchAddress( "eta",           eta    );
    tree->SetBranchAddress( "phi",           phi    );
    tree->SetBranchAddress( "energy",        energy );
    tree->SetBranchAddress( "btag",          btag   );

    tree->SetBranchAddress( "tot_tracks",    &tot_tracks );
    tree->SetBranchAddress( "ntracks",       ntracks );
    tree->SetBranchAddress( "trackidx",      trackidx );
    tree->SetBranchAddress( "track_pt",      track_pt );
    tree->SetBranchAddress( "track_eta",     track_eta );
    tree->SetBranchAddress( "track_phi",     track_phi );
    tree->SetBranchAddress( "track_ip2d",    track_ip2d );
    tree->SetBranchAddress( "track_ip3d",    track_ip3d );
    tree->SetBranchAddress( "track_ip2dsig", track_ip2dsig );
    tree->SetBranchAddress( "track_ip3dsig", track_ip3dsig );
    tree->SetBranchAddress( "track_vertidx", track_vertidx );

    tree->SetBranchAddress( "tot_vertices",  &tot_vertices );
    tree->SetBranchAddress( "nvertex",       nvertex );
    tree->SetBranchAddress( "vertexidx",     vertexidx );
    tree->SetBranchAddress( "vertex_x",      vertex_x );
    tree->SetBranchAddress( "vertex_y",      vertex_y );
    tree->SetBranchAddress( "vertex_z",      vertex_z );
    tree->SetBranchAddress( "vertex_xErr",   vertex_xErr );
    tree->SetBranchAddress( "vertex_yErr",   vertex_yErr );
    tree->SetBranchAddress( "vertex_zErr",   vertex_zErr );
    tree->SetBranchAddress( "vertex_mass",   vertex_mass );
    tree->SetBranchAddress( "vertex_mult",   vertex_mult );

    tree->SetBranchAddress( "tot_gens",      &tot_gens );
    tree->SetBranchAddress( "ngen",          ngen );
    tree->SetBranchAddress( "genidx",        genidx );
    tree->SetBranchAddress( "parton_pdgid",  parton_pdgid );
    tree->SetBranchAddress( "gen_vis_pt",    gen_vis_pt );
    tree->SetBranchAddress( "gen_vis_eta",   gen_vis_eta );
    tree->SetBranchAddress( "gen_vis_phi",   gen_vis_phi );
    tree->SetBranchAddress( "gen_vis_mass",  gen_vis_mass );
    tree->SetBranchAddress( "gen_pt",        gen_pt );
    tree->SetBranchAddress( "gen_eta",       gen_eta );
    tree->SetBranchAddress( "gen_phi",       gen_phi );
    tree->SetBranchAddress( "gen_energy",    gen_energy );
    tree->SetBranchAddress( "gen_lifetime",  gen_lifetime );
    tree->SetBranchAddress( "gen_decay_x",   gen_decay_x );
    tree->SetBranchAddress( "gen_decay_y",   gen_decay_y );
    tree->SetBranchAddress( "gen_decay_z",   gen_decay_z );
    tree->SetBranchAddress( "gen_pdgid",     gen_pdgid );
  }
};

#endif
