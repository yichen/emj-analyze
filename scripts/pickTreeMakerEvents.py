#!/usr/bin/env python2
# Simple script for getting files listed in a particular sample for EDM files.
import argparse, imp, os, subprocess

parser = argparse.ArgumentParser("""
Very simple wrapper script for copying some events in MINIAOD files listed in
the TreeMaker package to a local file.
""")

parser.add_argument('--sampledict',
                    '-s',
                    type=str,
                    required=True,
                    help='Sample dict to use')


args = parser.parse_args()

dict_file = "{0}/src/TreeMaker/Production/test/condorSub/dict_{1}.py".format(
    os.getenv('CMSSW_BASE'), args.sampledict)

print(dict_file)
samples = imp.load_source('flist', dict_file).flist['samples']
samples = sum(samples, [])


def getfiles(sample):
  cff_file = "{0}/src/TreeMaker/Production/python/{1}_cff".format(
      os.getenv('CMSSW_BASE'), sample).replace('.', '/') + ".py"
  flist = list(imp.load_source('readFiles', cff_file).readFiles)
  return flist

files = [getfiles(x) for x in samples]

for flist,sample in zip(files,samples):
  with open('{0}_Files.txt'.format(sample), 'w') as f:
    f.write("\n".join(flist))